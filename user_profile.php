<?php include("header_profile.php");


if (empty($ProfileAvatar))
{ 
	$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
}
elseif (!empty($ProfileAvatar))
{
	$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
}

//Check if user has business

$BusinessExists = $mysqli->query("SELECT * FROM reviews WHERE rev_active=1 AND u_id='$id'");

//Get Review Count

$ReviewsCount = $mysqli->query("SELECT * FROM reviews WHERE rev_active=1 AND u_id='$id'");
$NumReviews = $ReviewsCount->num_rows;

$ProductReviewsCount = $mysqli->query("SELECT * FROM produits WHERE user_id='$id'");
$ProductNumReviews = $ProductReviewsCount->num_rows;

$allReviews = $NumReviews + $ProductNumReviews ;

//Get Number of Followers
$follower = $mysqli->query("SELECT * FROM followers WHERE idFollowed='$id'");
$NumFollowers = $follower->num_rows;


//Verify if this user has suscribed
$followerSUB = $mysqli->query("SELECT * FROM followers WHERE idFollowed='$id' AND idFollower='$UserId'");
$subscribed = $followerSUB->num_rows;

//Get Ranking
$userRank = $mysqli->query("SELECT rank, image FROM ranking WHERE idRanking='$idRanking'");
$rankInfo = mysqli_fetch_array($userRank);

$rankname = $rankInfo["rank"];
$rankimage = $rankInfo["image"];

$userRank->close();

?>

  <div class="container container-main">
    
    <div class="col-md-4">
              
		<div class="col-shadow">
		
			<div class="right-title">
				<h1 class="pull-left"><?php echo ucfirst($ProfileAuthor);?></h1>
			</div>
			
			<div class="img-profile">
					<img src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=200&amp;w=200&amp;q=100" alt="<?php echo ucfirst($ProfileAuthor);?>" class="img-circle">
		   </div><!--img-profile-->
      
		</div>
		
		
		<div class="col-shadow col-profile">
		  
		   <div class="row"> 
			
				<div class="col-sm-3">
					<div class="button-like" id="follow">
						<?php
							if($subscribed == 1)
							{
								echo '<span style="color : red">';
								
								?>
									Suivi </span>
								<?php
							}
						
							
						?>
							
						<input readonly value="<?php echo $UserId?>" id="follower" name="follower" class="user-iden">
						<input readonly value="<?php echo $id?>" id="followed" name="followed" class="user-iden">
					</div>
				</div>
				<div class="col-sm-4"><div class="button-like"><a href="contact_user-<?php echo $id ;?>" >Contacter</a></div></div>
				<div class="col-sm-5"><a href="user_services-<?php echo $id ?>"><div class="button-like">Prendre Rendez-vous</div></a></div>
			
		   </div>
		  
		</div>
  
    
		<div class="col-shadow col-profile">
		  <div class="right-title">
			<h1 class="pull-left">Activité Professionnelle</h1>
		  </div>
		   <!-- 
				Afficher les activités professionnelles de l'utilisateur
				Afficher les services qu'il a créee
				Afficher son lieu de travail si il existe
		   -->
		  
		   <?php
							if($workplace != null)
							{
								echo '<p>Travaille chez <span style="color: red;">'.$workplace."</span></p>" ;
							}
							
							if($pid != 0)
							{
								if($SectorSql = $mysqli->query("SELECT * FROM secteurs WHERE secteur_id='$pid'"))
								{
									$SectorInfo = mysqli_fetch_array($SectorSql);
									
									$jobname = $SectorInfo["nom"];
									$mid = $SectorInfo["parent_sector"];
									
									$SectorSql->close();
									
									if($SectorMainSql = $mysqli->query("SELECT * FROM secteurs WHERE secteur_id='$mid'"))
									{
										$SectorMainInfo = mysqli_fetch_array($SectorMainSql);
										
										$jobnameMain = $SectorMainInfo["nom"];
										$SectorMainSql->close();
									}
									else
									{
										printf("Un problème est survenu.");
									}
									
									
									echo '<p>En tant que <span style="color: red;">'.utf8_encode($jobname).'</span> dans <span style="color: red;">'.utf8_encode($jobnameMain).'</span></p>';
								}
								else
								{
									printf("Un problème est survenu.");
								}
							}
					
							if($ServiceSql = $mysqli->query("SELECT * FROM business WHERE biz_user='$id'"))
							{
								$biznum = $ServiceSql -> num_rows;
								
								if($biznum > 0)
								{
									?>

									<p> Services :  
									
									<?php
									$i =0 ;
									
									while($ServiceInfo = mysqli_fetch_array($ServiceSql))
									{
									?>
									
										<a href="business-<?php echo $ServiceInfo['biz_id'];?>-<?php echo $ServiceInfo['business_name'];?>"><?php echo $ServiceInfo['business_name'];?></a>
										
										<?php
										$i++;
										
										if($i != $biznum)
										{
											echo ", ";
										}
									
									}
									echo '</p>';
								}
								
								$ServiceSql->close();
								
							}
							else
							{
								 
								 printf("Un problème est survenu.");
								 
							}
				
			?>
		   
		</div>
   
	
	
		<div class="col-shadow col-profile">
		  <div class="right-title">
			<h1 class="pull-left">A propos <?php echo ucfirst($ProfileAuthor);?></h1>
		  </div>
		   <p><?php echo stripslashes($ProfileInfo['about']);?></p>
		  
		</div>
    
	
	
	
	<div class="col-shadow">
		  <div class="right-title">
			<h1 class="pull-left">Statistics</h1>
		  </div>
			
			<table style="width: 100%; ">
			
				<tr style="height: 10px;"></tr>
				
				<tr>
				
					<td class="center"> <img src="<?php echo $rankimage?>" style="width: 30px; height: 30px "></td>
					
					<td class="center"> <img src="images/badges/group.png" style="width: 30px; height: 30px "></td>
				
				</tr>
				
				<tr style="height: 20px;">
				
					<td class="center"><div class="infostyle"><?php echo $rankname ?></div></td>
					<td class="center"><div class="infostyle"><?php echo $NumFollowers ?> Abonnés</div></td>
				
				</tr>
				
				<tr style="height: 20px;"></tr>
				
				<tr >
				
					<td class="center"> <img src="images/badges/edit.png" style="width: 30px; height: 30px "></td>
					
					<td class="center"> <img src="images/badges/bag.png" style="width: 30px; height: 30px "></td>
				
				</tr>
				
				<tr style="height: 20px;">
				
					<td class="center"><div class="infostyle"><?php echo $ProductNumReviews ?> avis Produits</div></td>
					<td class="center"><div class="infostyle"><?php echo $NumReviews ?> avis Services</div></td>
				
				</tr>
				
				<tr style="height: 20px;"></tr>
				
			</table>
			
	</div>   
	
	
		<div class="col-shadow col-profile">
		  <div class="right-title">
			<h1 class="pull-left">Mots clés</h1>
		  </div>
		   <?php
		   //Split Tags for viewing
		   
		    if($pieces != '')
		    {
					   $pieces = explode(",", $tags);
					   
					   for($z=0 ; $z < sizeof($pieces) ; $z++)
					   {
						   ?>
						   
						   <div class="tagsStyle card-1">
								<?php echo $pieces[$z]; ?>
						   </div>
						   <?php
					   }
			}
		   ?>
		  <!--col-right--> 
		</div>
    
    <?php if(!empty($Ad1)){?>
<div class="col-shadow col-ads">
<?php echo $Ad1;?>
</div><!--col-shadow-->
<?php }?>
    
    
    </div><!--col-md-4-->
    
    <div class="col-md-8"> 
<script>     
$(document).ready(function()
{
$('.star-rates').raty({
	readOnly: true,
    score: function() {
    return $(this).attr('data-score');

  }
});
});
</script>

      <div class="col-shadow">
			<div class="biz-title-2">
					<h1>Avis récent par <?php echo ucfirst($ProfileAuthor);?></h1>
			</div>
			
			<div class="col-desc" id="display-reviews">
			<?php

				if($Reviews = $mysqli->query("SELECT * FROM reviews LEFT JOIN business ON business.biz_id=reviews.b_id WHERE reviews.b_id=business.biz_id AND reviews.rev_active=1 AND reviews.u_id='$id' ORDER BY reviews.rev_id DESC LIMIT 10"))
				{
	

						while($ReviewsRow = mysqli_fetch_array($Reviews))
						{
		
							$bizName = $ReviewsRow['business_name'];
							$BizLink = preg_replace("![^a-z0-9]+!i", "-", $bizName);
							$UserLink = urlencode(strtolower($BizLink));
						
							$RewId = $ReviewsRow['rev_id'];
	
			?>
				<div class="review-box"> 
				
					<a href="business-<?php echo $ReviewsRow['biz_id'];?>-<?php echo $UserLink;?>">
						<img class="img-avatar" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $ReviewsRow['featured_image'];?>&amp;h=60&amp;w=80&amp;q=100" alt="<?php echo ucfirst($ReviewsRow['business_name']);?>">
					</a>
					
					<div class="review-heading"> <a href="business-<?php echo $ReviewsRow['biz_id'];?>-<?php echo $UserLink;?>"><?php echo ucfirst($ReviewsRow['business_name']);?></a> <span><?php echo $ReviewsRow['rew_date'];?></span>
						<div class="col-rate"> <span class="star-rates"  data-score="<?php echo $ReviewsRow['avg'];?>"></span> </div>
					</div>
					
					<div class="review-body">
						<p><?php echo nl2br($ReviewsRow['review']);?></p>
					</div>
					<!--review-body--> 
          
				</div>
				<!--review-box--> 
        

        <?php
}

	$Reviews->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

if($NumReviews==0){
?>
        <div class="col-note"><?php echo ucfirst($ProfileAuthor);?> n'a pas écrit d'avis sur des services encore !</div>
        <?php } if($NumReviews>5){?>
        <a href="all_reviews-<?php echo $id;?>-<?php echo $ProfileLink;?>"><span class="fa fa-arrow-right"></span> Tout voir (<?php echo $NumReviews;?>)</a>
        <?php }?>
      </div>
      <!--col-desc--> 
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
	
	
	
	
	<div class="col-shadow">
	
		<div class="biz-title-2">
			<h1>Avis sur les produits</h1>
		</div>
		
		<div class="col-desc" id="display-reviews-prod">
		
			<?php
				if($ReviewsProd = $mysqli->query("SELECT * FROM produits WHERE user_id='$id' ORDER BY date DESC LIMIT 10"))
				{
					
					if($ProductNumReviews == 0)
					{
						echo 'nope';
					}
					else
					{
						while($ReviewsRowProd = mysqli_fetch_array($ReviewsProd))
							{
								$prodname = $ReviewsRowProd["produit_name"];
								$prodreview = $ReviewsRowProd["avis"];
								$prodimage = $ReviewsRowProd["featured_image"];
								$prodcat = $ReviewsRowProd["cid"];
								$prodsubcat = $ReviewsRowProd["sid"];
								$datePost = $ReviewsRowProd["date"];
								$prodid = $ReviewsRowProd["idProduit"];
								$note = $ReviewsRowProd["avg"];
								?>
								
								
								<div class= "review-box">
								
										<a href="avis-<?php echo $prodid;?>-<?php echo $prodname;?>">
											<img class="img-avatar" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $prodimage;?>&amp;h=60&amp;w=80&amp;q=100" alt="<?php echo ucfirst($prodname);?>">
										</a>
									
										<div class="review-heading"> <a href="avis-<?php echo $prodid;?>-<?php echo $prodname;?>"><?php echo ucfirst($prodname);?></a> <span><?php getfinaldate($datePost);?></span>
											<div class="col-rate"> <span class="star-rates"  data-score="<?php echo $note;?>"></span> </div>
										</div>
										
										<div class="review-body">
											<p><?php echo nl2br($prodreview);?></p>
										</div>
										<!--review-body--> 
								
								</div>
								
								<?php
							}
					}
					
				}
				else
				{
					echo 'Une erreur est survenue';
				}

			?>
		
		</div>
	</div>
    <!--col-shadow-->

	
	
	
 <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Signets par <?php echo ucfirst($ProfileAuthor);?></h1>
      </div>
<div class="col-desc" id="display-reviews">    
 
 
<?php

//Get bookmark Count

$BookmarkCount = $mysqli->query("SELECT * FROM bookmarks WHERE user_id='$id'");
$NumBookmark = $BookmarkCount->num_rows;

if($PostSql = $mysqli->query("SELECT * FROM bookmarks LEFT JOIN business ON bookmarks.bizid=business.biz_id WHERE bookmarks.user_id=$id ORDER BY bookmarks.bm_id DESC LIMIT 0, 6")){


$CountRows = mysqli_num_rows($PostSql);	

while ($PostRow = mysqli_fetch_array($PostSql)){
	
	$longTitle = stripslashes($PostRow['business_name']);
	$strTitle = strlen ($longTitle);
	if ($strTitle > 25) {
	$PostTitle = substr($longTitle,0,23).'...';
	}else{
	$PostTitle = $longTitle;}
	
	$PostLink = preg_replace("![^a-z0-9]+!i", "-", $longTitle);
	$PostLink = urlencode(strtolower($PostLink));
	
	$longDescription = stripslashes($PostRow['description']);
	$strDescription = strlen ($longDescription);
	if ($strDescription > 70) {
	$Description = substr($longDescription,0,67).'...';
	}else{
	$Description = $longDescription;}
	
	$Tel = stripslashes($PostRow['phone']);
	$City = stripslashes($PostRow['city']);
	$Site = stripslashes($PostRow['website']);
	
	if(!empty($Tel)){
		$Telephone = $Tel;
	}else{
		$Telephone = "N/A";		
	}
?>

<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 col-box">
 
 <div class="grid wow fadeInUp">
 
        
        <a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $PostRow['featured_image'];?>&amp;h=300&amp;w=500&amp;q=100" alt="<?php echo $PostTitle;?>"></a>
    
    <h2><a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><?php echo $PostTitle;?></a></h2>
    <p><?php echo $Description;?></p>
    
    <div class="post-info-bottom">
<div class="col-rate">    
<span class="star-rates"  data-score="<?php echo $PostRow['avg'];?>"></span> <?php echo $PostRow['reviews'];?> Avis
</div>

<div class="info-row"><span class="fa fa-home"></span> <?php echo $City;?></div>
<div class="info-row"><span class="fa fa-phone"></span> <?php echo $Telephone;?></div>
<?php if(!empty($Site)){?>
<div class="info-row"><span class="fa fa-link"></span> <a href="<?php echo $Site;?>" target="_blank">Site Web</a></div>
<?php }else{?>
<div class="info-row"><span class="fa fa-link"></span> N/A</div>
<?php }?>
</div>
 
  </div><!-- /.grid -->  
    
</div><!-- /.col-sm-12 col-xs-12 col-md-4 col-lg-4 -->

<?php     
	}
$PostSql->close();
}else{
     printf("There Seems to be an issue");
}
if($NumBookmark==0){
?>
<div class="col-note"><?php echo ucfirst($ProfileAuthor);?> n'a pas encore de signets.</div>
<?php }if($NumBookmark>6){?>
 <a href="all_bookmarks-<?php echo $id;?>-<?php echo $ProfileLink;?>"><span class="fa fa-arrow-right"></span> Tout voir (<?php echo $NumBookmark;?>)</a>
<?php }?>    
   
    
  </div> <!--col-shadow--> 
   </div>     
    
</div><!--col-md-8-->    
    
  </div>
  <!--container-->
  
<?php include("footer.php");?>