<?php 

		include("header_manage_appointment.php");
		

		?>
		
		
		
		<div class="container container-main">
					
					<div class="col-md-1"></div>
				
					<div class="col-md-10">
				
							<div class="col-shadow">
		
										<div class="right-title">
											<h1 class="pull-left">Vos Rendez-vous
												</h1>
										</div>
			
										
									   
									   <input type="text" id="start" name="start" class="user-iden">
									   
									   <input type="text" id="end" name="end" class="user-iden">
									   
									  <div id='calendar'></div> 
									   
									<div class="container">
											  
											  
											  <!-- Trigger the modal with a button -->
											  <button type="button" class="btn btn-info btn-lg hide" id="modalButt" data-toggle="modal" data-target="#myModal">Open Modal</button>

											  <!-- Modal -->
											  <div class="modal fade" id="myModal" role="dialog">
														<div class="modal-dialog">
														
																  <!-- Modal content-->
																  <div class="modal-content">
																			<div class="modal-header">
																					  <button type="button" id = "close" class="close" data-dismiss="modal">&times;</button>
																					  <h2 class="modal-title">Agenda</h2>
																			</div>
																			
																			
																			<div class="modal-body">
																				
																				<div class="container">
																					
																					<div class="container" >
																							
																								<div class="row">
																								
																									
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
																										<label for="title"> Détails :  
																						
																											<textarea class="form-control" style="width: 100% ; margin-top: 5px" cols="80" type="text" name="inputtitle" id="inputtitle" autofocus> </textarea>
																										
																										</label>
																									</div>
																								</div>
																								
																								<div class="row">	
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																										 
																												<p id="daySave" style="font-size: 1.2em ; margin-top: 6px"> </p>
																												
																									</div>
																									
																								</div>
																								
																								
																								
																								<div class="row" style="margin-top : 20px"> 
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																										<button id="save" class="btn btn-danger btn-lg">Ajouter à l'agenda</button>
																									</div>
																								</div>
																								
																								
																							</div>
																				
																				
																					
																				</div>		
																			</div>
																			
																			
																  </div>
														  
														</div>
											  </div>
											  
											  
											  <!-- Trigger the modal with a button -->
											  <button type="button" class="btn btn-info btn-lg hide" id="modalButtAgenda" data-toggle="modal" data-target="#myModalAgenda">Open Modal</button>

											  <!-- Modal -->
											  <div class="modal fade" id="myModalAgenda" role="dialog">
														<div class="modal-dialog">
														
																  <!-- Modal content-->
																  <div class="modal-content">
																			<div class="modal-header">
																					  <button type="button" id = "close" class="close" data-dismiss="modal">&times;</button>
																					  <h2 class="modal-title">Agenda</h2>
																			</div>
																			
																			
																			<div class="modal-body">
																				
																				<div class="container">
																					
																					<div class="container" >
																							
																								<div class="row">
																								
																									
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
																										<label for="title"> Détails :  
																						
																											<textarea class="form-control" style="width: 100% ; margin-top: 5px" cols="80" type="text" name="display-titleA" id="display-titleA" autofocus> </textarea>
																										
																										</label>
																									</div>
																								</div>
																								
																								<div class="row">	
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																										 
																												<p id="daySaveAgenda" style="font-size: 1.2em ; margin-top: 6px"> </p>
																												
																									</div>
																									
																								</div>
																								
																								
																								
																								<div class="row" style="margin-top : 20px"> 
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																										<button id="delete" class="btn btn-danger btn-lg">Supprimer de l'agenda</button>
																										<button id="modify" class="btn btn-danger btn-lg">OK</button>
																									</div>
																								</div>
																								
																								<input type="hidden" id="eventIDAgenda">
																							
																							</div>
																				
																				
																					
																				</div>		
																			</div>
																			
																			
																  </div>
														  
														</div>
											  </div>
											  
							
											  <!-- Trigger the modal with a button -->
											  <button type="button" class="btn btn-info btn-lg hide" id="modalButtEdit" data-toggle="modal" data-target="#myModalEdit"></button>

											  <!-- Modal -->
											  <div class="modal fade" id="myModalEdit" role="dialog">
														<div class="modal-dialog" style="width: 51%">
														
																  <!-- Modal content-->
																  <div class="modal-content">
																			<div class="modal-header">
																					  <button type="button" id = "close" class="close" data-dismiss="modal">&times;</button>
																					  <h1 class="modal-title" id="owner-name"> </h1>
																			</div>
																			
																			
																			<div class="modal-body">
																			
																							<div style="text-align : center"> 
																							
																									<div id="owner-img" style="padding: 10px" > </div>
																									
																							</div>
																							
																							<div class="container" style="margin-top: 10px">
																							
																								<div class="row">
																								
																									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
																										<div id="business-img" >  </div>
																									</div>
																									
																									<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
																										 
																											
																												<h2 id="biz-name" style="font-size:2em ; font-family: Caviar Dreams ; color: #3F1263; margin-bottom: 10px; "> </h2>
																												<p 	id="day" style="font-size: 1.2em ; margin-top: 6px"> </p>
																												<p 	id="start-time" style="font-size: 1em ; margin-top: 6px"> <p>
																												<p 	id="adr" style="font-size: 1em ; margin-top: 5px"> </p>
																										
																									</div>
																									
																								</div>
																								
																								<div class="row">
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top : 20px">
																										<label for="display-title"> Détails :
																											<textarea style= "width: 100%" cols="80" rows="2" class="form-control" id="display-title" autofocus> 
																											</textarea>
																										</label>
																									</div>
																								</div>
																								
																								<div class="row">
																									<div class="container" style="margin-top : 10px">
																										<button id="edit" class="btn btn-danger btn-lg">Modifier les détails</button>
																										<button id="cancel" class="btn btn-danger btn-lg">Annuler de rendez-vous</button>
																									</div>
																								</div>
																							</div>
																		
																							<input type="hidden" id="eventID">
																							<input type="hidden" id="eventUserID">
																							<input type="hidden" id="eventOwnerID">
																							
																			</div>
																			
																  </div>
														  
														</div>
											  </div>
									  
									  
											<!-- Trigger the modal with a button -->
											  <button type="button" class="btn btn-info btn-lg hide" id="modalButtConfirm" data-toggle="modal" data-target="#myModalConfirm"></button>

											  <!-- Modal -->
											  <div class="modal fade" id="myModalConfirm" role="dialog">
														<div class="modal-dialog" style="width: 51%">
														
																  <!-- Modal content-->
																  <div class="modal-content">
																			<div class="modal-header">
																					  <button type="button" id = "close" class="close" data-dismiss="modal">&times;</button>
																					  <h1 class="modal-title" id="user-name"> </h1>
																			</div>
																			
																			
																			<div class="modal-body">
																			
																							<div style="text-align : center"> 
																							
																									<div id="user-img" style="padding: 10px" > </div>
																									
																							</div>
																							
																							<div class="container" style="margin-top: 10px">
																							
																								<div class="row">
																								
																									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
																										<div id="business-img_owner" >  </div>
																									</div>
																									
																									<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
																										 
																											
																												<h2 id="biz-nameU" style="font-size:2em ; font-family: Caviar Dreams ; color: #3F1263; margin-bottom: 10px; "> </h2>
																												<p 	id="dayU" style="font-size: 1.2em ; margin-top: 6px"> </p>
																												<p 	id="start-timeU" style="font-size: 1em ; margin-top: 6px"> <p>
																												<p 	id="adrU" style="font-size: 1em ; margin-top: 5px"> </p>
																										
																									</div>
																									
																								</div>
																								
																								<div class="row">
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top : 20px">
																										<label for="display-title"> Détails :
																											<textarea style= "width: 100%" cols="80" rows="2" class="form-control" id="display-titleU" autofocus readonly> 
																											</textarea>
																										</label>
																									</div>
																								</div>
																								
																								<div class="row">
																									<div class="container" style="margin-top : 10px">
																										<button id="confirm" class="btn btn-danger btn-lg">Confirmer le rendez-vous</button>
																										<button id="reject" class="btn btn-danger btn-lg">Annuler le rendez-vous</button>
																									</div>
																								</div>
																							</div>
																		
																							<input type="hidden" id="eventIDU">
																							<input type="hidden" id="eventUserIDU">
																							<input type="hidden" id="eventOwnerIDU">
																							
																			</div>
																			
																  </div>
														  
														</div>
											  </div>
											  
											  
											  <!-- Trigger the modal with a button -->
											  <button type="button" class="btn btn-info btn-lg hide" id="modalButtView" data-toggle="modal" data-target="#myModalView"></button>

											  <!-- Modal -->
											  <div class="modal fade" id="myModalView" role="dialog">
														<div class="modal-dialog" style="width: 51%">
														
																  <!-- Modal content-->
																  <div class="modal-content">
																			<div class="modal-header">
																					  <button type="button" id = "close" class="close" data-dismiss="modal">&times;</button>
																					  <h1 class="modal-title" id="user-nameV"> </h1>
																			</div>
																			
																			
																			<div class="modal-body">
																			
																							<div style="text-align : center"> 
																							
																									<div id="user-imgV" style="padding: 10px" > </div>
																									
																							</div>
																							
																							<div class="container" style="margin-top: 10px">
																							
																								<div class="row">
																								
																									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
																										<div id="business-img_ownerV" >  </div>
																									</div>
																									
																									<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
																										 
																											
																												<h2 id="biz-nameUV" style="font-size:2em ; font-family: Caviar Dreams ; color: #3F1263; margin-bottom: 10px; "> </h2>
																												<p 	id="dayUV" style="font-size: 1.2em ; margin-top: 6px"> </p>
																												<p 	id="start-timeUV" style="font-size: 1em ; margin-top: 6px"> <p>
																												<p 	id="adrUV" style="font-size: 1em ; margin-top: 5px"> </p>
																										
																									</div>
																									
																								</div>
																								
																								<div class="row">
																									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top : 20px">
																										<label for="display-title"> Détails :
																											<textarea style= "width: 100%" cols="80" rows="2" class="form-control" id="display-titleUV" autofocus readonly> 
																											</textarea>
																										</label>
																									</div>
																								</div>
																								
																								<div class="row">
																									<div class="container" style="margin-top : 10px">
																										
																										<button id="cancel2" class="btn btn-danger btn-lg">Annuler le rendez-vous</button>
																									</div>
																								</div>
																							</div>
																		
																							<input type="hidden" id="eventIDUV">
																							<input type="hidden" id="eventUserIDUV">
																							<input type="hidden" id="eventOwnerIDUV">
																							
																			</div>
																			
																  </div>
														  
														</div>
											  </div>
									</div>
      
							</div>
							
							
							
							
							

					</div>
				
				<div class="col-md-1">
					
				</div>
				
		</div>