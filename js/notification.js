
//Notification
$(document).ready(function() 
{
		
	waitForMsg();	
	waitForForum() ;
	waitForService() ;
	waitForAvis() ;
	
	in_notif_forum();	 
	in_notif_service();
	in_notif_product();
	in_notif_other_product();
	in_notif_other_forum();
	 
	$(".tab-forum-notif").on("click",".li-notif",function(event)
	{
			var id = $(this).find("[name='hitIdDiscussion']").val();
			
			$.ajax({
						type: "POST",
						url: "update_unread_discussion.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							window.location = "discussion-"+id+"-"+html;
						}
					});
	});
	
	
	$(".tab-forum-notif2").on("click",".li-notif",function(event)
	{
			var id = $(this).find("[name='hitIdDiscussion']").val();
			
			$.ajax({
						type: "POST",
						url: "update_unread_discussion_notif.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							window.location = "discussion-"+id+"-"+html;
						}
					});
	});
	
	
	
	$(".tab-service-notif").on("click",".li-notif",function(event)
	{
			var id = $(this).find("[name='id_biz']").val();
			
			$.ajax({
						type: "POST",
						url: "update_unread_service.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{ 
							window.location = "business-"+id+"-"+html;
						}
					});
	});
	
	
	$(".tab-avis-notif").on("click",".li-notif",function(event)
	{
			var id = $(this).find("[name='hitIdProduct']").val();
			
			$.ajax({
						type: "POST",
						url: "update_unread_prod_review.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{ 
							window.location = "avis-"+id+"-"+html;
						}
					});
	});
	
	$(".tab-avis-notif2").on("click",".li-notif",function(event)
	{
			var id = $(this).find("[name='hitIdProduct']").val();
			
			$.ajax({
						type: "POST",
						url: "update_unread_prod_notif.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{ 
							window.location = "avis-"+id+"-"+html;
						}
					});
	});
	
});

//Wait for new message
function waitForMsg() 
{
 
            $.ajax({
                type: "GET",
                url: "unread_messages.php",
 
                async: true,
                cache: false,
                timeout: 50000,
 
                success: function(data) {
                    addmsg("new", data);
                    setTimeout(
                        waitForMsg,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //addmsg("error", textStatus + " (" + errorThrown + ")");
                    setTimeout(
                        waitForMsg,
                        15000);
                }
            });
 };
 
 
 //Notify new messages
	function addmsg(type, msg) 
	{
			if(msg == '0')
			{
				$('#display_unread_messages').css("visibility", "hidden");
			}
			else
			{
				$('#display_unread_messages').css("visibility", "visible");
				$('#display_unread_messages').html(msg);
			}
             
    }
	
	
//Wait for answers in forum
function waitForForum() 
{
 
            $.ajax({
                type: "GET",
                url: "forum_response.php",
 
                async: true,
                cache: false,
                timeout: 50000,
 
                success: function(data) {
                    addF("new", data);
                    setTimeout(
                        waitForForum,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //addF("error", textStatus + " (" + errorThrown + ")");
                    setTimeout(
                        waitForForum,
                        15000);
                }
            });
 };
	

//Wait for notification concerning user services
function waitForService() 
{
 
            $.ajax({
                type: "GET",
                url: "service_response.php",
 
                async: true,
                cache: false,
                timeout: 50000,
 
                success: function(data) {
                    addS("new", data);
                    setTimeout(
                        waitForService,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //addS("error", textStatus + " (" + errorThrown + ")");
                    setTimeout(
                        waitForService,
                        15000);
                }
            });
 };
 
 //Notify in Service
function addS(type, msg) 
	{
			if(msg == '0')
			{
				$('#display_unread_services').css("visibility", "hidden");
			}
			else
			{
				$('#display_unread_services').css("visibility", "visible");
				$('#display_unread_services').html(msg);
			}
             
    }

//Notify answers in forum
function addF(type, msg) 
	{
			if(msg == '0')
			{
				$('#display_unread_responses').css("visibility", "hidden");
			}
			else
			{
				$('#display_unread_responses').css("visibility", "visible");
				$('#display_unread_responses').html(msg);
			}
             
    }

//Notify answers in avis
function addmsgA(type, msg) 
	{
			if(msg == '0')
			{
				$('#display_unread_comments').css("visibility", "hidden");
			}
			else
			{
				$('#display_unread_comments').css("visibility", "visible");
				$('#display_unread_comments').html(msg);
			}
             
    }	
	
	
//Wait for new reply 
function waitForAvis() 
{
 
            $.ajax({
                type: "GET",
                url: "unread_product_reviews.php",
 
                async: true,
                cache: false,
                timeout: 50000,
 
                success: function(data) {
                    addmsgA("new", data);
                    setTimeout(
                        waitForAvis,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    //addmsgA("error", textStatus + " (" + errorThrown + ")");
                    setTimeout(
                        waitForAvis,
                        15000);
                }
            });
 };


//Description for notification forum
function in_notif_forum()
{
	
		$.ajax({
                type: "GET",
                url: "forum_notify.php",
 
                async: true,
                cache: false,
                timeout: 50000,
				dataType: 'json',
                success: function(data) 
				{
                    display_notif(data);
					//alert(data
					setTimeout(
                        in_notif_forum,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //alert(textStatus);
					setTimeout(
                        in_notif_forum,
                        1000
                    );
                }
            });
    
}	



//Description for notification forum
function in_notif_other_forum()
{
	
		$.ajax({
                type: "GET",
                url: "other_forum_notify.php",
 
                async: true,
                cache: false,
                timeout: 500,
				dataType: 'json',
                success: function(data) 
				{
                    display_notif2(data);
					setTimeout
					(
                        in_notif_other_forum,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //alert(textStatus);
					setTimeout(
                        in_notif_other_forum,
                        1000
                    );
                }
            });
    
}	




//Description for notification forum
function in_notif_product()
{
	
		$.ajax({
                type: "GET",
                url: "product_notify.php",
 
                async: true,
                cache: false,
                timeout: 50000,
				dataType: 'json',
                success: function(data) 
				{// alert(data);
                    display_notif_product(data);
					//alert(data
					setTimeout(
                        in_notif_product,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //alert(textStatus);
					setTimeout(
                        in_notif_product,
                        1000
                    );
                }
            });
			
			
			
    
}	



//Description for notification forum
function in_notif_other_product()
{
	$.ajax({
                type: "GET",
                url: "prod_notify.php",
 
                async: true,
                cache: false,
                timeout: 50000,
				dataType: 'json',
                success: function(data) 
				{ 	
                    display_notif_other_product(data);
					
					setTimeout(
                        in_notif_other_product,
                        1000
                    );
					
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                   //alert(textStatus);
					setTimeout(
                        in_notif_other_product,
                        1000
                    );
					
                }
            });
}

function display_notif_product(data) 
	{
			if(data == '0')
			{
				
			}
			else
			{
				$(".tab-avis-notif").html('<ul class="ul-notif">');
				var i =0;
				var start = 0 ;
				
				while(data.length != 0)
				{
						//alert(data.length);
						if(data[i] == "next")
						{
							var idDiscussion 	= data[start] ;
							var nameDiscussion 	= data[start + 1];
							index = i ;
							
							var list = strip_info_product(data, index, start);
							
							var data = strip_array(data, index, start);
							
							$(".tab-avis-notif").append('<li class="li-notif"> <input type="hidden" value="' + idDiscussion + '" name="hitIdProduct">' + list + '</li>');
							i = -1;
						}
						i++;
					//alert(i);
				}
				$(".tab-avis-notif").append('</ul>');
			}
             
    }
	
	
	
	
	
	function display_notif_other_product(data) 
	{
			if(data == '0')
			{
				
			}
			else
			{
				$(".tab-avis-notif2").html('<ul class="ul-notif">');
				var i =0;
				var start = 0 ;
				
				while(data.length != 0)
				{
						//alert(data.length);
						if(data[i] == "next")
						{
							var idDiscussion 	= data[start] ;
							var nameDiscussion 	= data[start + 1];
							index = i ;
							
							var list = strip_info_product(data, index, start);
							
							var data = strip_array(data, index, start);
							
							$(".tab-avis-notif2").append('<li class="li-notif"> <input type="hidden" value="' + idDiscussion + '" name="hitIdProduct">' + list + '</li>');
							i = -1;
						}
						i++;
					//alert(i);
				}
				$(".tab-avis-notif2").append('</ul>');
			}
             
    }



function display_notif(data) 
	{
			if(data == '0')
			{
				
			}
			else
			{
				$(".tab-forum-notif").html('<ul class="ul-notif">');
				var i =0;
				var start = 0 ;
				
				while(data.length != 0)
				{
						//alert(data.length);
						if(data[i] == "next")
						{
							var idDiscussion 	= data[start] ;
							var nameDiscussion 	= data[start + 1];
							index = i ;
							
							var list = strip_info(data, index, start);
							
							var data = strip_array(data, index, start);
							
							$(".tab-forum-notif").append('<li class="li-notif"> <input type="hidden" value="' + idDiscussion + '" name="hitIdDiscussion">' + list + '</li>');
							i = -1;
						}
						i++;
					//alert(i);
				}
				$(".tab-forum-notif").append('</ul>');
			}
             
    }
	
	
	function display_notif2(data) 
	{
			if(data == '0')
			{
				
			}
			else
			{
				$(".tab-forum-notif2").html('<ul class="ul-notif">');
				var i =0;
				var start = 0 ;
				
				while(data.length != 0)
				{
						//alert(data.length);
						if(data[i] == "next")
						{
							var idDiscussion 	= data[start] ;
							var nameDiscussion 	= data[start + 1];
							index = i ;
							
							var list = strip_info(data, index, start);
							
							var data = strip_array(data, index, start);
							
							$(".tab-forum-notif2").append('<li class="li-notif"> <input type="hidden" value="' + idDiscussion + '" name="hitIdDiscussion">' + list + '</li>');
							i = -1;
						}
						i++;
					//alert(i);
				}
				$(".tab-forum-notif2").append('</ul>');
			}
             
    }
	
function strip_info(data, index,start)
{
	var nameDiscussion 	= data[start + 1];
	var username = '<span class="sp">' + data[start + 2] + '</span>';
	//alert(username);
	for(var i = start+3 ; i<index ; i++)
	{
		if(i == index-1)
		{
				username = username + " et " + '<span class="sp">' + data[i] + '</span>' ;
		}
		else
		{
				username = username + ", " + '<span class="sp">' + data[i] + '</span>';
		}		
	}
	
	var diff  = index - start -3;
	
	if(diff == 0)
	{
		var list = username + " a répondu dans la discussion " + '<span class="sp">' + nameDiscussion + '</span>'; 
	}
	else
	{
		var list = username + " ont répondu dans la discussion " + '<span class="sp">' + nameDiscussion + '</span>';
	}
	
	return list ;
}

function strip_info_product(data, index,start)
{
	var nameDiscussion 	= data[start + 1];
	var username = '<span class="sp">' + data[start + 2] + '</span>';
	//alert(username);
	for(var i = start+3 ; i<index ; i++)
	{
		if(i == index-1)
		{
				username = username + " et " + '<span class="sp">' + data[i] + '</span>' ;
		}
		else
		{
				username = username + ", " + '<span class="sp">' + data[i] + '</span>';
		}		
	}
	
	var diff  = index - start -3;
	
	if(diff == 0)
	{
		var list = username + " a commenté votre post " + '<span class="sp">' + nameDiscussion + '</span>'; 
	}
	else
	{
		var list = username + " ont commenté votre post " + '<span class="sp">' + nameDiscussion + '</span>';
	}
	
	return list ;
}

function strip_array(data, index, start)
{
	var newData = data.splice(start,index+1) ;
	//alert(data);
	return data ;
}






/////In notif service
//Description for notification forum
function in_notif_service()
{
	
		$.ajax({
                type: "GET",
                url: "service_notify.php",
 
                async: true,
                cache: false,
                timeout: 50000,
				dataType: 'json',
                success: function(data) 
				{
                    display_notif_service(data);
					
					setTimeout(
                        in_notif_service,
                        1000
                    );
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) 
				{
                    //alert(textStatus);
					setTimeout(
                        in_notif_service,
                        1000
                    );
                }
            });
    
}	


function display_notif_service(data) 
	{
			if(data == '0')
			{
				
			}
			else
			{
				$(".tab-service-notif").html('<ul class="ul-notif">');
				var i =0;
				var start = 0 ;
				
				while(data.length != 0)
				{
						//alert(data.length);
						if(data[i] == "next")
						{
							var idDiscussion 	= data[start] ;
							var nameDiscussion 	= data[start + 1];
							index = i ;
							
							var list = strip_info_service(data, index, start);
							
							var data = strip_array(data, index, start);
							
							$(".tab-service-notif").append('<li class="li-notif"> <input type="hidden" value="' + idDiscussion + '" name="id_biz">' + list + '</li>');
							i = -1;
						}
						i++;
					//alert(i);
				}
				$(".tab-service-notif").append('</ul>');
			}
             
    }
	
function strip_info_service(data, index,start)
{
	var nameDiscussion 	= data[start + 1];
	var username = '<span class="sp">' + data[start + 2] + '</span>';
	//alert(username);
	for(var i = start+3 ; i<index ; i++)
	{
		if(i == index-1)
		{
				username = username + " et " + '<span class="sp">' + data[i] + '</span>' ;
		}
		else
		{
				username = username + ", " + '<span class="sp">' + data[i] + '</span>';
		}		
	}
	
	var diff  = index - start -3;
	
	if(diff == 0)
	{
		var list = username + " a écrit un avis sur  " + '<span class="sp">' + nameDiscussion + '</span>'; 
	}
	else
	{
		var list = username + " ont écrit un avis sur " + '<span class="sp">' + nameDiscussion + '</span>';
	}
	
	return list ;
}