$(document).ready(function() 
{
	
	$(window).scroll(function()
	{
				if ($(window).scrollTop() == $(document).height() - $(window).height())
				{
						
								$('#loader').show();
								
								var term 		= $("[name='term_value']").val();
								var category 	= $("[name='category']").val();
								var city 		= $("[name='city']").val();
								var type 		= $("[name='type']").val();
								
								//alert("last_id : " + last_id + " term : " + term + " category : " + category + " type = "+ type + " city  : "+city);
								
								// If search concerns product reviews
								if(type == '1')
								{
										$.ajax({
											type: "POST",
											url: "load_more_products.php",
											data:{ last_id : last_id,
												   term : term,
												   category : category
												 }, 
											success: function(data)
											{
												
												$('#loader').hide();
												$("#display_products").append(data);
											}
										});
								}
								// If search concerns business
								else if(type == '2')
								{
										$.ajax({
											type: "POST",
											url: "load_more_business.php",
											data:{ last_id : last_id,
												   term : term,
												   category : category
												 }, 
											success: function(data)
											{	
												$('#loader').hide();
												$("#display_products").append(data);
											}
										});
								}
								// If search concerns discussions
								else if(type == '3')
								{
										$.ajax({
											type: "POST",
											url: "load_more_discussions.php",
											data:{ last_id : last_id,
												   term : term,
												   category : category
												 }, 
											success: function(data)
											{
												$('#loader').hide();
												$("#display_discussion").append(data);
											}
										});
								}
								// If search concerns experts
								else if(type == '4')
								{
										$.ajax({
											type: "POST",
											url: "load_more_experts.php",
											data:{ last_id : last_id,
												   term : term,
												   city : city
												 }, 
											success: function(data)
											{
												//alert(data);
												$('#loader').hide();
												$("#display_experts").append(data);
											}
										});
								}
	
				}
	});
	
	
});