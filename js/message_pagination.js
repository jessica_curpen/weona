$(document).ready(function() 
{

		$(".message-track").scroll(function()
		{
				if ($(".message-track").scrollTop() == 0)
				{
					 	
						$('#loader').css("visibility","visible");
						
						$.ajax({
								type: "POST",
								url: "load_previous_message.php",
								data:{ last_id : first_id,
									   idChatter : idChatter									   
									 }, 
								success: function(data)
								{
									
									$('#loader').css("visibility","hidden");
									$('#msg').prepend(data);
									$(".message-track").scrollTop(200);
									
								},
								error: function() 
								{
									$('#msg').prepend("Une erreur est survenue");
								}
							});
						
				}
		});

});