$(document).ready(function() 
{
	
		$("#inputPassword").focusout(function()
		{
			var val = $(this).val();
			
			if(val.length  < 6)
			{
				$("#syntaxFalse").show();
				$("#syntaxTrue").hide();
				$("#syntax-msg").show();
				$("#syntax-msg").html("Votre mot de passe doit contenir 6 caractères");
			}
			else
			{
				$("#syntaxFalse").hide();
				$("#syntaxTrue").show();
				$("#syntax-msg").hide();
			}
		});
		
		
		
		$("#inputConfirmPassword").focusout(function()
		{
			var val 	 = $(this).val();
			var password = $("#inputPassword").val();
			
			if(val != password )
			{
				$("#matchFalse").show();
				$("#matchTrue").hide();
				$("#match-msg").show();
				$("#match-msg").html("Vos mots de passe ne correspondent pas");
			}
			else
			{
				$("#matchFalse").hide();
				$("#matchTrue").show();
				$("#match-msg").hide();
			}
		});
		
		
		$("#comment").click(function()
		{
			
			if (!$.trim($("[name='commentArea']").val()))
			{
				alert("Votre réponse est vide");
			}
			else
			{
				var id = $("[name='idProd']").val(); 
				var comment = $("[name='commentArea']").val(); 
				
				$.ajax({
						type: "POST",
						url: "add_comment.php",
						data:{id : id,
							  reply : comment
 							 }, 
						cache: false,
						success: function(html)
						{
							
							location.reload();
						}
					});
			}
			
		});

		
		
		$("#confirmClose").click(function()
		{
			var id = $("[name='id_discu']").val(); 
			
			$.ajax({
						type: "POST",
						url: "close_discussion.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							location.reload();
						}
					});
		});

		$(".business").click(function()
		{
			var id = $(this).find("[name='service-val']").val();
			$("[name='final-service-val']").val(id);
			
			$(".business").css("border", "5px solid white");
			$(this).css("border", "5px solid green");
		});
		
		
		$("#goB").click(function()
		{

			if( $("[name='final-service-val']").val().length === 0 )
			{
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$("#output").addClass("bg-warning");
				$("#output").html("Veuillez choisir un service");
				
				setTimeout(function(){
				  $("#output").removeClass("bg-warning").html(" ");
				}, 3000);
			}
			else
			{
				var bizID = $("[name='final-service-val']").val();
				var ownerID = $("[name='idOwner']").val();
				window.location = "appointment-" + bizID +"-" + ownerID ;
			}
			
			
		});
		
		
		$("#follow").click(function()
		{
			var follower = $("[name='follower']").val();
			var followed = $("[name='followed']").val();
			
			$.ajax({
						type: "POST",
						url: "add_follower.php",
						data:{follower : follower,
							  followed : followed
 							 }, 
						cache: false,
						success: function(html)
						{
							
							if(html == 'ok')
							{
								location.reload();
							}
						}
					});
		});
		
		$(".usefullnessII").click(function()
		{
			
			var id = $(this).find("[name='idSUser']").val();
			var idProd = $("[name='idProd']").val();
			var idauteur = $(this).find("[name='idAut']").val();
			
			var $t = $(this);
			//alert(id);alert(idProd);alert(idauteur);
			//alert(idProd);
			$.ajax({
						type: "POST",
						url: "submit_like.php",
						data:{id  : id,
							  idauteur  : idauteur,
							  idProd : idProd
 							 }, 
						cache: false,
						success: function(html)
						{
							if(html == 'true')
							{
								
								$t.addClass("hide");
								
								var $p = $t.parent().children("#liked") ;
								$p.removeClass("hide");
								$p.show("fast");
							}
							else
							{
								//alert(html);
							}
							
						}
					});
			
		});
	
		$(".usefullness").click(function()
		{
			//alert("clicked");
			var id = $(this).find("[name='idSUser']").val();
			var idProd = $(this).find("[name='idProd']").val();
			var idauteur = $(this).find("[name='idAut']").val();
			
			var $t = $(this);
			//alert(id);alert(idProd);alert(idauteur);
			$.ajax({
						type: "POST",
						url: "submit_like.php",
						data:{id  : id,
							  idauteur  : idauteur,
							  idProd : idProd
 							 }, 
						cache: false,
						success: function(html)
						{
							if(html == 'true')
							{
								//alert("ready");
								$t.hide();
								$t.parent().children(".useful2").show("fast");
								
								var $numLikes = $t.parent().parent().children(".post-info-bottom").children("#num_likesID");
								var num = $numLikes.children(".subnumLikes").html();
								num ++;
								$numLikes.children(".subnumLikes").html(num);
								$numLikes.removeClass("hide");
							}
							else
							{
								alert(html);
							}
							
						}
					});
			
		});
	
		$('#oT').click(function()
		{
			$(".dis-head").css("color", "#264348");
			$(".dis-head").css("font-size", "1.2em");
			
			$("#o").css("font-size", "0.9em");
			$("#o").css("color", "#C93756");
		});
		
		$('#tt').click(function()
		{
			$(".dis-head").css("color", "#264348");
			$(".dis-head").css("font-size", "1.2em");
			
			$("#t").css("font-size", "0.9em");
			$("#t").css("color", "#C93756");
		});
		
		$("#reply-to").click(function(event)
		{
			
			if (!$.trim($("[name='reply-user']").val())) 
			{
				alert("Votre réponse est vide");	
			}
			else
			{
				var reply = $("[name='reply-user']").val(); 
				var id = $("[name='id_discu']").val(); 
				var idU = $("[name='id_U']").val(); 
				
				$.ajax({
						type: "POST",
						url: "add_reply.php",
						data:{reply : reply,
							  id : id,
							  idU : idU
 							 }, 
						cache: false,
						success: function(html)
						{
							$("#output").html("Votre réponse est enregistrée");
							location.reload();
						}
					});
			}
			
		});
		
		
		$(".discussion-title").mouseenter(function(event)
		{
			$(this).css("text-decoration","underline");
		});
		
		$(".discussion-title").mouseleave(function(event)
		{
			$(this).css("text-decoration","none");
		});
		
	
		$("#submit-disc").click(function()
		{
			
			/*var cat_id = $("[name='choix_cat2']").val(); 
			var title = $("[name='sujet-disc']").val(); 
			var description = $("[name='description-disc']").val();
			
			//alert(cat_id); alert(title); alert(description);
			
			$.ajax({
						type: "POST",
						url: "add_discussion.php",
						data:{cat_id : cat_id,
							  title : title,
							  description : description
 							 }, 
						cache: false,
						success: function(html)
						{
							alert(html);
							if(html == 'ok')
							{
								window.location = "forum.php";
							}
						}
					});*/
					
			$("#SubmitForm").submit();
		});
		
		
		$(".drop").on("mouseenter",".li-style",function(event)
		{
			$(this).css("background-color","#EEEEEE");
		});
		
		
		$(".drop").on("mouseleave",".li-style",function(event)
		{
			$(this).css("background-color","#fff");
		});
		
		
		$(".drop").on("click",".li-style",function(event)
		{
			$(".drop").css("display", "none");
			
			var id = $(this).find("[name='cat_id']").val();
			
			var $name = $(this).children().html();
			var decoded = $("<div/>").html($name).text();
			
			$("[name='choix_cat']").val(decoded);
			$("[name='choix_cat2']").val(id);
		});
		
		
		$(".wrap").click(function()
		{
			$(".drop").css("display", "none");
			var id = $(this).find("[name='cat_id']").val();
			
			$.ajax({
						type: "POST",
						url: "get_sub_cat.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							$(".drop").html("");
							$(".drop").html(html);
						}
					});
					
			$(this).children(".drop").show("fast");
		});
		
		$(".wrap").mouseleave(function()
		{
			$(".drop").css("display", "none");
		});

		$("#start-discussion").click(function()
		{
			window.location = "new_discussion.php";
		});
	
	
		$(".span-text").on("mouseenter",".highlight", function(event) 
		{
			$(this).css( "background-color", "#763568" );
			$(this).children(".img-close").attr("src", "images/delete_white.png");
		});
		
		$(".span-text").on("mouseleave",".highlight", function(event) 
		{
			$(this).css( "background-color", "#FCC9B9" );
			$(this).children(".img-close").attr("src", "images/delete.png");
		});
		
		$(".span-text").on("click",".highlight", function(event) 
		{
			var $name = $(this).parent().parent().children(".box-text").html();
			var decoded = $("<div/>").html($name).text();
			
			$(this).parent().parent().remove();
			
			var existent = $('#search-user').val();
			existent = existent.replace(decoded + ',' ,'');
			
			$('#search-user').val(existent);
			
			if(existent == "")
			{
				$(".message-area").css("display","none");
				$(".send-messages-option").css("display","none");
			}

		});
	
	
		
		$(".userNameTextBox2").keyup(function() 
		{
			var searchbox = $(this).val();
			var dataString = 'searchword='+ searchbox;
			
			if(searchbox=='')
			{
				$("#display-suggestions").html("");
			}
			else
			{
					$.ajax({
							type: "POST",
							url: "searchUser.php",
							data: dataString,
							cache: false,
							success: function(html)
							{
								$("#display-suggestions").html(html).show();
							}
					});
			}return false; 
			
		});
		
		
		
		
		$( "#display-suggestions" ).on( "click", ".dis-user", function( event ) 
		{
			var $clicked = $(event.target);
			var $name = $clicked.find('.name').html();
			var decoded = $("<div/>").html($name).text();
			
			var existent = $(".userNameTextBox").val();
			
			if(existent.indexOf(",") > -1)
			{
				existent = existent.substring(0, existent.lastIndexOf(',')+1);
				$('#search-user').val(existent + decoded + ',');
			}
			else
			{
				$('#search-user').val(decoded + ',');
			}
			
			$('#search-user2').val("");
			var span = '<div class="box-span-text"><div class="box-text">'  + decoded + '</div><div class="delete-user"><div class="highlight"><img class="img-close" src="images/delete.png" style="width: 10px; height: 10px;"></div></div></div>'
			$(".span-text").append(span);
			
			$(".message-area").css("display","block");
			$(".send-messages-option").css("display","block");
			
		});
		
		
		
		$( "#display-suggestions" ).on( "click", "img", function( event ) 
		{
			var $name = $(this).parent().parent().parent().parent().find('.name').html();
			var decoded = $("<div/>").html($name).text();
			
			var existent = $(".userNameTextBox").val();
			
			if(existent.indexOf(",") > -1)
			{
				existent = existent.substring(0, existent.lastIndexOf(',')+1);
				$('#search-user').val(existent + decoded + ',');
			}
			else
			{
				$('#search-user').val(decoded + ',');
			}
			
			$('#search-user2').val("");
			var span = '<div class="box-span-text"><div class="box-text">'  + decoded + '</div><div class="delete-user"><div class="highlight"><img class="img-close" src="images/delete.png" style="width: 10px; height: 10px;"></div></div></div>'
			$(".span-text").append(span);
			
			$(".message-area").css("display","block");
			$(".send-messages-option").css("display","block");
			
			jQuery("#display-suggestions").fadeOut("fast");
			return false;
		});
		

		$( "#display-suggestions" ).on( "click", ".name", function( event ) 
		{
			
			var $name = $(this).parent().parent().parent().parent().find('.name').html();
			var decoded = $("<div/>").html($name).text();
			
			var existent = $(".userNameTextBox").val();
			if(existent.indexOf(",") > -1)
			{
				existent = existent.substring(0, existent.lastIndexOf(',')+1);
				$('#search-user').val(existent + ' ' + decoded + ',');
			}
			else
			{
				
				$('#search-user').val(decoded + ',');
			}
			
			$('#search-user2').val("");
			var span = '<div class="box-span-text"><div class="box-text">'  + decoded + '</div><div class="delete-user"><div class="highlight"><img class="img-close" src="images/delete.png" style="width: 10px; height: 10px;"></div></div></div>'
			$(".span-text").append(span);
			
			$(".message-area").css("display","block");
			$(".send-messages-option").css("display","block");
			
			jQuery("#display-suggestions").fadeOut("fast");
			return false;
		});
		
		
		$(document).click(function(e) 
		{
			var $clicked = $(e.target);
			if (! $clicked.hasClass("userNameTextBox"))
			{
				jQuery("#display-suggestions").fadeOut("fast"); 
			}
		});
		
		
		$("#send-message").click(function()
		{
			var message = $("[name='message']").val();
			var utilisateurs = $("[name='userName']").val();
			
			
			$.ajax({
						type: "POST",
						url: "send_message.php",
						data:{message : message,
							  utilisateurs : utilisateurs
 							 }, 
						cache: false,
						success: function(html)
						{
								location.reload();
							
						}
					});
		});
		
		
		
		$("#new-message").click(function()
		{
			$("#search-user").val("");
			$("#search-user").val("");
			$("#recipients").css("display","block");
			$(".message-track").empty();
			$(".message-track").css("display","none");
		});
		
		
		
		
		$( ".conversation" ).on("click", function( event ) 
		{
			var $clicked = $(event.target);
			var id = $(this).find("[name='user_identifiant']").val();
			
			$("#msg").html("");
			$(".message-track").css("display", "block");
			$("#msg").css("display", "block");
			
			$.ajax({
						type: "POST",
						url: "user.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							$("[name='userName']").val(html + ',');
							$(".left-side h1").html(html);
						}
					});
			
			
			$("#recipients").css("display","none");
			
			$.ajax({
						type: "POST",
						url: "load_conversation.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							$("#msg").html(" ");
							$("#msg").html(html);
						}
					});
			$(".message-track").animate({ scrollTop: $(document).height() }, "slow");
		});
		
		
		
		$(document).ready(function() 
		{
			var id = $(this).find("[name='user_identifiant']").val();
			
			
			if(id == null)
			{
					$(".message-track").css("display","none");
					$(".message-area").css("display","none");
					$(".send-messages-option").css("display","none");
			}
			else
			{
				$.ajax({
						type: "POST",
						url: "user.php",
						data:{id : id
 							 }, 
						cache: false,
						success: function(html)
						{
							$("[name='userName']").val(html + ',');
							$(".left-side h1").html(html);
						}
					});
					
				id = $("[name='top-message").val();
				
				
				
				if(id > 0)
				{
				
					$.ajax({
							type: "POST",
							url: "load_top_conversation.php",
							data:{id : id
								 }, 
							cache: false,
							success: function(html)
							{
								$("#msg").html(" ");
								$("#msg").html(html);
							}
						});
					
					$(".message-track").animate({ scrollTop: $(document).height() }, "fast");
					
				}
			}

		});
});