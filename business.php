<?php 
	
	include("header_business.php");

	$add1 = $BizRow['address_1'];
	$add2 = $BizRow['address_2'];

	$Tel = stripslashes($BizRow['phone']);
	$City = stripslashes($BizRow['city']);
	$Site = stripslashes($BizRow['website']);
	$Email = stripslashes($BizRow['email']);
	$Menu  = stripslashes($BizRow['menu']);
	$Facebook  = stripslashes($BizRow['facebook']);
	$Twitter  = stripslashes($BizRow['twitter']);
	$Pinterest  = stripslashes($BizRow['pinterest']);
	$Tags  = stripslashes($BizRow['tags']);
	$BizUid  = stripslashes($BizRow['biz_user']);

	$biznumber = $BizRow['unique_biz'] ;

	$BizLink = preg_replace("![^a-z0-9]+!i", "-", $BizName);
	$bizLink = urlencode(strtolower($BizName));

	$Latitude = stripslashes($BizRow['latitude']);
	$Longitude = stripslashes($BizRow['longitude']);

	$Category = stripslashes($BizRow['cid']);
	
	if(!empty($Tel)){
		$Telephone = $Tel;
	}else{
		$Telephone = "N/A";		
	}

	$add = urlencode($add1.", ".$add2);
	$city = urlencode($BizRow['city']);
	$country  = urlencode($Settings['county']);
	$zip = urlencode($Settings['zip']);

	if(empty($Latitude))
	{

			$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$add.',+'.$city.',+'.$country.'&sensor=false');

			$output= json_decode($geocode); //Store values in variable

			if($output->status == 'OK')
			{ // Check if address is available or not
					$lat = $output->results[0]->geometry->location->lat; //Returns Latitude
					$long = $output->results[0]->geometry->location->lng; // Returns Longitude
			}
	}
	else
	{

			$lat = $Latitude;
			$long = $Longitude; 	
		
	}

	//Get Review Count

	//$ReviewsCount = $mysqli->query("SELECT * FROM reviews WHERE rev_active=1 AND b_id='$id'");
	$ReviewsCount = $mysqli->query("SELECT * FROM reviews WHERE b_id='$id'");
	$NumReviews = $ReviewsCount->num_rows;

	//Get Opening time today

	$Today = date("D");
	$UniqId = stripslashes($BizRow['unique_biz']);

	$GetToday = $mysqli->query("SELECT * FROM hours WHERE unique_hours='$UniqId' AND day='$Today'");
	$TodayTime = mysqli_fetch_array($GetToday);
	$OpenTime =  $TodayTime['day'];
?>
<script src="js/jquery.form.js"></script>
<script>
$(function(){
$('#rate-biz').raty({readOnly: true, score:<?php echo $BizRow['avg'];?>});
});

$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
		$('html, body').animate({scrollTop: '0px'}, 300);
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

$(function() {
$(".bookmarks").click(function() 
{
var id = $(this).data("id");
var name = $(this).data("name");
var dataString = 'id='+ id ;
var parent = $(this);

if (name=='bookmarks')
{
$(this).fadeIn(200).html;
$.ajax({
type: "POST",
url: "save_bookmarks.php",
data: dataString,
cache: false,

success: function(html)
{
parent.html(html);
}
});
}
return false;
});
});
</script>

<div class="container container-biz">
  <div class="col-md-8">
    <div class="col-shadow">
      <div class="biz-title">
        <h1><?php echo $BizRow['business_name'];?></h1>
        <span><span class="fa fa-home"></span> <?php echo $add1.", "; if(!empty($add2)){ echo $add2.", "; } echo $City?></span> </div>
      <!--biz-title--> 
      <img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $BizRow['featured_image'];?>&amp;h=400&amp;w=900&amp;q=100" alt="<?php echo $BizRow['business_name'];?>">
      <div class="col-desc col-boader"> <a class="social-buttons btn-fb" href="javascript:void(0);" onclick="popup('http://www.facebook.com/share.php?u=http://<?php echo $SiteLink;?>/business-<?php echo $BizRow['biz_id'];?>-<?php echo $BizLink;?>&amp;title=<?php echo urlencode(ucfirst($BizName));?>')"><span class="fa fa-facebook"></span> Facebook</a> <a class="social-buttons btn-tweet" href="javascript:void(0);" onclick="popup('http://twitter.com/home?status=<?php echo urlencode(ucfirst($BizName));?>+http://<?php echo $SiteLink;?>/business-<?php echo $BizRow['biz_id'];?>-<?php echo $BizLink;?>')"><span class="fa fa-twitter"></span> Twitter</a> <a class="social-buttons btn-pinit" href="javascript:void(0);" onclick="popup('http://pinterest.com/pin/create/bookmarklet/?media=http://<?php echo $SiteLink;?>/uploads/<?php echo $BizRow['featured_image'];?>&amp;url=http://<?php echo $SiteLink;?>/business-<?php echo $BizRow['biz_id'];?>-<?php echo $BizLink;?>&amp;is_video=false&description=<?php echo urlencode(ucfirst($BizName));?>')"><span class="fa fa-pinterest"></span> Pinterest</a>
        

		
		<?php

$CheckBookmarks = $mysqli->query("SELECT * FROM bookmarks WHERE bizid='$id' AND user_id='$UserId'");

$BookmarkCount = $CheckBookmarks->num_rows; 


if(!isset($_SESSION['username'])){	
?>
        <a class="social-buttons btn-bm" href="login"><span class="fa fa-bookmark"></span> Signet (<?php echo $BizRow['bookmarks'];?>)</a>
        <?php }else{

if($BookmarkCount==0){
	$BmText = "Bookmark";
}else{
	$BmText = "Bookmarked";
}
?>
        <a class="social-buttons btn-bm bookmarks" data-id="<?php echo $id;?>" data-name="bookmarks" href="#"><span class="fa fa-bookmark"></span> <?php echo $BmText;?> (<?php echo $BizRow['bookmarks'];?>)</a>
        <?php } if($BizUid==$UserId){?>
        <a class="pull-right edit-link-1" href="edit_basic-<?php echo $id;?>"><span class="fa fa-edit"></span> Modifier</a>
        <?php }?>
      </div>
      <!--col-desc-->
      
	  <div class="col-desc">
			<h2>Professionel</h2>
			<?php
			
					$getOwnerInfo = $mysqli->query("SELECT * FROM users WHERE user_id='$BizUid'");
					$OwnerInfo = mysqli_fetch_array($getOwnerInfo);
			
					if (empty($OwnerInfo["avatar"]))
					{ 
						$OwnerPic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
					}
					elseif (!empty($OwnerInfo["avatar"]))
					{
						$OwnerPic =  'http://'.$SiteLink.'/avatars/'.$OwnerInfo["avatar"];
					}
					
					$ownername = $OwnerInfo["username"];
					
					if($BizUid==$UserId)
					{
						
					?>
						<a href="profile"><img src="thumbs.php?src=<?php echo $OwnerPic;?>&amp;h=60&amp;w=60&amp;q=100" class="img-circle"></a>
						<a href="profile"> <?php echo $OwnerInfo["username"] ;?> </a><br>
					<?php
					}
					else
					{
						
						?>
						<a href="user_profile-<?php echo $BizUid;?>-<?php echo $ownername;?>"><img src="thumbs.php?src=<?php echo $OwnerPic;?>&amp;h=60&amp;w=60&amp;q=100" class="img-circle"></a>
						<a href="user_profile-<?php echo $BizUid;?>-<?php echo $ownername;?>"> <?php echo $ownername ;?> </a><br>
						<a href="appointment-<?php echo $biznumber;?>-<?php echo $BizUid;?>"> Prendre Rendez-vous</a>
						<?php
					}?>
					
	  </div>
	  
      <div class="col-desc">
        <h2>Information sur le service</h2>
        <?php if(!empty($OpenTime)){?>
        <div class="info-biz-row"><span class="fa fa-clock-o"></span> Aujourd'hui: <?php echo $TodayTime['open_from'];?> - <?php echo $TodayTime['open_till'];?></div>
        <?php }else{?>
        <div class="info-biz-row"><span class="fa fa-clock-o"></span> Aujourd'hui: N/A</div>
        <?php }?>
        <div class="info-biz-row"><span class="fa fa-phone"></span> <?php echo $Telephone;?></div>
        <div class="info-biz-row"><span class="fa fa-link"></span>
          <?php if(!empty($Site)){?>
          <a href="<?php echo $Site;?>" target="_blank">Site Web</a>
          <?php }else{?>
          N/A
          <?php }?>
          &nbsp;&nbsp;<span class="fa fa-envelope"></span>
          <?php if(!empty($Email)){?>
          <a href="mailto:<?php echo $Email;?>?Subject=<?php echo urlencode($BizRow['business_name']);?>" target="_blank">Email</a>
          <?php }else{?>
          N/A
          <?php }?>
          &nbsp;&nbsp;<span class="fa fa-facebook-square"></span>
          <?php if(!empty($Facebook)){?>
          <a href="<?php echo $Facebook;?>" target="_blank">Facebook</a>
          <?php }else{?>
          N/A
          <?php }?>
          &nbsp;&nbsp;<span class="fa fa-twitter-square"></span>
          <?php if(!empty($Twitter)){?>
          <a href="<?php echo $Twitter;?>" target="_blank">Twitter</a>
          <?php }else{?>
          N/A
          <?php }?>
          &nbsp;&nbsp;<span class="fa fa-pinterest-square"></span>
          <?php if(!empty($Pinterest)){?>
          <a href="<?php echo $Pinterest;?>" target="_blank">Pinterest</a>
          <?php }else{?>
          N/A
          <?php }?>
        </div>
        <?php if(!empty($Menu)){?>
        <div class="info-biz-row"><span class="fa fa-book"></span> <a href="<?php echo $Menu;?>" target="_blank">Menu</a></div>
        <?php }?>
        <?php if(!empty($Tags)){?>
        <div class="info-biz-row"><span class="fa fa-tag"></span> Mots Clés:
          <?php
$arr=explode(",",$Tags);
foreach ($arr as $TagValue) {
	$ShowTags = preg_replace("![^a-z0-9]+!i", "", $TagValue);
	$ShowTags = strtolower($ShowTags);
	echo '<a href="tags-'.$ShowTags.'">'.$TagValue.'</a>&nbsp;';
}
?>
        </div>
        <?php }?>
        <div class="info-biz-row"><span class="fa fa-chevron-circle-right"></span> Categorie: <a href="category-<?php echo $BizRow['cid'];?>-<?php echo $CLink;?>"><?php echo $CName." - ".$SId;?></a>
          <?php
        if($SId>0){
			
		if($SubCat = $mysqli->query("SELECT * FROM categories WHERE cat_id='$SId'")){

    		$SubCatRow = mysqli_fetch_array($SubCat);
			
			$SCName = $SubCatRow['category'];
			$SCLink = preg_replace("![^a-z0-9]+!i", "-", $SCName);
			$SCLink = urlencode($SCLink);
			$SCLink = strtolower($SCLink);	
		?>
          / <a href="subcategory-<?php echo $SubCatRow['cat_id'];?>-<?php echo $SCLink;?>"><?php echo $SCName;?></a>
          <?php
		
		$SubCat->close();
	
		}else{
    
		printf("There Seems to be an issue");
		}	
			
		}
		
		?>
        </div>
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
    <div class="col-shadow">
      <div class="col-desc">
        <h2>Description</h2>
        <p><?php echo nl2br($BizRow['description']);?></p>
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
    <div class="col-shadow">
      <div class="col-desc">
        <h2>Photos</h2>
        <div class="row">
          <?php

$CountPhotos = $mysqli->query("SELECT * FROM galleries WHERE uniq='$UniqId'");

$NumPhotos = $CountPhotos->num_rows;


if($Photos = $mysqli->query("SELECT * FROM galleries WHERE uniq='$UniqId' ORDER BY img_id DESC LIMIT 4")){
	
	
	while($PhotosRow = mysqli_fetch_array($Photos)){
		

?>
          <div class="col-sm-6 col-md-3"> <a href="http://<?php echo $SiteLink;?>/gallery/<?php echo $PhotosRow['image'];?>" class="thumbnail" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $BizRow['business_name'];?> Photos"> <img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/gallery/<?php echo $PhotosRow['image'];?>&amp;h=400&amp;w=500&amp;q=100" alt="<?php echo $BizRow['business_name'];?> Photos"> </a> </div>
          <?php
}

	$Photos->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

?>
        </div>
        <!--row-->
        <?php if($NumPhotos==0){?>
        <div class="col-note"> Il n'y a aucune autre photo pour le moment. Avez des photos de <?php echo $BizRow['business_name'];?>? <a href="upload_photo-<?php echo $UniqId;?>"><span class="fa fa-cloud-upload"></span> Importer</a> </div>
        <?php  }else if($NumPhotos>0) {?>
        <div class="col-note"><a href="photos-<?php echo $UniqId;?>">Voir toutes les photos (<?php echo $NumPhotos;?>)</a></div>
        <?php }?>
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
    <?php if(!empty($Ad2)){?>
    <div class="col-shadow col-ads"> <?php echo $Ad2;?> </div>
    <!--col-shadow-->
    <?php 
	}
	
	if($BizUid!=$UserId)
	{
	
	?>
    <div id="output"></div>
    <div class="col-shadow" id="col-review-box">
      <div class="biz-title-2">
        <h1>Voulez vous écrire un avis sur ce service ?</h1>
      </div>
      <div class="col-desc">
        <?php if(isset($_SESSION['username'])){

$TimeNow			= time();
$RandNumber   		= rand(0, 9999);
$UniqNumber			= $UserId.$RandNumber.$TimeNow;		
	
?>
        <script>
$(function() {
$('#rate-biz-2').raty({
score: 0,
click: function(score, evt) {
	$('#rate-msg').load("rate.php?id="+<?php echo $id;?>+"&score="+score+"&uniq="+<?php echo $UniqNumber;?>+"&userID="+<?php echo $UserId?>+"&bizId="+<?php echo $id?>);
	$('#rate-msg').fadeOut(10000);
		}
	});
});
</script>
        <form id="SubmitForm" class="forms" action="submit_review.php?id=<?php echo $id;?>" method="post">
          
		  <div class="form-group">
            <label for="star-rate">Note</label>
            <div id="rate-biz-2" class="star-rate"></div>
			
            <span id="rate-msg"></span>
		  </div>
         
		 
		 <div class="form-group">
            <label for="inputReview">Votre avis</label>
            <textarea class="form-control" id="inputReview" name="inputReview" placeholder="Votre avis sers à la communauté. Partagez la !"></textarea>
          </div>
		  
		  
          <input type="hidden" class="form-control" name="inputUniq" id="inputUniq" value="<?php echo $UniqNumber;?>">
		  
          <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Poster votre avis</button>
		  
        </form>
        <?php }else{?>
        <div class="col-note">Veuillez vous <a href="login">connecter</a> ou vous <a href="register">inscrire</a> pour partager votre avis</div>
        <?php }?>
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow--> 
    <?php }?>
    <script>
$(function()
{
$('.more').on("click",function()
{
var ID = $(this).attr("id");
if(ID)
{
$("#more"+ID).html('<img src="templates/<?php echo $Settings['template'];?>/images/loader.gif"/>');

$.ajax({
type: "POST",
url: "data_reviews.php",
data: "lastmsg="+ ID +"&id="+<?php echo $id;?>,
cache: false,
success: function(html){
$("div#display-reviews").append(html);
$("#more"+ID).remove(); // removing old more button
}
});
}
return false;
});
});

$(document).ready(function()
{
$('.star-rates').raty({
	readOnly: true,
    score: function() {
    return $(this).attr('data-score');

  }
});
});
</script>
    <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Avis sur <?php echo $BizRow['business_name'];?></h1>
      </div>
      <div class="col-desc" id="display-reviews">
        <?php

if($Reviews = $mysqli->query("SELECT * FROM reviews LEFT JOIN users ON users.user_id=reviews.u_id WHERE reviews.u_id=users.user_id AND reviews.rev_active=1 AND reviews.b_id='$id' ORDER BY reviews.rev_id DESC LIMIT 10")){
	
	

    while($ReviewsRow = mysqli_fetch_array($Reviews)){
		
		$UserName = $ReviewsRow['username'];
		$UserLink = preg_replace("![^a-z0-9]+!i", "-", $UserName);
		$UserLink = urlencode(strtolower($UserLink));
		$UserAvatar = $ReviewsRow['avatar'];
		
		if (empty($UserAvatar)){ 
		$AvatarImg =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
		}elseif (!empty($UserAvatar)){
		$AvatarImg =  'http://'.$SiteLink.'/avatars/'.$UserAvatar;
 		}	
		
		$RewId = $ReviewsRow['rev_id'];
	
?>
        <div class="review-box"> <a href="profile-<?php echo $ReviewsRow['user_id'];?>-<?php echo $UserLink." ". $ReviewsRow['avg'];?>">
          <?php
	echo '<img class="img-avatar" src="thumbs.php?src='.$AvatarImg.'&amp;h=60&amp;w=60&amp;q=100" alt="'.ucfirst($UserName).'" />';
 ?>
          </a>
          <div class="review-heading"> <a href="profile-<?php echo $ReviewsRow['user_id'];?>-<?php echo $UserLink;?>"><?php echo ucfirst($UserName);?></a> <span><?php echo $ReviewsRow['rew_date'];?></span>
            <div class="col-rate"> <span class="star-rates"  data-score="<?php echo $ReviewsRow['avg'];?>"></span> </div>
          </div>
          <div class="review-body">
            <p><?php echo nl2br($ReviewsRow['review']);?></p>
          </div>
          <!--review-body--> 
          
        </div>
        <!--review-box--> 
        

        <?php
}

	$Reviews->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

if($NumReviews==0)
{
	if($BizUid!=$UserId)
	{
?>
        <div class="col-note">Soyez le premier à écrire un avis</div>
        
<?php 
}else
{
	?>
	<div class="col-note">Personne n'a encore ecrit d'avis sur votre service</div>
	<?php
}	
} if($NumReviews>10){?>
        <div id="more<?php echo $RewId ;?>" class="morebox"> <a href="#" class="more btn btn-lg btn-danger" id="<?php echo $RewId ;?>"><span class="fa fa-chevron-down"></span> See More</a> </div>
        <?php }?>
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow--> 
    
  </div>
  <!--col-md-8-->
  
  <div class="col-md-4">
    <div class="col-shadow">
      <div class="rate-title"> <span class="font-big"><?php echo $BizRow['avg'];?></span>/5
        <div id="rate-biz" class="pull-biz-info"></div>
        <div class="pull-biz-info"><?php echo $NumReviews;?> Avis</div>
      </div>
      <!--rate-title-->
      <div class="col-right">
        <?php
//Calculate

$star1 = $BizRow['star1'];
$star2 = $BizRow['star2'];
$star3 = $BizRow['star3'];
$star4 = $BizRow['star4'];
$star5 = $BizRow['star5'];
$Tot = $BizRow['tot'];

if($Tot==0){
	echo "Pas encore noté";
}else{
for ($i=1;$i<=5;++$i) {
  $var = "star$i";
  $count = $$var;
  $percent = $count * 100 / $Tot;

?>
        <div class="progress-left"><?php echo $i;?> Etoiles</div>
        <div class="progress" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $count;?>">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $percent;?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percent;?>%"> </div>
        </div>
        <!--progress-->
        <?php	
}

}
?>
      </div>
      <!--col-right--> 
    </div>
    <!--col-shadow-->
    
    <div class="col-shadow">
      <div class="right-title">
        <h1 class="pull-left">Plan </h1>
        <?php if($BizUid==$UserId){?>
        <a class="pull-right edit-link-2" href="edit_map-<?php echo $id;?>"><span class="fa fa-edit"></span>Modifier</a>
        <?php }?>
      </div>
      <div class="col-right">
        <div id="map"></div>
        <div class="col-note"><a href="http://maps.google.com/maps?z=12&t=m&q=loc:<?php echo $lat;?>,<?php echo $long;?>" target="_blank"><span class="fa fa-car"></span> Directions</a></div>
      </div>
      <!--col-right--> 
    </div>
    <!--col-shadow-->
    
    <div class="col-shadow">
      <div class="right-title">
        <h1 class="pull-left">Horaires d'ouverture</h1>
        <?php if($BizUid==$UserId){?>
        <a class="pull-right edit-link-2" href="edit_hours-<?php echo $UniqId;?>"><span class="fa fa-edit"></span> Modifier</a>
        <?php }?>
      </div>
      <div class="col-right">
        <?php
if($OpenHours = $mysqli->query("SELECT * FROM hours WHERE unique_hours='$UniqId' ORDER BY FIELD(day, 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');")){
	
	$NumHours = $OpenHours->num_rows;
				
    while($DisplyHours = mysqli_fetch_array($OpenHours)){

?>
        <div class="info-biz-row">
          <div class="col-day"><?php echo $DisplyHours['day'];?></div>
          <?php echo $DisplyHours['open_from']." - ".$DisplyHours['open_till'];?></div>
        <?php
}
	$OpenHours->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

if($NumHours==0){
?>
        <div class="info-biz-row">N/A</div>
        <?php }?>
      </div>
      <!--col-right--> 
    </div>
    <!--col-shadow-->
    
    <?php if(!empty($Ad1)){?>
    <div class="col-shadow col-ads"> <?php echo $Ad1;?> </div>
    <!--col-shadow-->
    <?php } ?>
    <div class="col-shadow">
      <div class="right-title">
        <h1 class="pull-left">En Lien avec ce que vous avez regardé</h1>
      </div>
      <script>
$(document).ready(function()
{
$('.sidebar-rate').raty({
	readOnly: true,
    score: function() {
    return $(this).attr('data-score');

  }
});
});
</script>
      <?php

if($RelatedSql = $mysqli->query("SELECT * FROM business WHERE active=1 AND cid='$Category' AND biz_id >= Round(  Rand() * ( SELECT Max( biz_id ) FROM business))LIMIT 6")){ 

while ($RelatedRow = mysqli_fetch_array($RelatedSql)){ 

	$longRelated = stripslashes($RelatedRow['business_name']);
	$strRelated = strlen ($longRelated);
	if ($strRelated > 20) {
	$RelatedTitle = substr($longRelated,0,17).'...';
	}else{
	$RelatedTitle = $longRelated;}
	
	$RelatedLink = preg_replace("![^a-z0-9]+!i", "-", $longRelated);
	$RelatedLink = urlencode(strtolower($RelatedLink));

?>
      <div class="img-thumbs">
        <div class="right-caption span4"> <img class="img-remove" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $RelatedRow['featured_image'];?>&amp;h=90&amp;w=120&amp;q=100" alt="<?php echo $longFeat;?>">
          <div class="col-caption"> <a href="business-<?php echo $RelatedRow['biz_id'];?>-<?php echo $RelatedLink;?>">
            <h4><?php echo $RelatedTitle;?></h4>
            </a>
            <p><span class="sidebar-rate" data-score="<?php echo stripslashes($RelatedRow['avg']);?>"></span></p>
            <p><?php echo stripslashes($RelatedRow['reviews']);?> Avis</p>
          </div>
        </div>
      </div>
      <?php     
	
	}
$RelatedSql->close();

}else{
     printf("There Seems to be an issue");
}


?>
      <a class="pull-link" href="all"><span class="fa fa-arrow-right"></span> Tout voir</a> </div>
    <!--col-shadow--> 
    
  </div>
  <!--col-md-4--> 
  
</div>
<!--container-biz--> 
<script src="js/ekko-lightbox.min.js"></script> 
<script src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript">
$(document).ready(function () {
// Define the latitude and longitude positions
var latitude = parseFloat("<?php echo $lat; ?>"); // Latitude get from above variable
var longitude = parseFloat("<?php echo $long; ?>"); // Longitude from same
var latlngPos = new google.maps.LatLng(latitude, longitude);
// Set up options for the Google map
var myOptions = {
zoom: 15,
center: latlngPos,
mapTypeId: google.maps.MapTypeId.ROADMAP,
zoomControlOptions: true,
zoomControlOptions: {
style: google.maps.ZoomControlStyle.LARGE
}
};
// Define the map
map = new google.maps.Map(document.getElementById("map"), myOptions);
// Add the marker
var marker = new google.maps.Marker({
position: latlngPos,
map: map,
title: "<?php echo $BizName;?>"
});
});
$('[data-toggle="tooltip"]').tooltip({
    'placement': 'top'
});
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>
<?php include("footer.php");?>