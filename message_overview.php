<?php


						$topmessage = -1;
						$displayed_users = array();
						
						if($getLastMessage = $mysqli->query("SELECT  sender.username AS sender_user_name
       ,recipient.username AS recipient_user_name
       ,sender.user_id AS senderID
       ,recipient.user_id AS receiverID
       ,dateMessage
	   ,idMessage
       ,message
FROM    messages
        INNER JOIN users AS sender ON sender.user_id = messages.user_id
        INNER JOIN users AS recipient ON recipient.user_id = messages.receiverID
        INNER JOIN ( SELECT MAX(idMessage) AS most_recent_message_id
                     FROM   messages
                     GROUP BY CASE WHEN user_id > receiverID
                                   THEN receiverID
                                   ELSE user_id
                              END -- low_id
                           ,CASE WHEN user_id < receiverID
                                 THEN receiverID
                                 ELSE user_id
                            END -- high_id
                   ) T ON T.most_recent_message_id = messages.idMessage
WHERE   messages.user_id = '$id'
        OR messages.receiverID = '$id'
ORDER BY dateMessage DESC"))
						{
							
							$rowcount=mysqli_num_rows($getLastMessage);
							
							if($rowcount == 0)
							{
								echo '<div class="no-display">';
									echo "Pas de messages pour le moment.";
								echo '</div>';
								$topMessageId = -1;
							}
							else
							{
								while($row = $getLastMessage->fetch_assoc())
								{
									$topmessage ++;
									
									if($row["receiverID"] == $id)
									{
										$idUser = $row["senderID"];
									}
									else
									{
										$idUser = $row["receiverID"];
									}
									
									
									if($topmessage == 0)
									{
										$topMessageId = $row["idMessage"];
									}
									
									$idMessage = $row["idMessage"];
									$message = $row["message"];
									$dateMessage = $row["dateMessage"];
									
									if($ProfileSql = $mysqli->query("SELECT user_id, username, avatar FROM users WHERE user_id='$idUser'"))
									{
										$ProfileInfo = mysqli_fetch_array($ProfileSql);
										
										$idUse = $ProfileInfo['user_id'];
										$name = $ProfileInfo['username'];
										$ProfileAvatar = $ProfileInfo['avatar'];
										
										
										
										if (empty($ProfileAvatar))
										{ 
											$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
										}
										elseif (!empty($ProfileAvatar))
										{
											$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
										}
										$ProfileSql->close();
										
										if (!in_array($idUse, $displayed_users))
										{
													echo '<div class="conversation">';
													echo '<input type=text class="user-iden" value='.$idUse.' name="user_identifiant">';
													echo '<div class="conv">';
														echo '<div class="pPic">'; ?>
															<img src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=60&amp;w=60&amp;q=100" class="img-circle">
														<?php
														echo '</div>';
														
														echo '<div class="recipent-message">';
															echo '<div class="recipient-name">';
																echo $name;
															echo '</div>';
															
															echo '<div class="message-overview">';
																echo $message;
															echo '</div>';
														echo '</div>';
														
														echo '<div class="date-message">';
															
															getFinalDate($dateMessage);
															
														echo '</div>';
														
													echo '</div>';
													
												echo '</div>';
										}
										
										array_push($displayed_users,$idUse);
										
									}
									else
									{
										printf("Impossible d'afficher cette coversation."); 
										return;
									}
								}
							}
						}
						else
						{
							echo '<div class="no-display-error">';
									echo "Les messages n'ont pas pu être afficher.";
							echo '</div>';
							return;
						}
						
						echo '<input type="text" name="top-message" class="top-message-class" id="top-message-id" value="'.$topMessageId.'">';
						
						
						
						

					?>