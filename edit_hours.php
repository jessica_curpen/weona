<?php include("header.php");
if(!isset($_SESSION['email'])){?>
<script type="text/javascript">
function leave() {
window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php }else{?>
  <div class="container container-main">
    <div class="col-md-8"> 
      <script type="text/javascript" src="js/jquery.form.js"></script> 
      <script>
$(document).ready(function()
{
    $('#SubmitHours').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-1").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output-1',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

$(document).ready(function(){
//Delete	
$('a.btnDelete').on('click', function (e) {
    e.preventDefault();
    var id = $(this).closest('div').data('id');
    $('#myModal').data('id', id).modal('show');
});

$('#btnDelteYes').click(function () {
    var id = $('#myModal').data('id');
	var dataString = 'id='+ id ;
    $('[data-id=' + id + ']').parent().remove();
    $('#myModal').modal('hide');
	//ajax
	$.ajax({
type: "POST",
url: "delete_hours.php",
data: dataString,
cache: false,
success: function(html)
{
//$(".fav-count").html(html);
$("#output").html(html);
}
});
//ajax ends
});
});
</script>

<?php 

$id = $mysqli->escape_string($_GET['id']);

?>	
	<div class="col-shadow">
      <div class="biz-title-2">
        <h1>Gestion des horaires d'ouverture</h1>
      </div>
      <div class="col-desc">
      
      <div id="output"></div>
      
      <div class="row">
        

        <?php
if($OpenHours = $mysqli->query("SELECT * FROM hours WHERE unique_hours='$id' ORDER BY FIELD(day, 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');")){
	
	$NumHours = $OpenHours->num_rows;
				
    while($DisplyHours = mysqli_fetch_array($OpenHours)){

?>		
		<div class="input-group-2">
        	
        <div class="col-xs-4">
      	<?php echo $DisplyHours['day'];?>
        </div><!--col-xs-4-->
        
        <div class="col-xs-4">
      	
        <?php echo $DisplyHours['open_from']." - ".$DisplyHours['open_till'];?>
        
        </div><!--col-xs-4-->
        
        <div class="col-xs-4" data-id="<?php echo $DisplyHours['hour_id'];?>">
      	
        <a class="btnDelete" href="#"><span class="fa fa-remove"></span> Supprimer </a>
        
        </div><!--col-xs-4-->
        
         </div><!--input-group-2-->
        
        <?php
}
	$OpenHours->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}
?>        
    
     
      </div><!--row-->
      </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow--> 	

     <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Ajouter Horaire d'ouverture</h1>
      </div>
      <div class="col-desc">
                                 
              <div id="output-1"></div>
              <form id="SubmitHours" class="forms" action="submit_hours.php?id=<?php echo $id;?>" method="post">
              <div class="row">
               <div class="input-group-2">
                <div class="col-xs-4">
                  <label for="inputDay">Jour</label>
                  <select class="form-control" id="inputDay" name="inputDay">
                    <option>Lun</option>
                    <option>Mar</option>
                    <option>Mer</option>
                    <option>Jeu</option>
                    <option>Ven</option>
                    <option>Sam</option>
                    <option>Dim</option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <label for="inputFrom">De</label>
                  <select class="form-control" id="inputFrom" name="inputFrom">
                    <option>00.00  </option>
                    <option>00.30  </option>
                    <option>01.00  </option>
                    <option>01.30  </option>
                    <option>02.00  </option>
                    <option>02.30  </option>
                    <option>03.00  </option>
                    <option>03.30  </option>
                    <option>04.00  </option>
                    <option>04.30  </option>
                    <option>05.00  </option>
                    <option>05.30  </option>
                    <option>06.00  </option>
                    <option>06.30  </option>
                    <option>07.00  </option>
                    <option>07.30  </option>
                    <option>08.00  </option>
                    <option>08.30  </option>
                    <option selected="selected">09.00  </option>
                    <option>09.30  </option>
                    <option>10.00  </option>
                    <option>10.30  </option>
                    <option>11.00  </option>
                    <option>11.30  </option>
                    <option>12.00   </option>
                    <option>12.30  </option>
                    <option>13.00  </option>
                    <option>13.30  </option>
                    <option>14.00  </option>
                    <option>14.30  </option>
                    <option>15.00  </option>
                    <option>15.30  </option>
                    <option>16.00  </option>
                    <option>16.30  </option>
                    <option>17.00  </option>
                    <option>017.30  </option>
                    <option>18.00  </option>
                    <option>18.30  </option>
                    <option>19.00  </option>
                    <option>19.30  </option>
                    <option>20.00  </option>
                    <option>20.30  </option>
                    <option>21.00  </option>
                    <option>21.30  </option>
                    <option>22.00  </option>
                    <option>22.30  </option>
                    <option>23.00  </option>
                    <option>23.30  </option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <label for="inputTo">A</label>
                  <select class="form-control" id="inputTo" name="inputTo">
                    <option>00.00  </option>
                    <option>00.30  </option>
                    <option>01.00  </option>
                    <option>01.30  </option>
                    <option>02.00  </option>
                    <option>02.30  </option>
                    <option>03.00  </option>
                    <option>03.30  </option>
                    <option>04.00  </option>
                    <option>04.30  </option>
                    <option>05.00  </option>
                    <option>05.30  </option>
                    <option>06.00  </option>
                    <option>06.30  </option>
                    <option>07.00  </option>
                    <option>07.30  </option>
                    <option>08.00  </option>
                    <option>08.30  </option>
                    <option>09.00  </option>
                    <option>09.30  </option>
                    <option>10.00  </option>
                    <option>10.30  </option>
                    <option>11.00  </option>
                    <option>11.30  </option>
                    <option>12.00  </option>
                    <option>12.30  </option>
                    <option>13.00  </option>
                    <option>13.30  </option>
                    <option>14.00  </option>
                    <option>14.30  </option>
                    <option>15.00  </option>
                    <option>15.30  </option>
                    <option>16.00  </option>
                    <option>16.30  </option>
                    <option selected="selected">17.00  </option>
                    <option>17.30  </option>
                    <option>18.00  </option>
                    <option>18.30  </option>
                    <option>19.00  </option>
                    <option>19.30  </option>
                    <option>20.00  </option>
                    <option>20.30  </option>
                    <option>21.00  </option>
                    <option>21.30  </option>
                    <option>22.00  </option>
                    <option>22.30  </option>
                    <option>23.00  </option>
                    <option>23.30  </option>
                  </select>
                </div>
                </div>
                </div><!--row-->
                 <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Ajouter Horaire</button>
              </form>
               
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
</div><!--col-md-8-->

  
    
    <div class="col-md-4">
      <?php include("side_bar.php");?>
    </div>
    <!--col-md-4--> 
    
  </div>
 <!--container-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Confirmation</h4>

            </div>
            <div class="modal-body">
				<p>Etes-vous sûr de vouloir supprimer cette horaire d'ouverture</p>
                <p class="text-warning"><small>Vous perdrez toutes les informations liées à cette horaire</small></p>		
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDelteYes">Oui</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
  
<?php } include("footer.php");?>