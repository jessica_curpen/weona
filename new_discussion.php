<?php 
include("header.php");

$Uname = $_SESSION['username'];
$Uemail = $_SESSION['email'];

//if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE username='$Uname'"))
if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE username='$Uemail'"))
{
    $ProfileInfo = mysqli_fetch_array($ProfileSql);
	$name = $ProfileInfo['username'];	
	$id = $ProfileInfo['user_id'];	
	$ProfileSql->close();
}
else
{
     printf("Un problème est survenu."); 
}

?>


<div class="container container-main2">
<script type="text/javascript" src="js/jquery.form.js"></script>
<script>
$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        });
    });
});
</script>
	<div class="col-md-3">
		
			<div class="new-disc">
				<div class="button-style raised iris" id="start-discussion">
				  <div class="center" fit>Commencer une discussion</div>
				  <paper-ripple fit></paper-ripple>
				</div>
			</div>
			
			<!--Tableau de bord Forum-->
			<div class="categories-disc" style="margin-top: 10px">
			<div class="col-shadow">
				<div class="row">
				
						<div class="col-md-12"> <div class="tab-forum"> <a href="forum.php"> Discussions récentes </a> </div></div>
						<div class="col-md-12"> <div class="tab-forum"> <a href="forum.php"> Mes discussions </a></div></div>
						<div class="col-md-12">
							<div class="tab-forum-notif">
								
							</div>
							<div class="tab-forum-notif2">
								
							</div>
						</div>	
				</div>
			</div>	
			</div>
		
		</div>
	<div class="col-md-7">
		<div class="col-shadow">
				<div class="biz-title-2">
					<h1>Discussion</h1>
				</div>
				
				<div class="col-desc">
					<div id="output"></div>
					
					<form id="SubmitForm" class="forms" action="submit_discussion.php" method="post">
					
					<div class="form-group">
						<label for="sujet-disc">Titre</label>
						<div class="input-group">
						   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input type="text" class="form-control" name="sujet-disc" id="sujet-disc" placeholder="Quel est le sujet de la discussion ?">
						</div>
					</div>
					
					
					<div class="form-group">
						<label for="description-disc">Description</label>
						<textarea class="form-control" id="description-disc" name="description-disc" placeholder="Donnez une description du sujet"></textarea>
					</div>
					
					<div class="form-group">
                  <label for="inputCategory">Categorie</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCategory" name="inputCategory">
                      <option value="">Choisir une catégorie</option>
                      <?php
								if($SelectCategories = $mysqli->query("SELECT cat_id, category FROM categories WHERE parent_id=0"))
								{

											while($categoryRow = mysqli_fetch_array($SelectCategories))
											{
												
											?>
													  <option value="<?php echo $categoryRow['cat_id'];?>"><?php echo utf8_encode($categoryRow['category']);?></option>
													  <?php

											}

									$SelectCategories->close();
									
								}
								else
								{
									
									 printf("There Seems to be an issue");
								}

					?>
                    </select>
                  </div>
                </div>
				
                <div class="form-group">
                  <label for="inputSubcategory">Sous-categorie</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputSubcategory" name="inputSubcategory">
                      <option value="">Choisir une sous-catégorie (Optionelle)</option>
                    </select>
                  </div>
                </div>
				
				<div style="margin-top: 20px;">
									<div class="button-style raised iris" id="submit-disc">
										<div class="center" fit>Poster</div>
										<paper-ripple fit></paper-ripple>
									</div> 
				</div> 
					</form>
				</div>
			</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script>
$(document).ready(function(){

    $('#inputCategory').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_subcategory.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) {
                console.log(response);
                $("#inputSubcategory").html(response);
            },
        });
    }); 

});</script>
<?php include("footer.php");?>