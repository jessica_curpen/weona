<?php include("header2.php");

$term = $mysqli->escape_string($_GET['name']);

?>

<div class="container container-main" id="display-posts">

<div class="page-title"><h1>Votre recherche pour "<?php echo $term;?>"</h1></div>

<script>
$(document).ready(function()
{
$('.star-rates').raty({
	readOnly: true,
  score: function() {
    return $(this).attr('data-score');

  }
});
});
</script>


<div class="container">
	<div class="col-md-2">
	</div>
	
	<div class="col-md-6">
	
		<ul class="list-group">

<?php

	
			$PostSql = $mysqli->query("SELECT * FROM users WHERE user_id <> '$UserId' AND (username LIKE '%$term%' OR firstname LIKE '%$term%' OR lastname LIKE '%$term%') And active = '1' ORDER BY user_id DESC LIMIT 0, 12");

			$CountRows = mysqli_num_rows($PostSql);	

			while ($PostRow = mysqli_fetch_array($PostSql))
			{
				$username 			= $PostRow['username'];
				$firstname 			= $PostRow['firstname'];
				$lastname 			= $PostRow['lastname'];
				$ProfileAvatar 		= $PostRow['avatar'];
				$idRanking 			= $PostRow['idRanking'];
				$id 				= $PostRow['user_id'];
				$workplace 			= $PostRow['workplace'];
				
				if (empty($ProfileAvatar))
				{ 
					$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
				}
				elseif (!empty($ProfileAvatar))
				{
					$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
				}
				
				
				//Get Number of Followers
				$follower = $mysqli->query("SELECT * FROM followers WHERE idFollowed='$id'");
				$NumFollowers = $follower->num_rows;

				//Get Ranking
				$userRank = $mysqli->query("SELECT rank, image FROM ranking WHERE idRanking='$idRanking'");
				$rankInfo = mysqli_fetch_array($userRank);

				$rankname = $rankInfo["rank"];
				$rankimage = $rankInfo["image"];

				$userRank->close();

?>


			<li class="list-group-item">
			
					  <div class="row no-gutter " >
						<div class="col-sm-2">
							<img style="margin-left: 10px ; margin-right: 10px" src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=85&amp;w=85&amp;q=80" class="img-rounded">
						</div>
						<div class="col-sm-6">
							<a href="user_profile-<?php echo $id;?>-<?php echo $username;?>"><h2><?php echo $username?>	</h2></a>
							<?php 
							if($workplace != null)
							{
								echo '<p class="text-muted">Travaille chez '.$workplace.'</p>' ;
							}
							?>
							<table style="margin-top : 10px" class="pull-bottom">
									<tr>
									
											<td class="center"> <img src="images/badges/group.png" style="width: 20px; height: 20px "></td>
											
											<td class="center" style="margin-left: 4px"><div class="infostyle"><?php echo $NumFollowers ?> Abonnés</div></td>
											 
											<td style="width : 10px"> </td>
											
											<td class="center"> <img src="<?php echo $rankimage ?>" style="width: 30px; height: 30px "></td>
											
											<td class="center" style="margin-left: 4px"><div class="infostyle"><?php echo $rankname ?></div></td>
											
											<td style="width : 10px"> </td>
									
									</tr>
							</table>
						</div>
						<div class="col-sm-4">
							<table class="pull-right">
								
										<tr>
											<td> <div class="side-button-style">Suivre</div></td>
										</tr>
										
										<tr>
											<td> <div style="height: 5px"></div></td>
										</tr>
										
										<tr>
											<td><div class="side-button-style">Contacter</div></td>
										</tr>
							
							</table>
						</div>
					  </div>
					
			</li>
		
		

<?php     
	}
	?>
	
	</ul>
    
	</div>
	
</div><!-- container -->
<?php

if($CountRows==0){
?>
<div class="col-box-search">
<div class="search-title"><h3>Votre recherche pour <span class="tt-text">"<?php echo $term;?>"</span> n'a donné aucun résultats.</h3></div>
<ul class="search-again">
<li>Assurez vous d'avoir bien écrit le mot</li>
<li>Essayer avec d'autres mots clés</li>
<li>Essayer avec des mots clés plus général</li>
</ul>
</div>
<?php }?>




</div><!--container-->

<nav id="page-nav"><a href="data_search.php?page=2&amp;term=<?php echo $term;?>"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#display-posts').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.col-box',     //
		loading: {
          				finishedMsg: 'Fin des recherches',
          				img: 'templates/<?php echo $Settings['template'];?>/images/loader.gif'
	}
	}, function(newElements, data, url){
		
		$('.star-rates').raty({
		readOnly: true,
  		score: function() {
   		 return $(this).attr('data-score');

  		}
		});
		$('.star-rates').raty('reload');	
	});	

</script>


<?php include("footer.php");?>