-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 13 Juillet 2016 à 13:07
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `weona`
--
CREATE DATABASE IF NOT EXISTS `weona` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `weona`;

-- --------------------------------------------------------

--
-- Structure de la table `actions`
--

CREATE TABLE IF NOT EXISTS `actions` (
  `idAction` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) NOT NULL,
  `description` varchar(999) NOT NULL,
  `points` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  PRIMARY KEY (`idAction`),
  UNIQUE KEY `action` (`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `actions`
--

INSERT INTO `actions` (`idAction`, `action`, `description`, `points`, `coins`) VALUES
(1, 'add_prod_review', 'Golden coins gained when user adds a review on a product', 50, 40),
(2, 'add_biz', 'Golden coins gained when user adds a business to his account', 200, 100),
(3, 'create_discussion', 'Golden coins gained when users starts a discussion in the forum', 100, 50),
(4, 'reply_discussion', 'Golden coins gained when users replies in a discussion', 50, 30),
(5, 'follow_user', 'Golden coins gained when users starts to follow a user', 100, 20),
(6, 'user_is_followed', 'Golden coins gained when users is followed', 100, 20),
(7, 'like_review', 'Golden coins gained when user likes a review', 50, 10),
(8, 'review_liked', 'Golden coins gained when the user''s review is liked by another user', 60, 10);

-- --------------------------------------------------------

--
-- Structure de la table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `id` int(11) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(999) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `administrator`
--

INSERT INTO `administrator` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Structure de la table `advertisements`
--

CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` int(11) NOT NULL,
  `ad1` varchar(999) NOT NULL,
  `ad2` varchar(999) NOT NULL,
  `ad3` varchar(999) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `advertisements`
--

INSERT INTO `advertisements` (`id`, `ad1`, `ad2`, `ad3`) VALUES
(1, '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks`
--

CREATE TABLE IF NOT EXISTS `bookmarks` (
  `bm_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bizid` int(11) NOT NULL,
  PRIMARY KEY (`bm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `business`
--

CREATE TABLE IF NOT EXISTS `business` (
  `biz_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(999) NOT NULL,
  `description` varchar(999) NOT NULL,
  `address_1` varchar(999) NOT NULL,
  `address_2` varchar(999) NOT NULL,
  `city` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `website` varchar(999) NOT NULL,
  `menu` varchar(999) NOT NULL,
  `email` varchar(500) NOT NULL,
  `featured_image` varchar(999) NOT NULL,
  `facebook` varchar(999) NOT NULL,
  `twitter` varchar(999) NOT NULL,
  `pinterest` varchar(999) NOT NULL,
  `tags` varchar(999) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `active` int(11) NOT NULL,
  `star1` int(11) NOT NULL,
  `star2` int(11) NOT NULL,
  `star3` int(11) NOT NULL,
  `star4` int(11) NOT NULL,
  `star5` int(11) NOT NULL,
  `tot` int(11) NOT NULL,
  `avg` varchar(255) NOT NULL,
  `reviews` int(11) NOT NULL,
  `feat` int(11) NOT NULL,
  `biz_user` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `unique_biz` varchar(999) NOT NULL,
  `hits` int(11) NOT NULL,
  `bookmarks` int(11) NOT NULL,
  PRIMARY KEY (`biz_id`),
  KEY `biz_user` (`biz_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `business`
--

INSERT INTO `business` (`biz_id`, `business_name`, `description`, `address_1`, `address_2`, `city`, `phone`, `website`, `menu`, `email`, `featured_image`, `facebook`, `twitter`, `pinterest`, `tags`, `cid`, `sid`, `date`, `active`, `star1`, `star2`, `star3`, `star4`, `star5`, `tot`, `avg`, `reviews`, `feat`, `biz_user`, `latitude`, `longitude`, `unique_biz`, `hits`, `bookmarks`) VALUES
(9, 'Hat Store', 'Choose among a hundred hats of different styles at small prices. You will get hats for every occasions, from a casual summer ride to a chic evening event. Re-invent yourself with hats.', '3, avenue des lilas', '', 'Paris', '0125352659', 'http://www.hatstore.com', 'www.hatstore.com', 'hatstore@weona.com', 'hatstore_1129455162.jpg', 'www.hatstore.com', 'www.hatstore.com', 'www.hatstore.com', 'hat,hat store,chapeau,casquette,femm', 47, 48, 'April 21, 2016', 1, 0, 0, 0, 0, 0, 0, '0', 0, 0, 27, '', '', '2796931461229638', 2, 0),
(10, 'Book Store', 'Has more than 1000 books at mini prices', '3, avenue des lilas', '', 'Paris', '0152535963', 'http://www.bookstore.com', 'www.bookstore.com', 'bookstore@weona.com', 'bookstore_1148819464.jpg', 'www.bookstore.com', 'www.bookstore.com', 'www.bookstore.com', 'book, book store,mini prices,paris,soldes', 41, 42, 'April 21, 2016', 1, 0, 0, 2, 1, 0, 3, '3.33', 0, 0, 27, '', '', '2761181461229990', 2, 0),
(11, 'Shoe Store', 'Shoe Store has imported shoes from varous countries such as america, china accross europe. ', '3, avenue des lilas', '', 'Paris', '0152539685', 'http://www.shoestore.com', 'www.shoestore.com', 'shoestore@weona.com', 'shoestore_775906047.jpg', 'www.shoestore.com', 'www.shoestore.com', 'www.shoestore.com', 'chaussure, shoe, samml prices,sales, soldes', 47, 50, 'April 21, 2016', 1, 0, 0, 0, 0, 0, 0, '0', 0, 0, 27, '', '', '2766641461230234', 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(500) NOT NULL,
  `cat_description` varchar(999) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`cat_id`, `category`, `cat_description`, `parent_id`) VALUES
(25, 'Technologie', 'Technologies', 0),
(26, 'Mobile', 'Mobile', 25),
(27, 'Tablette', 'Tablette', 25),
(28, 'Casque', 'Casque', 25),
(29, 'PC Portable', 'PC Portable', 25),
(30, 'Haut-Parleurs', 'Haut-Parleurs', 25),
(31, 'MP3', 'MP3', 25),
(32, 'Souris', 'Souris', 25),
(33, 'Clavier', 'Clavier', 25),
(34, 'Ecouteurs', 'Ecouteurs', 25),
(41, 'Loisirs', 'Loisirs', 0),
(42, 'Livres', 'Livres', 41),
(43, 'Musique', 'Musique', 41),
(44, 'Films', 'Films', 41),
(45, 'SÃ©ries', 'SÃ©ries', 41),
(46, 'Presse', 'Presse', 41),
(47, 'Mode', 'Mode', 0),
(48, 'Accesoires', 'Accesoires', 47),
(49, 'VÃªtements', 'VÃªtements', 47),
(50, 'Chaussures', 'Chaussures', 47),
(51, 'Sport', 'Sport', 0),
(52, 'Tenue', 'Tenue', 51),
(53, 'Accesoires', 'Accesoires', 51),
(54, 'Chaussures', 'Chaussures', 51),
(55, 'Informatique', 'Informatique', 0),
(56, 'Alimentaire', 'Alimentaire', 0),
(57, 'ThÃ©', 'ThÃ©', 56),
(58, 'CafÃ©', 'CafÃ©', 56),
(59, 'PC', 'PC', 25);

-- --------------------------------------------------------

--
-- Structure de la table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(999) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `city`
--

INSERT INTO `city` (`city_id`, `city`) VALUES
(16, 'Paris');

-- --------------------------------------------------------

--
-- Structure de la table `discussions`
--

CREATE TABLE IF NOT EXISTS `discussions` (
  `id_discussion` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sujet` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(999) CHARACTER SET utf8 NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed` tinyint(1) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tags` varchar(999) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_discussion`),
  KEY `user_id` (`user_id`),
  KEY `categorie` (`cid`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `discussions`
--

INSERT INTO `discussions` (`id_discussion`, `user_id`, `sujet`, `description`, `date_post`, `closed`, `cid`, `sid`, `tags`) VALUES
(7, 29, 'Probleme de lecture de vidÃ©o?', 'Restabat ut Caesar post haec properaret accitus et abstergendae causa suspicionis sororem suam, eius uxorem, Constantius ad se tandem desideratam venire multis fictisque blanditiis hortabatur. quae licet ambigeret metuens saepe cruentum, spe tamen quod eum lenire poterit ut germanum profecta, cum Bithyniam introisset, in statione quae Caenos Gallicanos appellatur, absumpta est vi febrium repentina. cuius post obitum maritus contemplans cecidisse fiduciam qua se fultum existimabat, anxia cogitatione, quid moliretur haerebat.', '2016-04-22 08:41:14', 0, 55, 0, ''),
(8, 29, 'LevÃ© avant le jour nous trouva perdus dans la rue se remplit de bruit, le guet a passÃ© ', 'orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. N', '2016-04-22 08:50:00', 0, 25, 30, ''),
(9, 29, 'Maboriosam, nisi ut aliquid ex ea commodi consequatur?', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores', '2016-04-22 09:39:42', 0, 41, 42, ''),
(10, 29, 'es approvisionne en rÃ¨glalades nÃ©cessaires en tout genre;', 'Loin, trÃ¨s loin, au delÃ  des monts Mots, Ã  mille lieues des pays Voyellie et Consonnia, demeurent les Bolos Bolos. Ils vivent en retrait, Ã  Bourg-en-Lettres, sur les cÃ´tes de la SÃ©mantique, un vaste ocÃ©an de langues. Un petit ruisseau, du nom de Larousse, coule en leur lieu et les approvisionne en rÃ¨glalades nÃ©cessaires en tout genre; un pays paradisiagmatique, dans lequel des pans entiers de phrases prÃ©mÃ¢chÃ©es vous volent litÃ©ralement tout cuit dans la bouche. Pas mÃªme la toute puissante Ponctuation ne rÃ©git les Bolos Bolos - une vie on ne peut moins orthodoxographique. Un jour pourtant, une petite ligne de Bolo Bolo du nom de Lorem Ipsum dÃ©cida de s''aventurer dans la vaste Grammaire. Le grand Oxymore voulut l''en dissuader, le prevenant que lÃ -bas cela fourmillait de vils Virgulos, de sauvages Pointdexclamators et de sournois Semicolons qui l''attendraient pour sÃ»r au prochain paragraphe, mais ces mots ne firent Ã©cho dans l''oreille du petit Bolo qui ne se laissa poi', '2016-04-22 09:45:43', 0, 51, 0, ''),
(11, 29, 'le haut duquel la couverture, prÃªte Ã  glisser tout Ã  fait, ne tenait plus quâ€™Ã  peine', 'En se rÃ©veillant un matin aprÃ¨s des rÃªves agitÃ©s, Gregor Samsa se retrouva, dans son lit, mÃ©tamorphosÃ© en un monstrueux insecte. Il Ã©tait sur le dos, un dos aussi dur quâ€™une carapace, et, en relevant un peu la tÃªte, il vit, bombÃ©, brun, cloisonnÃ© par des arceaux plus rigides, son abdomen sur le haut duquel la couverture, prÃªte Ã  glisser tout Ã  fait, ne tenait plus quâ€™Ã  peine. Ses nombreuses pattes, lamentablement grÃªles par comparaison avec la corpulence quâ€™il avait par ailleurs, grouillaient dÃ©sespÃ©rÃ©ment sous ses yeux.Â« Quâ€™est-ce qui mâ€™est arrivÃ© ? Â» pensa-t-il. Ce nâ€™Ã©tait pas un rÃªve. Sa chambre, une vraie chambre humaine, juste un peu trop petite, Ã©tait lÃ  tranquille entre les quatre murs quâ€™il connaissait bien. Au-dessus de la table oÃ¹ Ã©tait dÃ©ballÃ©e une collection dâ€™Ã©chantillons de tissus - Samsa Ã©tait reprÃ©sentant de commerce - on voyait accrochÃ©e lâ€™image quâ€™il avait rÃ©cemment dÃ©coupÃ©e dans un magazine et mise dans un joli', '2016-04-22 09:50:28', 0, 47, 48, ''),
(12, 27, ' wallon, de graphie en kit mais bref. Portez ce vieux whisky au juge blond qui fume sur son Ã®le intÃ©rieure, Ã  cÃ´tÃ© de l''alcÃ´ve ovoÃ¯de, oÃ¹ les bÃ»ches se consument dans l''Ã¢tre, ce qui lui p', ' wallon, de graphie en kit mais bref. Portez ce vieux whisky au juge blond qui fume sur son Ã®le intÃ©rieure, Ã  cÃ´tÃ© de l''alcÃ´ve ovoÃ¯de, oÃ¹ les bÃ»ches se consument dans l''Ã¢tre, ce qui lui p', '2016-04-22 10:09:24', 0, 51, 0, ''),
(13, 27, 'ate eleifend tellus. Aenean leo ligula, porttitor eu, cons', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. ', '2016-04-22 10:49:57', 0, 47, 50, ''),
(14, 27, ' "What''s happened to me?" he thought', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What''s happened to me?" he thought. It wasn''t a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards ', '2016-04-22 10:51:03', 0, 41, 42, '');

-- --------------------------------------------------------

--
-- Structure de la table `followers`
--

CREATE TABLE IF NOT EXISTS `followers` (
  `idFollow` int(11) NOT NULL AUTO_INCREMENT,
  `idFollowed` int(11) NOT NULL,
  `idFollower` int(11) NOT NULL,
  `dateFollowed` timestamp NOT NULL,
  PRIMARY KEY (`idFollow`),
  KEY `idFollowed` (`idFollowed`,`idFollower`),
  KEY `idFollower` (`idFollower`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(999) NOT NULL,
  `uid` int(11) NOT NULL,
  `uniq` varchar(999) NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `galleries`
--

INSERT INTO `galleries` (`img_id`, `image`, `uid`, `uniq`) VALUES
(1, '145875186812108077_10153531878232773_6134348455462740706_n.jpg', 0, '677131458751611');

-- --------------------------------------------------------

--
-- Structure de la table `hours`
--

CREATE TABLE IF NOT EXISTS `hours` (
  `hour_id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(200) CHARACTER SET utf8 NOT NULL,
  `open_from` varchar(200) CHARACTER SET utf8 NOT NULL,
  `open_till` varchar(200) CHARACTER SET utf8 NOT NULL,
  `unique_hours` varchar(999) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`hour_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `hours`
--

INSERT INTO `hours` (`hour_id`, `day`, `open_from`, `open_till`, `unique_hours`) VALUES
(1, 'Mon', '09.00 am', '05.00 pm', '188711449064704'),
(2, 'Tue', '09.00 am', '05.00 pm', '188711449064704'),
(3, 'Wed', '09.00 am', '05.00 pm', '188711449064704'),
(4, 'Fri', '09.00 am', '05.00 pm', '188711449064704'),
(5, 'Sat', '09.00 am', '05.00 pm', '188711449064704'),
(6, 'Thu', '09.00 am', '05.00 pm', '188711449064704'),
(7, 'Lun', '09.00', '17.00', '275901455283123'),
(8, 'Mar', '09.00', '17.00', '275901455283123'),
(9, 'Mon', '09.00 am', '05.00 pm', '646861458748456'),
(10, 'Tue', '08.00 am', '02.30 pm', '614761458750933'),
(11, 'Thu', '07.00 am', '10.00 am', '614761458750933'),
(12, 'Mon', '09.00 am', '05.00 pm', '677131458751611'),
(13, 'Tue', '09.00 am', '05.00 pm', '677131458751611'),
(14, 'Wed', '09.00 am', '05.00 pm', '677131458751611'),
(15, 'Thu', '09.00 am', '05.00 pm', '677131458751611'),
(16, 'Fri', '09.00 am', '05.00 pm', '677131458751611'),
(17, 'Sat', '09.00 am', '05.00 pm', '677131458751611'),
(18, 'Sun', '09.00 am', '10.00 am', '677131458751611');

-- --------------------------------------------------------

--
-- Structure de la table `likes_produits`
--

CREATE TABLE IF NOT EXISTS `likes_produits` (
  `idLike` int(11) NOT NULL AUTO_INCREMENT,
  `idProduit` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idLike`),
  KEY `idProduit` (`idProduit`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=111 ;

--
-- Contenu de la table `likes_produits`
--

INSERT INTO `likes_produits` (`idLike`, `idProduit`, `idUser`) VALUES
(109, 43, 32),
(110, 41, 27);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `idMessage` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `receiverID` int(11) NOT NULL,
  `dateMessage` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message` varchar(255) DEFAULT NULL,
  `open` tinyint(1) NOT NULL DEFAULT '0',
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `deletedBySender` tinyint(1) NOT NULL DEFAULT '0',
  `deletedByReceiver` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`idMessage`),
  KEY `user_id` (`user_id`),
  KEY `receiverID` (`receiverID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `page` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`id`, `page`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br>'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br>'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum<br>'),
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce porttitor\r\n lobortis tortor sit amet auctor. Praesent pretium, leo eget luctus \r\ntempor, lectus erat vulputate libero, in viverra dolor velit quis ante. \r\nVivamus viverra pulvinar sollicitudin. Vivamus dictum orci sed lorem \r\nvenenatis quis rutrum risus dictum. Ut non enim elit. In consectetur, \r\nnibh sed iaculis pellentesque, nisi ligula egestas enim, sagittis \r\nfermentum nulla nisl sit amet velit. Aliquam erat volutpat. Suspendisse \r\nac mi tortor, at vestibulum augue. Suspendisse ut nisl quam, euismod \r\nscelerisque urna. Donec non leo et nisi tempus fermentum. Cum sociis \r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus \r\nmus. Donec gravida sodales est vitae tincidunt.<br><br>Curabitur neque \r\ndui, adipiscing a dignissim sed, congue ut sem. Vivamus eget tellus \r\nlectus. Nullam ut tempus purus. Vestibulum pellentesque lorem nec velit \r\nhendrerit porta. Cras sit amet mauris odio. Sed sit amet libero nec \r\nipsum venenatis interdum. Class aptent taciti sociosqu ad litora \r\ntorquent per conubia nostra, per inceptos himenaeos. Sed at ligula eu \r\nenim congue molestie. Lorem ipsum dolor sit amet, consectetur adipiscing\r\n elit. Praesent tincidunt diam at metus facilisis aliquam. Vivamus a \r\norci nunc, molestie consectetur purus. Vivamus aliquam, diam eu rhoncus \r\nmollis, est lorem aliquam neque, elementum molestie sapien velit id \r\nsapien. Maecenas quis dolor nisl.<br><br>Morbi nisi quam, suscipit eu \r\naccumsan a, porttitor sed risus. Donec laoreet, dolor semper eleifend \r\nsodales, erat lacus pretium velit, vitae dignissim felis risus non \r\nmagna. Suspendisse potenti. Proin at nulla massa, et dictum magna. Lorem\r\n ipsum dolor sit amet, consectetur adipiscing elit. Integer pharetra, \r\npurus vel egestas egestas, orci lectus vehicula quam, non placerat massa\r\n lacus id justo. Integer posuere laoreet porttitor.<br><br>Nam nulla \r\nmetus, rutrum in suscipit ac, hendrerit eget nunc. In hac habitasse \r\nplatea dictumst. Mauris at justo magna. Class aptent taciti sociosqu ad \r\nlitora torquent per conubia nostra, per inceptos himenaeos. Duis \r\nmalesuada ultricies hendrerit. Vivamus ullamcorper consectetur \r\ndignissim. Praesent nunc lorem, elementum vitae scelerisque vitae, \r\npellentesque quis ipsum. Cras volutpat, erat eu mollis pulvinar, justo \r\nlibero dictum leo, ac accumsan tellus ipsum eget magna. Suspendisse \r\ntristique mauris nec odio fringilla sagittis. Donec rhoncus euismod \r\ntortor, nec commodo magna cursus at. Nunc ut euismod dui. Suspendisse \r\nsollicitudin, magna eget auctor euismod, dolor mi aliquet tellus, et \r\nsuscipit dui est nec odio. Aliquam rutrum tellus in nisi ultricies sed \r\nelementum nisi molestie. Praesent sit amet ligula id lorem ultrices \r\ntristique.<br><br>Suspendisse potenti. Sed urna est, fringilla a \r\ncondimentum eu, dapibus et erat. Cras ac aliquet erat. Integer eget \r\nrisus magna, non egestas nulla. Maecenas ut ante tortor. Pellentesque \r\nhabitant morbi tristique senectus et netus et malesuada fames ac turpis \r\negestas. Morbi vel eros nec quam cursus porttitor et nec orci. Nulla vel\r\n odio sit amet nisi feugiat dignissim. Mauris tincidunt enim quam, non \r\npharetra risus. Sed sed mi nulla, sit amet aliquam ipsum. Ut faucibus \r\nvestibulum feugiat. Nulla ut dui eget massa pellentesque scelerisque. \r\nAenean ut ipsum at orci lacinia mollis id nec urna. Proin eros dui, \r\nfeugiat vitae adipiscing ut, rutrum vitae quam. Nam et convallis augue. \r\nQuisque ut odio eu ante consequat elementum.<br>');

-- --------------------------------------------------------

--
-- Structure de la table `participation`
--

CREATE TABLE IF NOT EXISTS `participation` (
  `idparticipation` int(11) NOT NULL AUTO_INCREMENT,
  `idDiscussion` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply` varchar(999) CHARACTER SET utf8 NOT NULL,
  `date_rep` timestamp NOT NULL,
  PRIMARY KEY (`idparticipation`),
  KEY `idDiscussion` (`idDiscussion`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `produit_name` varchar(500) CHARACTER SET utf8 NOT NULL,
  `avis` varchar(999) CHARACTER SET utf8 NOT NULL,
  `featured_image` varchar(999) CHARACTER SET utf8 NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tags` varchar(999) CHARACTER SET utf8 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avg` int(1) NOT NULL,
  `unique_biz` varchar(999) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `hits` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProduit`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Contenu de la table `produits`
--

INSERT INTO `produits` (`idProduit`, `produit_name`, `avis`, `featured_image`, `cid`, `sid`, `tags`, `date`, `avg`, `unique_biz`, `user_id`, `hits`) VALUES
(10, 'Casque Marshall Major II', 'Vendu moins de 100 euros, le Major II de Marshall offre une qualitÃ© sonore acceptable mais privilÃ©gie surtout les mÃ©diums. Il bÃ©nÃ©ficie dÃ©sormais d''un cÃ¢ble amovible. CÃ©lÃ¨bre pour ses amplificateurs de guitares (utilisÃ©s entre autres par AC/DC), le constructeur Marshall propose Ã©galement des enceintes et des casques. C''est le cas du Major lancÃ© en 2010 dont voici la seconde version. ExtÃ©rieurement, les deux casques se ressemblent beaucoup et font la part belle au plastique, ce qui n''est pas un gage de robustesse.', 'casquemarshallmajorii_1061206757.jpg', 25, 28, 'casque,marshall,major,major2,fnac', '2016-04-20 15:40:29', 3, '2753801461166782', 27, NULL),
(39, 'Avengers 3', 'Critics Consensus: Exuberant and eye-popping, Avengers: Age of Ultron serves as an overstuffed but mostly satisfying sequel, reuniting its predecessor''s unwieldy cast with a few new additions and a worthy foe.', 'avengers3_1021015073.jpg', 41, 44, 'a,v,g,ers,t', '2016-04-25 08:39:36', 3, '2792601461573408', 27, NULL),
(40, 'ThÃ© Lipton', 'Du coup, quand les salariÃ©s de lâ€™usine Fralib ont lancÃ© un appel Ã  boycotter Lipton, Ã§a mâ€™a tentÃ©e. Pour eux, câ€™est une maniÃ¨re de faire parler de leur lutte. Depuis trois ans, ils marinent. Unilever a fermÃ© leur usine de GÃ©menos, prÃ¨s de Marseille, qui produisait les tisanes ElÃ©phant et des thÃ©s Lipton.\r\n\r\nSeul ElÃ©phant intÃ©resse les salariÃ©s aujourdâ€™hui, qui voudraient reprendre la production en Scop (SociÃ©tÃ© coopÃ©rative ouvriÃ¨re de production). Mais pour ce faire, il faut que la multinationale leur cÃ¨de la marque Ã  la trompe, ce quâ€™elle refuse. Dâ€™oÃ¹ lâ€™idÃ©e du boycott, pour faire pression.', 'thlipton_359702659.jpg', 56, 57, 'lipton, thÃ©, lipton thÃ©,tea, lipton tes', '2016-04-28 09:16:24', 4, '2915131461834805', 29, NULL),
(41, 'L G4', 'dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nun', 'lg4_122339354.jpg', 25, 26, 'lg,g4,lg g4,portable,hui', '2016-04-28 12:47:42', 3, '3145371461847501', 31, NULL),
(42, 'HP Sprout', 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel ', 'hpsprout_404498743.jpg', 25, 59, 'pc,hp,sprout,pc sprout hp,srt', '2016-04-28 13:53:13', 3, '3188271461851511', 31, NULL),
(43, 'Vogue Article page 5', 'One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What''s happened to me?" he thought. It wasn''t a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards ', 'voguearticlepage5_816054699.jpg', 41, 46, 'ert,tyu,uio,kol,io', '2016-04-28 14:00:24', 2, '3099161461851847', 30, NULL),
(44, 'HHM Law', 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didnâ€™t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmar', 'hhmlaw_1404901594.png', 60, 62, 'hhm,m,mcgill,hamlin,er', '2016-04-28 14:34:36', 3, '3271461461853986', 32, NULL),
(45, 'Sony Xperia X', ' Si Sony a dÃ©cidÃ© de faire du Xperia X le porte-Ã©tendard de son offre mobile pour cette premiÃ¨re partie de l''annÃ©e, ne vous y trompez pas, le vÃ©ritable morceau de choix, c''est lui, le Xperia X Performance. Plus complet que le X sur l''Ã©quipement haut de gamme, il compile assez de qualitÃ©s, sur le papier, pour aller taper le bras de fer avec les actuels champions de la mobilitÃ©. Certaines mauvaises langues pourraient aussi ne le voir que comme un simple Xperia X aux hormones... et ils n''ont peut-Ãªtre pas tort.', 'sonyxperiax_1250976912.jpg', 25, 26, 'portable,sony, xperia,phone,performance', '2016-07-11 14:45:18', 3, '2717501468248055', 27, NULL),
(46, 'Lenovo K5', 'Le K5 est le premier smartphone Ã  sortir officiellement en France sous pavillon Lenovo. Une entrÃ©e en matiÃ¨re par la petite porte, avec un terminal d''entrÃ©e de gamme relativement modeste, mais pas dÃ©nuÃ© d''intÃ©rÃªt. Pour un marchÃ© sur lequel Lenovo est loin d''Ãªtre la marque la plus en vue, et encore moins sur le segment ultra-concurrentiel des smartphones Ã  moins de 200 â‚¬, le K5 semble manquer d''arguments sÃ©rieux pour se faire une vÃ©ritable place.', 'lenovok5_1299645857.jpg', 25, 26, 'portable,lenovo,k5lenovo,mobile,smartphone', '2016-07-11 14:58:16', 3, '2721961468248327', 27, NULL),
(47, 'Honor 5C', 'Honor s''est bÃ¢ti une notoriÃ©tÃ© sur le marchÃ© du smartphone en proposant des terminaux Ã  des rapports performances/prix intÃ©ressants. La sÃ©rie C est synonyme d''entrÃ©e de gamme pour la filiale de Huawei. Le Honor 3C est le dernier de la famille Ã  Ãªtre arrivÃ© jusqu''Ã  nos contrÃ©es, l''impasse ayant Ã©tÃ© faite chez nous pour le 4C. Voici maintenant le 5C, qui constitue une franche rÃ©ussite pour un smartphone Ã  moins de 200', 'honor5c_458589692.jpg', 25, 26, 'honor,huawei,honor5c,portable,smartphone', '2016-07-11 15:01:25', 3, '2718491468249115', 27, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `ranking`
--

CREATE TABLE IF NOT EXISTS `ranking` (
  `idRanking` int(11) NOT NULL AUTO_INCREMENT,
  `rank` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` varchar(999) CHARACTER SET utf8 NOT NULL,
  `min_points` int(11) NOT NULL,
  `max_points` int(11) NOT NULL,
  `image` varchar(999) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`idRanking`),
  UNIQUE KEY `rank` (`rank`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `ranking`
--

INSERT INTO `ranking` (`idRanking`, `rank`, `description`, `min_points`, `max_points`, `image`) VALUES
(3, 'Ecuyer', 'Le plus faible rang est celui d''Ã©cuyer. A l''origine, c''Ã©tait le rang portÃ© par un gentilhomme (personne nÃ©e noble) ou un anobli (personne pas nÃ©e noble mais anoblie par sa fonction ou par lettre patente du roi) qui accompagnait un chevalier. Ce dernier Ã©tait alors en charge de l''Ã©cu (bouclier avec sa partie infÃ©rieur en forme ogivale) d''oÃ¹ le nom Ã©cuyer. Au fil du temps, on garda cette appellation pour les gentilshommes n''ayant pas de titre.', 0, 2000, 'images/badges/rank.png'),
(4, 'Chevalier', 'Le titre de chevalier prit de l''importance notamment grÃ¢ce Ã  l''Ã©glise et les croisades, mais surtout par le roman de ChrÃ©tien de Troyes, Les chevaliers de la Table Ronde au XII siÃ¨cle qui influencera les mentalitÃ©s. Les chevaliers Ã©taient au services d''un seigneur. On devient chevalier par la cÃ©rÃ©monie de l''adoubement. ', 2000, 10000, 'images/badges/ranking-cup.png'),
(5, 'Baron', 'Un baron est un seigneur tenant son fief directement du roi. Les fiefs sont de l''ordre des villes, grandes villes avec parfois une forÃªt. Le siÃ¨ge de la baronnie (terre d''un baron) Ã©tait souvent un chÃ¢teau. La baronnie est le synonyme au Moyen-Ã‚ge de la chÃ¢tellerie (plus petit dÃ©coupage administratif d''un domaine).', 10000, 50000, 'images/badges/rankings.png'),
(6, 'Vicompte', 'Le vicomte Ã©tait un officier de judicature du duc ou du comte, pour le gouvernement d''une ville ou d''une province ou le seigneur dâ€™une terre titrÃ©e Â« vicomtÃ© Â». Câ€™est donc un officier, il reprÃ©sente une autoritÃ© supÃ©rieure, notamment le comte ou le duc mais aussi, sâ€™il y en a un, le roi. Sachant cela, il est supÃ©rieur au baron dont le fief est obtenu par le roi mais ne reprÃ©sentant pas ce dernier.', 50000, 150000, 'images/badges/star.png'),
(7, 'Marquis', 'Le marquis est un comte spÃ©cial, il est le seigneur dâ€™un comtÃ© se trouvant aux frontiÃ¨res du domaine supÃ©rieur (royal ou ducal). Il Ã©tait ainsi le premier exposÃ© en cas dâ€™invasion ennemie. Ainsi on lui confÃ©ra des pouvoirs militaires lui permettant de lever le contingent de lâ€™armÃ©e sans avoir reÃ§u lâ€™ordre du souverain. Le but est ici de pouvoir rÃ©agir rapidement Ã  une attaque ennemie sans avoir Ã  demander lâ€™autorisation au souverain, ce qui signifie envoyer un message au chÃ¢teau et attendre son retour. Le marquis Ã  la tÃªte dâ€™un marquisat est donc un comte avec des prÃ©rogatives Ã©tendues.', 150000, 350000, 'images/badges/insignia.png');

-- --------------------------------------------------------

--
-- Structure de la table `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `rev_id` int(11) NOT NULL AUTO_INCREMENT,
  `review` longtext NOT NULL,
  `u_id` int(11) NOT NULL,
  `rew_date` varchar(255) NOT NULL,
  `avg` varchar(255) NOT NULL,
  `uniq` varchar(999) NOT NULL,
  `b_id` int(11) NOT NULL,
  `rev_active` int(11) NOT NULL,
  PRIMARY KEY (`rev_id`),
  KEY `u_id` (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `secteurs`
--

CREATE TABLE IF NOT EXISTS `secteurs` (
  `secteur_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `parent_sector` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`secteur_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=531 ;

--
-- Contenu de la table `secteurs`
--

INSERT INTO `secteurs` (`secteur_id`, `nom`, `parent_sector`) VALUES
(1, 'Aéronautique Et Espace', 0),
(2, 'Agriculture - Agroalimentaire', 0),
(3, 'Agroalimentaire - Industries Alimentaires', 0),
(4, 'Artisanat', 0),
(5, 'Audiovisuel, Cinéma', 0),
(6, 'Audit, Comptabilité, Gestion', 0),
(7, 'Automobile', 0),
(8, 'Banque, Assurance', 0),
(9, 'Bâtiment, Travaux Publics', 0),
(10, 'Biologie, Chimie, Pharmacie', 0),
(11, 'Commerce, Distribution', 0),
(12, 'Communication', 0),
(13, 'Création, Métiers D’art', 0),
(14, 'Culture, Patrimoine', 0),
(15, 'Défense, Sécurité', 0),
(16, 'Documentation, Bibliothèque', 0),
(17, 'Droit', 0),
(18, 'Edition, Livre', 0),
(19, 'Enseignement', 0),
(20, 'Environnement', 0),
(21, 'Ferroviaire', 0),
(22, 'Foires, Salons Et Congrès', 0),
(23, 'Fonction Publique', 0),
(24, 'Hôtellerie, Restauration', 0),
(25, 'Humanitaire', 0),
(26, 'Immobilier', 0),
(27, 'Industrie', 0),
(28, 'Informatique, Télécoms, Web', 0),
(29, 'Journalisme', 0),
(30, 'Langues', 0),
(31, 'Marketing, Publicité', 0),
(32, 'Médical', 0),
(33, 'Mode-Textile', 0),
(34, 'Paramédical', 0),
(35, 'Propreté Et Services Associés', 0),
(36, 'Psychologie', 0),
(37, 'Ressources Humaines', 0),
(38, 'Sciences Humaines Et Sociales', 0),
(39, 'Secrétariat', 0),
(40, 'Social', 0),
(41, 'Spectacle - Métiers De La Scène', 0),
(42, 'Sport', 0),
(43, 'Tourisme', 0),
(44, 'Transport-Logistique', 0),
(45, 'Agent de trafic', 1),
(46, 'Chef d’avion', 1),
(47, 'Contrôleur aérien', 1),
(48, 'Hôtesse de l’air ', 1),
(49, 'Steward', 1),
(50, 'Ingénieur aéronautique', 1),
(51, 'Mécanicien moteur', 1),
(52, 'Pilote de ligne', 1),
(53, 'Technicien supérieur de l’aviation)', 1),
(54, 'Acheteur dans l’agroalimentaire', 2),
(55, 'Assistante paysagiste ', 2),
(56, 'Auxiliaire vétérinaire', 2),
(57, 'Conducteur de machines agricoles', 2),
(58, 'Conseiller agricole', 2),
(59, 'Contrôleur laitier', 2),
(60, 'Éleveur', 2),
(61, 'Exploitant agricole', 2),
(62, 'Horticulteur', 2),
(63, 'Ingénieur agronome', 2),
(64, 'Ingénieur en agroalimentaire', 2),
(65, 'Œnologue', 2),
(66, 'Pépiniériste', 2),
(67, 'Responsable qualité', 2),
(68, 'Technicien agricole', 2),
(69, 'Technicien en aquaculture', 2),
(70, 'Vétérinaire', 2),
(71, 'Viticulteur', 2),
(72, 'Animateur logistique', 3),
(73, 'Attaché commercial', 3),
(74, 'Merchandising', 3),
(75, 'Conducteur de ligne de fabrication ou de conditionnement', 3),
(76, 'Conducteur de machine de fabrication ou de conditionnement', 3),
(77, 'Opérateur de fabrication', 3),
(78, 'Responsable d’atelier de fabrication ou de conditionnement', 3),
(79, 'Responsable de la supply chain', 3),
(80, 'Responsable de maintenance', 3),
(81, 'Responsable de production', 3),
(82, 'Technicien de maintenance', 3),
(83, 'Technicien qualité', 3),
(84, 'Bouchère', 4),
(85, 'Boulanger', 4),
(86, 'Charpentier', 4),
(87, 'Coiffeur', 4),
(88, 'Fleuriste', 4),
(89, 'Plombier', 4),
(90, 'Installateur Thermique', 4),
(91, 'Assistant(e) de production', 5),
(92, 'Assistant(e) réalisateur(trice)', 5),
(93, 'Cadreur(euse)', 5),
(94, 'Chargé(e) de diffusion', 5),
(95, 'Chef opérateur(trice)', 5),
(96, 'Ingénieur(e) de la vision', 5),
(97, 'Ingénieur(e) du son', 5),
(98, 'Monteur(euse)', 5),
(99, 'Producteur(trice)', 5),
(100, 'Réalisateur(trice)', 5),
(101, 'Régisseur(euse) général(e)', 5),
(102, 'Scénariste', 5),
(103, 'Scripte', 5),
(104, 'Truquiste', 5),
(105, 'Assistant comptable ', 6),
(106, 'Auditeur à la cour des comptes', 6),
(107, 'Auditeur financier', 6),
(108, 'Auditeur interne ', 6),
(109, 'Auditeur spécialisé', 6),
(110, 'Commissaire aux comptes', 6),
(111, 'Comptable', 6),
(112, 'Contrôleur de gestion', 6),
(113, 'Directeur comptable', 6),
(114, 'Directeur financier', 6),
(115, 'Expert-comptable', 6),
(116, 'Fiscaliste', 6),
(117, 'Responsable consolidation', 6),
(118, 'Trésorier', 6),
(119, 'Expert(e) automobile', 7),
(120, 'Ingénieur(e) d’études', 7),
(121, 'Mécanicien poids lourds ', 7),
(122, 'Mécanicien(ne) réparateur(trice)', 7),
(123, 'Peintre en carrosserie', 7),
(124, 'Vendeur d’équipement auto', 7),
(125, 'Vendeur(euse) automobile', 7),
(126, 'Actuaire', 8),
(127, 'Agent général d’assurance', 8),
(128, 'Analyste financier', 8),
(129, 'Banquier du commerce international', 8),
(130, 'Chargé de clientèle', 8),
(131, 'Conseiller bancaire ', 8),
(132, 'Courtier d’assurances ', 8),
(133, 'Credit manager', 8),
(134, 'Directeur d’agence bancaire', 8),
(135, 'Expert en assurances', 8),
(136, 'Gestionnaire assurance', 8),
(137, 'Gestionnaire de patrimoine', 8),
(138, 'Opérateur back office', 8),
(139, 'Trader', 8),
(140, 'Rédacteur dans les assurances', 8),
(141, 'Souscripteur', 8),
(142, 'Architecte', 9),
(143, 'Chargé d’affaires dans le BTP', 9),
(144, 'Chef de chantier', 9),
(145, 'Conducteur d’engins de chantier', 9),
(146, 'Conducteur de travaux', 9),
(147, 'Dessinateur-projeteur en bâtiment', 9),
(148, 'Électricien du BTP', 9),
(149, 'Ingénieur études de prix', 9),
(150, 'Ingénieur géomètre', 9),
(151, 'Maçon', 9),
(152, 'Ouvrier routier', 9),
(153, 'Peintre décorateur', 9),
(154, 'Bio-informaticien', 10),
(155, 'Créateur de parfum', 10),
(156, 'Hydrobiologiste', 10),
(157, 'Ingénieur chimiste', 10),
(158, 'Pharmacien', 10),
(159, 'Préparateur en pharmacie', 10),
(160, 'Responsable des affaires réglementaires', 10),
(161, 'Technicien d’analyses biomédicales', 10),
(162, 'Technicien de l’industrie pharmaceutique', 10),
(163, 'Acheteur', 11),
(164, 'Agent commercial', 11),
(165, 'Assistant export', 11),
(166, 'Chargé d’affaires', 11),
(167, 'Chef de rayon', 11),
(168, 'Commercial', 11),
(169, 'Directeur ', 11),
(170, 'Responsable de magasin', 11),
(171, 'Vendeuse esthéticienne', 11),
(172, 'Assistant en communication', 12),
(173, 'Attaché de presse', 12),
(174, 'Chargé de communication', 12),
(175, 'Directeur de la communication', 12),
(176, 'Journaliste d’entreprise', 12),
(177, 'Architecte d’intérieur', 13),
(178, 'Artiste plasticien', 13),
(179, 'Bijoutier - joaillier', 13),
(180, 'Céramiste', 13),
(181, 'Décorateur', 13),
(182, 'Designer industriel', 13),
(183, 'Dessinateur de BD', 13),
(184, 'Ébéniste', 13),
(185, 'Facteur d’instruments', 13),
(186, 'Game designer', 13),
(187, 'Graphiste', 13),
(188, 'Illustrateur', 13),
(189, 'Infographiste 2D-3D', 13),
(190, 'Modiste', 13),
(191, 'Photographe', 13),
(192, 'Professeur d’arts plastiques', 13),
(193, 'Réalisatrice multimédia', 13),
(194, 'Relieur-doreur', 13),
(195, 'Verrier', 13),
(196, 'Animateur(trice) du patrimoine', 14),
(197, 'Archéologue', 14),
(198, 'Commissaire-priseur(euse)', 14),
(199, 'Conservateur(trice) du patrimoine', 14),
(200, 'Galeriste', 14),
(201, 'Guide-conférencier(ière) de musée et monument', 14),
(202, 'Médiateur(trice) culturel(le)', 14),
(203, 'Régisseur(euse) d’œuvres d’art', 14),
(204, 'Restaurateur(trice) d’œuvres d’art', 14),
(205, 'Commissaire dans les armées', 15),
(206, 'Exploitant radio', 15),
(207, 'Fusilier marin', 15),
(208, 'Gendarme', 15),
(209, 'Médecin des armées', 15),
(210, 'Militaire du rang', 15),
(211, 'Officier Technique et administratif', 15),
(212, 'Officier de l’armée', 15),
(213, 'Pilote dans l’armée de l’air', 15),
(214, 'Pompier professionnel', 15),
(215, 'Sous-officier', 15),
(216, 'Archiviste', 16),
(217, 'Bibliothécaire', 16),
(218, 'Conservateur(trice) de bibliothèque', 16),
(219, 'Documentaliste', 16),
(220, 'Assistant parlementaire', 17),
(221, 'Avocat', 17),
(222, 'Clerc de notaire', 17),
(223, 'Greffier', 17),
(224, 'Huissier de justice', 17),
(225, 'Juge aux affaires familiales', 17),
(226, 'Juge d’instruction', 17),
(227, 'Juge de l’application des peines', 17),
(228, 'Juge des enfants', 17),
(229, 'Juriste d’entreprise', 17),
(230, 'Notaire', 17),
(231, 'Substitut du procureur', 17),
(232, 'Auteur(e)', 18),
(233, 'Chef(fe) de fabrication', 18),
(234, 'Correcteur(trice)', 18),
(235, 'Iconographe', 18),
(236, 'Libraire', 18),
(237, 'Responsable d’édition', 18),
(238, 'Conseiller(ère) principal(e) d’éducation', 19),
(239, 'Enseignant(e) à l’étranger', 19),
(240, 'Enseignant(e)-chercheur(euse)', 19),
(241, 'Formateur(trice) d’adultes', 19),
(242, 'Professeur(e) de collège et de lycée', 19),
(243, 'Professeur(e) de lycée professionnel ou technique', 19),
(244, 'Professeur(e) de musique', 19),
(245, 'Professeur(e) des écoles', 19),
(246, 'Professeur(e) du privé', 19),
(247, 'Acousticien(ne)', 20),
(248, 'Agent(e) des réseaux d’eau potable ', 20),
(249, 'Conseiller(ère) en environnement', 20),
(250, 'Garde-moniteur(trice)', 20),
(251, 'Garde-pêche', 20),
(252, 'Géologue', 20),
(253, 'Ingénieur(e) en analyse de l’air', 20),
(254, 'Ingénieur(e) hydrologue', 20),
(255, 'Juriste en environnement', 20),
(256, 'Paysagiste', 20),
(257, 'Responsable commercial(e) environnement', 20),
(258, 'Responsable de station d’épuration', 20),
(259, 'Responsable environnement', 20),
(260, 'Technicien(ne) de laboratoire (environnement)', 20),
(261, 'Technicien(ne) de mesure de la pollution', 20),
(262, 'Technicien(ne) de traitement des déchets', 20),
(263, 'Technicien(ne) supérieur(e) forestier(ière)', 20),
(264, 'Agent(e) d’escale ferroviaire (agence cadre)', 21),
(265, 'Agent(e) de la sûreté ferroviaire (agence transverse)', 21),
(266, 'Commercial(e) en gare (agence voyageurs)', 21),
(267, 'Conducteur(trice) de train (agence transverse)', 21),
(268, 'Ingénieur(e) d’études génie électrique (agence cadre)', 21),
(269, 'Manageur(euse) maintenance et travaux en génie civil (agence cadre)', 21),
(270, 'Opérateur(trice) de maintenance des trains (agence matériel)', 21),
(271, 'Opérateur(trice) de signalisation électrique (agence Infra)', 21),
(272, 'Technicien(ne) de la voie ferrée (agence Infra)', 21),
(273, 'Technicien(ne) de maintenance des trains (agence matériel)', 21),
(274, 'Architecte scénographe pour des foires, salons ou congrès', 22),
(275, 'Chargé(e) de sécurité sur des foires, salons ou congrès', 22),
(276, 'Chef de hall(s) sur des salons, foires ou congrès', 22),
(277, 'Chef de projet marketing NTIC', 22),
(278, 'Concepteur(trice) de stand sur des foires, salons, congrès', 22),
(279, 'Conseiller(ère) d’exposant sur des salons, foires ou congrès', 22),
(280, 'Décorateur(trice) floral(e) sur des foires, salons, congrès', 22),
(281, 'Directeur(trice) d’exploitation sur des salons, foires ou congrès', 22),
(282, 'Directeur(trice) de congrès-expositions', 22),
(283, 'Directeur(trice) de foire(s)', 22),
(284, 'Directeur(trice) de salons', 22),
(285, 'Directeur(trice) juridique pour des foires, salons ou congrès', 22),
(286, 'Directeur(trice) technique sur des foires, salons ou congrès', 22),
(287, 'Installateur(trice) général(e) sur des foires, salons, congrès', 22),
(288, 'Régisseur(euse) sur des salons, foires ou congrès', 22),
(289, 'Responsable commercial(e) et marketing pour des foires, salons ou congrès', 22),
(290, 'Responsable développement salon', 22),
(291, 'Responsable marketing pour des salons, foires ou congrès', 22),
(292, 'Responsable sécurité sur des foires, salons, congrès', 22),
(293, 'Tapissier(ière) sur des foires, salons, congrès', 22),
(294, 'Technicien(ne) sur des salons, foires ou congrès', 22),
(295, 'Administrateur(trice) de la fonction publique', 23),
(296, 'Agent(e) d’administration des finances publiques ', 23),
(297, 'Assistant(e) ingénieur(e)', 23),
(298, 'Attaché(e) de la fonction publique', 23),
(299, 'Commissaire de police', 23),
(300, 'Contrôleur(euse) des douanes', 23),
(301, 'Contrôleur(euse) des travaux publics de l’État', 23),
(302, 'Gardien(ne) de la paix', 23),
(303, 'Gardien(ne) de police municipale', 23),
(304, 'Ingénieur des travaux de la météorologie ', 23),
(305, 'Ingénieur(e) d’études et de fabrications', 23),
(306, 'Inspecteur(trice) des finances publiques (anciennement inspecteur des impôts)', 23),
(307, 'Inspecteur(trice) du permis de conduire et de la sécurité routière', 23),
(308, 'Inspecteur(trice) du travail', 23),
(309, 'Lieutenant(e) de Sapeurs-Pompiers', 23),
(310, 'Officier(ière) de police', 23),
(311, 'Rédacteur(trice) territorial(e)', 23),
(312, 'Secrétaire administratif de l’éducation nationale et de l’enseignement supérieur ', 23),
(313, 'Secrétaire de Chancellerie', 23),
(314, 'Secrétaire des affaires étrangères', 23),
(315, 'Surveillant(e) de l’administration pénitentiaire', 23),
(316, 'Technicien(ne) sanitaire', 23),
(317, 'Chef de cuisine', 24),
(318, 'Concierge', 24),
(319, 'Cuisinier', 24),
(320, 'Directeur(trice) d’hôtel', 24),
(321, 'Gérant(e) de restauration collective', 24),
(322, 'Maître (Maîtresse) d’hôtel', 24),
(323, 'Sommelier(ière)', 24),
(324, 'Yield manager', 24),
(325, 'Administrateur(trice) de mission', 25),
(326, 'Chef de mission', 25),
(327, 'Collecteur(trice) de fonds (fundraiser)', 25),
(328, 'Coordinateur(trice)', 25),
(329, 'Développeur(euse) local(e)', 25),
(330, 'Logisticien(ne)', 25),
(331, 'Personnel médical', 25),
(332, 'Responsable technique', 25),
(333, 'Administrateur(trice) de biens', 26),
(334, 'Agent(e) immobilier(ière)', 26),
(335, 'Chasseur(euse) de biens immobiliers', 26),
(336, 'Gestionnaire d’actifs immobiliers', 26),
(337, 'Gestionnaire de galerie marchande', 26),
(338, 'Gestionnaire social(e)', 26),
(339, 'Juriste immobilier', 26),
(340, 'Négociateur(trice) immobilier', 26),
(341, 'Syndic de copropriété', 26),
(342, 'Acheteur(euse) industriel(le)', 27),
(343, 'Automaticien(ne)', 27),
(344, 'Conducteur(trice) de ligne de production', 27),
(345, 'Dessinateur(trice) industriel(le)', 27),
(346, 'Etalagiste', 27),
(347, 'Ingénieur(e) calcul', 27),
(348, 'Ingénieur(e) commercial(e)', 27),
(349, 'Ingénieur(e) de production', 27),
(350, 'Ingénieur(e) méthodes', 27),
(351, 'Ingénieur(e) R & D', 27),
(352, 'Mécanicien(ne) outilleur(euse)', 27),
(353, 'Opérateur d’ouvrages chaudronnés ', 27),
(354, 'Opératrice sur machine', 27),
(355, 'Responsable d’ordonnancement', 27),
(356, 'Responsable QSE (qualité, sécurité, environnement)', 27),
(357, 'Tailleur(euse) de pierre', 27),
(358, 'Technicien électronicien(ne)', 27),
(359, 'Technicien qualité : le témoignage de Frédéric, apprenti', 27),
(360, 'Technicien(ne) contrôle', 27),
(361, 'Technicien(ne) d’essais', 27),
(362, 'Technicien(ne) d’études', 27),
(363, 'Technicien(ne) de maintenance', 27),
(364, 'Vendeur en électronique', 27),
(365, 'Administrateur(trice) de base de données', 28),
(366, 'Architecte de système d’information', 28),
(367, 'Business analyst', 28),
(368, 'Chef de projet informatique', 28),
(369, 'Concepteur(trice) Web', 28),
(370, 'Consultant(e) en référencement de sites Web (consultant SEM/SEO)', 28),
(371, 'Data scientist', 28),
(372, 'Développeur front end', 28),
(373, 'Développeur(euse)', 28),
(374, 'Hotliner', 28),
(375, 'Ingénieur(e) réseaux', 28),
(376, 'Ingénieur(e) sécurité', 28),
(377, 'Ingénieur(e) technico-commercial(e)', 28),
(378, 'Installateur télécoms : le témoignage de Christophe, apprenti', 28),
(379, 'Référenceur(euse)', 28),
(380, 'Technicien(ne) en informatique industrielle', 28),
(381, 'Technicien(ne) en télécommunications', 28),
(382, 'Vendeur(euse) en micro-informatique', 28),
(383, 'Webdesigner', 28),
(384, 'Webmaster', 28),
(385, 'Agencier(ière)', 29),
(386, 'Journaliste radio', 29),
(387, 'Journaliste reporter d’images', 29),
(388, 'Journaliste web', 29),
(389, 'Maquettiste de presse', 29),
(390, 'Photographe de presse', 29),
(391, 'Rédacteur(trice) en chef', 29),
(392, 'Rédacteur(trice) en presse écrite', 29),
(393, 'Secrétaire de rédaction', 29),
(394, 'Interprète', 30),
(395, 'Professeur(e) de FLE (français langue étrangère)', 30),
(396, 'Professeur(e) de langue vivante étrangère', 30),
(397, 'Terminologue', 30),
(398, 'Traducteur(trice) audiovisuel(le)', 30),
(399, 'Traducteur(trice) dans la fonction publique', 30),
(400, 'Traducteur(trice) expert(e)', 30),
(401, 'Traducteur(trice) littéraire', 30),
(402, 'Traducteur(trice) technique', 30),
(403, 'Chargé(e) d’études marketing', 31),
(404, 'Chargé(e) de marketing direct', 31),
(405, 'Chargé(e) de promotion', 31),
(406, 'Chef de produit', 31),
(407, 'Chef de publicité', 31),
(408, 'Concepteur(trice)-rédacteur(trice)', 31),
(409, 'Directeur(trice) artistique', 31),
(410, 'Directeur(trice) de clientèle', 31),
(411, 'Directeur(trice) de création', 31),
(412, 'Directeur(trice) du marketing', 31),
(413, 'Maquettiste de pub', 31),
(414, 'Médiaplanneur(euse)', 31),
(415, 'Responsable base de données', 31),
(416, 'Responsable du merchandising', 31),
(417, 'Anesthésiste-réanimateur(trice)', 32),
(418, 'Chirurgien(ne)', 32),
(419, 'Chirurgien(ne)-dentiste', 32),
(420, 'Dermatologue', 32),
(421, 'Médecin de l’Éducation nationale', 32),
(422, 'Médecin du sport', 32),
(423, 'Médecin du travail', 32),
(424, 'Médecin généraliste', 32),
(425, 'Médecin régulateur', 32),
(426, 'Médecin urgentiste', 32),
(427, 'Ophtalmologue', 32),
(428, 'Oto-rhino-laryngologiste', 32),
(429, 'Pédiatre', 32),
(430, 'Psychiatre', 32),
(431, 'Sage-femme', 32),
(432, 'Acheteur(euse) habillement', 33),
(433, 'Chef de produit', 33),
(434, 'Designer textile', 33),
(435, 'Ingénieur(e) textile', 33),
(436, 'Modéliste', 33),
(437, 'Styliste', 33),
(438, 'Technicien(ne) de fabrication', 33),
(439, 'Aide-soignant(e)', 34),
(440, 'Ambulancier(ière)', 34),
(441, 'Assistant(e) dentaire', 34),
(442, 'Audioprothésiste', 34),
(443, 'Auxiliaire de puériculture', 34),
(444, 'Diététicien(ne)', 34),
(445, 'Ergothérapeute', 34),
(446, 'Infirmier(e) puériculteur(trice)', 34),
(447, 'Infirmier(ière)', 34),
(448, 'Manipulateur(trice) en électroradiologie médicale', 34),
(449, 'Masseur(euse)-kinésithérapeute', 34),
(450, 'Opticien(ne)-lunetier(ière)', 34),
(451, 'Orthophoniste', 34),
(452, 'Orthoptiste', 34),
(453, 'Ostéopathe, chiropracteur(trice)', 34),
(454, 'Pédicure-podologue', 34),
(455, 'Podo-orthésiste', 34),
(456, 'Prothésiste dentaire', 34),
(457, 'Psychomotricien(ne)', 34),
(458, 'Secrétaire médical(e)', 34),
(459, 'Technicien(ne) de laboratoire (paramédical)', 34),
(460, 'Visiteur(euse) médical(e)', 34),
(461, 'Agent d’entretien et de rénovation', 35),
(462, 'Agent machiniste', 35),
(463, 'Chef d’équipe', 35),
(464, 'Laveur de vitres en hauteur', 35),
(465, 'Responsable de secteur', 35),
(466, 'Technicien(ne) qualité, sécurité et développement durable', 35),
(467, 'Conseiller(ère) d’orientation-psychologue', 36),
(468, 'Psychologue clinicien(ne)', 36),
(469, 'Psychologue du travail', 36),
(470, 'Psychologue scolaire', 36),
(471, 'Psychothérapeute', 36),
(472, 'Assistant(e) des ressources humaines', 37),
(473, 'Chargé(e) du recrutement', 37),
(474, 'Directeur(trice) des ressources humaines', 37),
(475, 'Gestionnaire de carrières', 37),
(476, 'Juriste social(e)', 37),
(477, 'Outplacer', 37),
(478, 'Cartographe', 38),
(479, 'Démographe', 38),
(480, 'Géographe', 38),
(481, 'Historien(ne)', 38),
(482, 'Sociologue', 38),
(483, 'Urbaniste', 38),
(484, 'Assistant(e) commercial(e)', 39),
(485, 'Assistant(e) de manager', 39),
(486, 'Assistant(e) trilingue', 39),
(487, 'Secrétaire administratif(ve)', 39),
(488, 'Secrétaire comptable', 39),
(489, 'Secrétaire de mairie', 39),
(490, 'Secrétaire juridique', 39),
(491, 'Secrétaire médical(e)', 39),
(492, 'Animateur(trice) socioculturel(le)', 40),
(493, 'Assistant(e) de service social', 40),
(494, 'Conseiller(ère) en économie sociale et familiale', 40),
(495, 'Conseiller(ère) pénitentiaire d’insertion et de probation', 40),
(496, 'Directeur(trice) de structure sociale', 40),
(497, 'Éducateur(trice) de jeunes enfants', 40),
(498, 'Éducateur(trice) de la protection judiciaire de la jeunesse', 40),
(499, 'Éducateur(trice) spécialisé(e)', 40),
(500, 'Éducateur(trice) technique spécialisé(e)', 40),
(501, 'Chanteur(euse)', 41),
(502, 'Comédien(ne)', 41),
(503, 'Costumier(ière)', 41),
(504, 'Danseur(euse)', 41),
(505, 'Décorateur(trice)-scénographe', 41),
(506, 'Musicien(ne)', 41),
(507, 'Directeur(trice) d’équipement sportif', 42),
(508, 'Entraîneur(euse)', 42),
(509, 'Journaliste sportif(ve)', 42),
(510, 'Maître-nageur(euse) sauveteur(euse)', 42),
(511, 'Moniteur(trice) de sport', 42),
(512, 'Professeur(e) d’EPS (éducation physique et sportive)', 42),
(513, 'Professeur(e) de sport', 42),
(514, 'Vendeur(euse) d’articles de sport', 42),
(515, 'Agent(e) de comptoir (vendeur(euse)-conseil)', 43),
(516, 'Animateur(trice)', 43),
(517, 'Chef(fe) de produit touristique', 43),
(518, 'Guide-accompagnateur(trice)', 43),
(519, 'Guide-conférencier(ière)', 43),
(520, 'Hôtesse d’accueil', 43),
(521, 'Attaché commercial', 44),
(522, 'Chef d’agence de transport', 44),
(523, 'Conducteur(trice) de train', 44),
(524, 'Conducteur(trice) routier(ière)', 44),
(525, 'Contrôleuse de train : le témoignage de Ilham, apprentie', 44),
(526, 'Gestionnaire de stocks', 44),
(527, 'Officier(ière) de la marine marchande', 44),
(528, 'Responsable d’entrepôt', 44),
(529, 'Responsable d’exploitation', 44),
(530, 'Responsable logistique', 44);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `site_title` varchar(500) NOT NULL,
  `site_link` varchar(999) NOT NULL,
  `meta_keywords` varchar(999) NOT NULL,
  `meta_description` varchar(999) NOT NULL,
  `home_text` varchar(999) NOT NULL,
  `site_email` varchar(500) NOT NULL,
  `county` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `fb_app_id` varchar(999) NOT NULL,
  `fb_secret_key` varchar(500) NOT NULL,
  `fb_page` varchar(999) NOT NULL,
  `twitter_link` varchar(999) NOT NULL,
  `pinterest_link` varchar(999) NOT NULL,
  `google_pluse_link` varchar(999) NOT NULL,
  `active` int(11) NOT NULL,
  `rev_active` int(11) NOT NULL,
  `template` varchar(256) NOT NULL,
  `site_views` int(11) NOT NULL,
  `vertion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `site_link`, `meta_keywords`, `meta_description`, `home_text`, `site_email`, `county`, `zip`, `fb_app_id`, `fb_secret_key`, `fb_page`, `twitter_link`, `pinterest_link`, `google_pluse_link`, `active`, `rev_active`, `template`, `site_views`, `vertion`) VALUES
(1, 'Weona', 'localhost/weona/', '', '', 'Search Local Businesses', 'jessica.h.curpen@gmail.com', '', '', '', '', '', '', '', '', 1, 1, 'default', 5890, '1.0.0');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(999) NOT NULL,
  `country` varchar(500) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `about` varchar(999) NOT NULL,
  `avatar` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `registered_date` varchar(255) NOT NULL,
  `idRanking` int(11) NOT NULL DEFAULT '3',
  `money` int(11) NOT NULL DEFAULT '100',
  `points` int(11) NOT NULL DEFAULT '50',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `mid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `tags` varchar(999) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `idRanking` (`idRanking`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`user_id`, `username`, `firstname`, `lastname`, `email`, `country`, `gender`, `birthday`, `about`, `avatar`, `password`, `registered_date`, `idRanking`, `money`, `points`, `verified`, `mid`, `pid`, `tags`) VALUES
(27, 'Jessica.Curpen', 'Jessica', 'Curpen', 'jessica.curpen@clinkast.fr', '', '', '', 'PÃªche, pÃªche, pÃªche, pÃªeeeeeechhhhhhhhhhhhhhe', '146157377127.png', '86f34e48b72eed511ecf38eb7695de6a', 'April 20, 2016', 4, 130, 2310, 0, NULL, NULL, 'erp,sap'),
(29, 'Sterling.Archer', 'Sterling', 'Archer', 'sterlingarcher@armyspy.com', 'US', 'Male', '1985/04/04', 'The World''s best secret agent ever, ever, ever !', '146131192129.jpg', '80da5ba91f238b832faec0a4492f4538', 'April 21, 2016', 4, 0, 2310, 0, NULL, NULL, ''),
(30, 'Constantine', 'John', 'Constantine', 'constantine@armyspy.com', 'US', '', '2015/10/26', 'HellBlazer\nI''m the one who steps from the shadows, all trenchcoat and cigarette and arrogance, ready to deal with the madness. Oh, I''ve got it all sewn up. I can save you. If it takes the last drop of your blood, I''ll drive your demons away. I''ll kick them in the bollocks and spit on them when they''re down and then I''ll be gone back into darkness, leaving only a nod and a wink and a wisecrack. I walk my path alone... who would walk with me?', '146132256330.jpg', '2f2aa5570eb38a679d2690116090ff65', 'April 22, 2016', 3, 0, 350, 0, NULL, NULL, ''),
(31, 'lana.kane', 'Lana', 'Kane', 'lanakane@armyspy.com', 'US', '', '1987/03/28', 'You know what ?', '146166113131.png', 'bb86940e9abf0808d77baa5793033fe9', 'April 26, 2016', 3, 10, 210, 0, NULL, NULL, ''),
(32, 'Saul.Goodman', 'Jimmy', 'McGill', 'bettercallsaul@armyspy.com', 'US', 'Male', '1977/03/30', 'Its Saul Goodman', '146168550532.jpg', '1769e8bae29583759fde7c42666ae2a7', 'April 26, 2016', 3, 700, 700, 0, NULL, NULL, ''),
(33, 'pam', 'Pam', 'Jefferson', 'pamjefferson@armyspy.com', '', '', '', '', '', '14ce1924b9954365c29b7593faea2237', 'July 12, 2016', 3, 100, 50, 0, NULL, NULL, ''),
(34, 'kipee.Scoop', 'Kipee', 'Scoop', 'kipeescoop@armyspy.com', '', '', '', '', '', '7f66fef5f7c64d87a26256450de4750f', 'July 12, 2016', 3, 100, 50, 0, 29, 389, '');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `business`
--
ALTER TABLE `business`
  ADD CONSTRAINT `business_ibfk_1` FOREIGN KEY (`biz_user`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `discussions`
--
ALTER TABLE `discussions`
  ADD CONSTRAINT `discussions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_ibfk_1` FOREIGN KEY (`idFollowed`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `followers_ibfk_2` FOREIGN KEY (`idFollower`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `likes_produits`
--
ALTER TABLE `likes_produits`
  ADD CONSTRAINT `likes_produits_ibfk_1` FOREIGN KEY (`idProduit`) REFERENCES `produits` (`idProduit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_produits_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiverID`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `participation`
--
ALTER TABLE `participation`
  ADD CONSTRAINT `participation_ibfk_1` FOREIGN KEY (`idparticipation`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `participation_ibfk_2` FOREIGN KEY (`idDiscussion`) REFERENCES `discussions` (`id_discussion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
