<?php

	include("header.php");
	
	$id = $mysqli->escape_string($_GET['id']);
	
	if(!isset($_SESSION['email']))
	{
		?>
			<script type="text/javascript">
				function leave() 
				{
					window.location = "login";
				}
				setTimeout("leave()", 2);
			</script>
			
		<?php 
	}
	else
	{?>

			<script>
					$(document).ready(function()
					{
						$('#SubmitForm').on('submit', function(e)
						{
							e.preventDefault();
							$('#submitButton').attr('disabled', ''); // disable upload button
							//show uploading message
							$("#output").html('<div class="alert alert-info" role="alert">Votre mail va être transféré</div>');
							
							$(this).ajaxSubmit({
							target: '#output',
							success:  afterSuccess //call function after success
							});
						});
					});
					 
					function afterSuccess()
					{	
						 
						$('#submitButton').removeAttr('disabled'); //enable submit button
					   
					}

			</script>
				<div class="container container-main">
				
					<div class="col-md-8"> 
				
						<div class="col-shadow">
						
								<div class="biz-title-2">
									<h1>Contacter un Utilisateur</h1>
								</div>
								
								<div class="col-desc">
								<form id="SubmitForm" class="forms" action="submit_contact_user.php" method="post">
									<div id="output"></div>
									
									<?php
									
												if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE user_id='$id'"))
												{

													$ProfileInfo = mysqli_fetch_array($ProfileSql);
													
													$ProfileAuthor	 = stripslashes($ProfileInfo['username']);
													$idRanking = $ProfileInfo['idRanking'];
													$ProfileAvatar = $ProfileInfo['avatar'];
													$workplace = $ProfileInfo['workplace']; 
														
													$ProfileSql->close();
													
												}
												else{
													 
													 printf("Un problème est survenu.");
													 
												}
												
												if (empty($ProfileAvatar))
												{ 
													$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
												}
												elseif (!empty($ProfileAvatar))
												{
													$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
												}
									
									?>
									
									<p class="bg-info lead" style="padding : 5px ; margin-bottom : 10px">
										<img src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=50&amp;w=50&amp;q=100" alt="<?php echo ucfirst($ProfileAuthor);?>" class="img-circle" style="margin-right: 10px;"> 
										Contacter <?php echo $ProfileAuthor ?>
									</p>
									
									
									
										<input type="text" id="senderID" name="senderID" value = "<?php echo $UserId?>" class="user-iden" >
										<input type="text" id="receiverID" name="receiverID" value = "<?php echo $id?>" class="user-iden" >
										
										<div class="form-group">
										  <label for="inputObjet">Objet</label>
										  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
											<input type="text" class="form-control" name="inputObjet" id="inputObjet" placeholder="Objet">
										  </div>
										</div>
										
										<div class="form-group">
										  <label for="inputMail">Adresse mail</label>
										  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-comment"></span></span>
											<textarea class="form-control" name="inputDesc" id="inputDesc" placeholder="Adresse mail" rows="7">
											</textarea>
										  </div>
										</div>
										
										<!--
										<div class="form-group">
											<div class="checkbox">
											  <label><input type="checkbox" value="1">Recevoir une copie du mail</label>
											</div>
										</div>-->
										
										
										
										<button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Contacter</button>
											
									</form>
								</div>
								
						</div>
						
					</div>
				
				</div>
<?php }
	
?>

