<?php 
include("header.php");

$Uname = $_SESSION['username'];
$Uemail = $_SESSION['email'];

//if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE username='$Uname'"))
if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE username='$Uemail'"))
{
    $ProfileInfo = mysqli_fetch_array($ProfileSql);
	$name = $ProfileInfo['username'];	
	$id = $ProfileInfo['user_id'];	
	$ProfileSql->close();
}
else
{
     printf("Un problème est survenu."); 
}

?>


<div class="container container-main2">

	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="col-shadow">
				<div class="biz-title-2">
					<h1>Discussion</h1>
				</div>
				
				<div class= "disc-info" > 
					
					<table class = "topic-table">	
						<tr>
							<td><div class="dis-head" id="o">Titre</div></td>
						</tr>
						
						<tr>
							<td>
								<div class = "sujet-div">
									<textarea id="oT" autofocus name="sujet-disc" class="sujet-text" ></textarea>
								</div>
							</td>
						</tr>
						
						<tr>
							<td><div class="dis-head" id="t">Description</div></td>
						</tr>
						
						<tr>
							<td>
								<div class = "description-div">
									<textarea id="tt" name="description-disc" class="description-text"></textarea>
								</div>
							</td>
						</tr>
						
						<tr>
							<td><div class="dis-head" id="th">Catégories</div></td>
						</tr>
						
						<tr>
							<td><?php include("list_categories.php") ?></td>
						</tr>
						<tr>
							<td>
								<div class="dis-head">Choix Catégorie : </div> 
								<div class="choice-cat">
									<input type="text" name="choix_cat" class="no-style" id="disp-name">
									<input type="text" name="choix_cat2" class="no-style2" id="disp-name-not" readonly>
								</div>
							</td>
						</tr>
						
						<tr>
							<td> 
								<div style="width: 200px; height: 100px; margin-top: 20px;">
									<div class="button-style raised iris" id="submit-disc">
										<div class="center" fit>Poster</div>
										<paper-ripple fit></paper-ripple>
									</div> 
								</div> 
							</td>
						</tr>
					</table>
					
				</div>
			</div>
	</div>
	<div class="col-md-2"></div>
</div>

<?php include("footer.php");?>