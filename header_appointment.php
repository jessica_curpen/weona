<?php 
session_start();

include("db.php");
include("function.php");



//Get Site Settings

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteLink = $Settings['site_link'];
	
	$SiteTitle = $Settings['site_title'];
	
	$FaceBook = $Settings['fb_page'];
	
	$Twitter = $Settings['twitter_link'];
	
	$Pinterest = $Settings['pinterest_link'];
	
	$Gplus = $Settings['google_pluse_link'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

//Get User Info

if(isset($_SESSION['email']))
{
	
		$LoggedUser = $_SESSION['username'];
		$LoggedUserEmail = $_SESSION['email'];

		if($GetUser = $mysqli->query("SELECT * FROM users WHERE email='$LoggedUserEmail'"))
		{

			$UserInfo = mysqli_fetch_array($GetUser);
			
			$LoggedUsername = strtolower($UserInfo['username']);
			
			$LoggedUser = $UserInfo['username'];
			
			$LoggedUserLink   = preg_replace("![^a-z0-9]+!i", "-", $LoggedUsername);
			
			$LoggedUserLink	  = strtolower($LoggedUserLink);

			$UserId = $UserInfo['user_id'];
			
			$UserEmail = $UserInfo['email'];
			
			$LoggedUserAvatar = $UserInfo['avatar'];
				
			$GetUser->close();
	
		}
		else
		{
			printf("There Seems to be an issue");
	 
		}

}
else
{

$UserId = 0;
header('Location: index.php');
		exit;
}


	if (empty($LoggedUserAvatar))
	{ 
		$LoggedUserAvatarPic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
	}
	elseif (!empty($LoggedUserAvatar))
	{
		$LoggedUserAvatarPic =  'http://'.$SiteLink.'/avatars/'.$LoggedUserAvatar;
	}

//Ads

if($AdsSql = $mysqli->query("SELECT * FROM advertisements WHERE id='1'")){

    $AdsRow = mysqli_fetch_array($AdsSql);
	
	$Ad1 = $AdsRow['ad1'];
	$Ad2 = $AdsRow['ad2'];
	$Ad3 = $AdsRow['ad3'];

    $AdsSql->close();

}else{
	
     printf("There Seems to be an issue");
}
	

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Prendre rendez-vous</title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<link href='fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='fullcalendar/lib/jquery.min.js'></script>
<script src='fullcalendar/lib/moment.min.js'></script>
<script src='fullcalendar/fullcalendar.min.js'></script>


<link href="templates/<?php echo $Settings['template'];?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/style.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/animate.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.raty.js"></script>
<script src="js/wow.min.js"></script>




<?php
		$biz_num 	= $mysqli->escape_string($_GET['idbiz']);;
		$user_id 	= $UserId;
		$owner_id 	= $mysqli->escape_string($_GET['idOwner']);;
		
						if($getbusinneshours = $mysqli->query("SELECT * FROM hours WHERE unique_hours='$biz_num'"))
						{
								$numberofdays = $getbusinneshours->num_rows;
								
								if($numberofdays == 0)
								{
									$startTime = "08:00:00";
									$endTime = "18:00:00";
								}
								else
								{
											$allhoursMorning = array();
											$allhoursAfternoon = array();
											$alldays = array();
											
											while($gethour = mysqli_fetch_array($getbusinneshours))
											{
													$startHour = $gethour["open_from"];
													array_push($allhoursMorning, $startHour);
													
													$endHour = $gethour["open_till"];
													array_push($allhoursAfternoon, $endHour);
													
													$day = $gethour["day"];
													array_push($alldays, $day);
													
											}
											$startTime 	= min($allhoursMorning);
											$endTime 	= max($allhoursAfternoon);
											
											$fixedTimeStart = $startTime;
											$fixedTimeEnd = $endTime;
											
											$startTime = str_replace(".",":",$startTime);
											$endTime   = str_replace(".",":",$endTime);
											
											$startTime  = $startTime.':00' ;
											$endTime	= $endTime.':00';
											
											
											$hidden = array();
											if(!in_array("Lun", $alldays))
											{
												array_push($hidden,'1');
											}
											if(!in_array("Mar", $alldays))
											{
												array_push($hidden,'2');
											}
											if(!in_array("Mer", $alldays))
											{
												array_push($hidden,'3');
											}
											if(!in_array("Jeu", $alldays))
											{
												array_push($hidden,'4');
											}
											if(!in_array("Ven", $alldays))
											{
												array_push($hidden,'5');
											}
											if(!in_array("Sam", $alldays))
											{
												array_push($hidden,'6');
											}
											if(!in_array("Dim", $alldays))
											{
												array_push($hidden,'0');
											}
											
									}
									
									$getbusinneshours->close();
								}
								else
									{
										echo 'Un problème est survenu.';
									}
								
					//Get Start Day for calender (in order to prevent appointment in past dates)	
					$tempDate = date("Y-m-d");
					$Aday = date('D', strtotime( $tempDate)); echo getDayNum($Aday);
					
					
					
					$sqlBiz = "Select * from business where unique_biz='$biz_num'";
														if($resBiz = $mysqli->query($sqlBiz))
														{
															$BINF = mysqli_fetch_array($resBiz);
															
															$B_name_I_S = $BINF['business_name'];
															$B_img_I_S = $BINF['featured_image'];
															
															$resBiz->close();
														}
												
					$B_img_I_S = '<img class="img-responsive" src="thumbs.php?src=http://'.$SiteLink.'/uploads/' .$B_img_I_S.'&amp;h=150&amp;w=250&amp;q=100" alt="' .$B_name_I_S.'" >';
					
?>

<script>

		var biz_num = <?php echo $biz_num ?> ;
		var user_id = <?php echo $user_id ?> ;
		var owner_id = <?php echo $owner_id ?> ;
		var businessName = '<?php echo $B_name_I_S  ?>';
		var businessIMG = '<?php echo $B_img_I_S  ?>';
</script>

<script>

	function getDayName(d) 
	{
		if(d == 0)
		{
			return "Dimanche";
		}
		
		if(d == 1)
		{
			return "Lundi";
		}
		
		if(d == 2)
		{
			return "Mardi";
		}
		
		if(d == 3)
		{
			return "Mercredi";
		}
		
		if(d == 4)
		{
			return "Jeudi";
		}
		
		if(d == 5)
		{
			return "Vendredi";
		}
		
		if(d == 6)
		{
			return "Samedi";
		}
	}
	

</script>

<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: 
			{
				left: 'prev, next, today',
				center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			defaultView: 'agendaWeek',
			minTime: <?php echo '"'.$startTime.'"'?>,
			maxTime: '<?php echo $endTime?>',
			defaultDate: '<?php echo date("Y-m-d") ?>',
			firstDay: '<?php echo $Aday ?>',
			timezone: "France/Paris",
			<?php
			
					if($numberofdays == 0 )
					{
						
					}
					else
					{
						?>
						hiddenDays: 
						[ 
							<?php
									for($i = 0 ;$i<sizeof($hidden);$i++)
									{
										echo $hidden[$i];
										
										if($i < sizeof($hidden)-1)
										{
											echo ',';
										}
									}
							?>	
						],
						<?php
					}
			?>
			eventDurationEditable: false,
			eventStartEditable: false,
			selectable: true,
			selectHelper: true,
			select: 
			function(start, end, event) 
			{
				//alert(moment(start) +" "+ end);
				//$( "#modalButt" ).trigger( "click" );
				start 	= moment(start).format();
				end 	= moment(end).format();
				$("[name='start']").val(start);
				$("[name='end']").val(end);
				//alert(start +" "+ end);
				
					var d = new Date(start);
					var n = d.getDay();
					var day = getDayName(n);
					
					var date = start.substring(0,10);
					var dateAr = date.split('-');
					var newDate = dateAr[1] + '-' + dateAr[2] + '-' + dateAr[0].slice(-2);
					
					var start = start.substring(11,16);
					var end = end.substring(11,16);
					
					var t = new Date(newDate);
					var c = '<?php echo date("Y/m/d")?>';
					c = new Date(c);
					
					if(t.getFullYear() < c.getFullYear())
					{
						
					}
					else if (t.getMonth() < c.getMonth())
					{
						
					}
					else if(t.getDate() < c.getDate())
					{	
						
							
					}
					else
					{
						$( "#modalButt" ).trigger( "click" );
					}
					
					$("#daySave").html(day + " le " + newDate );
					$("#start-timeSave").html(" de " + start + " à " + end);
					
					$("#biz-nameSave").html(businessName);
					$("#business-imgSave").html(businessIMG);
				
				$('#save').click(function()
				{ 
					
					var title = $("[name='inputtitle']").val();
					var start = $("[name='start']").val();
					var end = $("[name='end']").val();
					
					$("[name='inputtitle']").val("");
					
					var eventData;
					if (title) 
					{
						eventData = 
						{
							title: title,
							start: start,
							end: end
						};
						
						$.ajax({
											type: "POST",
											url: "add_appointment.php",
											data:{ biz_num : biz_num,
												   user_id : user_id,
												   owner_id : owner_id,
												   title : title,
												   start : start,
												   end 	  : end
												 }
						});
						$( "#close" ).trigger( "click" );
						$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
					
					}
					$('#calendar').fullCalendar('unselect');
					
				});
				
			},
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: 
			[
				// Check for Not availaible time slots
						// Check closed hours
						<?php
						$previousEvent = 0;
						if($getbusinneshours = $mysqli->query("SELECT * FROM hours WHERE unique_hours='$biz_num'"))
									{
											$countbh = $getbusinneshours -> num_rows;
											if($countbh == 0)
											{
												
											}
											else
											{
												
											
												while($gethour = mysqli_fetch_array($getbusinneshours))
												{
														
														$h1 = $gethour["open_from"];
														$day = $gethour["day"];
														
														$num = getDayNum($day);
														
														if($fixedTimeStart < $h1)
														{
															
															$h1 = str_replace(".",":",$h1);
															$h1 = $h1.':00';
															
															$temp = str_replace(".",":",$fixedTimeStart);
															$temp = $temp.':00';
															
															if($previousEvent == 1)
															{
																echo ',';
																$previousEvent --;
															}
															
															?>
																{
																		title: 'Not available',
																		start: '<?php echo $temp?>',
																		end:   '<?php echo $h1?>',
																		dow: [<?php echo $num ?>],
																		color: '#E0E0E0'
																}
															<?php
															$previousEvent ++;
														}
														
														
														
														
														$h2 = $gethour["open_till"];
														$day = $gethour["day"];
														
														$num = getDayNum($day);
														
														if($fixedTimeEnd > $h2)
														{
															
															$h2 = str_replace(".",":",$h2);
															$h2 = $h2.':00';
															
															$temp = str_replace(".",":",$fixedTimeEnd);
															$temp = $temp.':00';
															
															if($previousEvent == 1)
															{
																echo ',';
																$previousEvent --;
															}
															
															?>
																{
																		title: 'Fermé',
																		start: '<?php echo $h2?>',
																		end:   '<?php echo $temp?>',
																		dow: [<?php echo $num ?>],
																		color: '#E0E0E0' //Grey
																}
															<?php
															$previousEvent ++;
														}
												}
											}
											
											$getbusinneshours->close();
									}
									else
									{
											echo 'Un problème est survenu.';
									}
									
									?>
				
						// Check for already taken time slots (for the business owner)
						<?php 
						$sql = "Select * from appointment where owner_id='$owner_id'";

						if($result = $mysqli->query($sql))
						{
									$count = $result->num_rows;
									
									if($count == 0)
									{
										
									}
									else
									{
										while($row = mysqli_fetch_array($result))
										{
											
											$start = $row["start"];
											$end = $row["end"];
											
											$userMeetingID = $row["user_id"];
											
											if($userMeetingID == $user_id)
											{
												
											}
											else
											{
												if($previousEvent == 1)
															{
																echo ',';
																$previousEvent --;
															}
											
													?>
													{
															title: 'Pris',
															start: '<?php echo $start?>',
															end:   '<?php echo $end?>',
															color: '#000' //Black
													}
													<?php
													$previousEvent ++;	
													}
											
													
										}	
									}
									
									$result->close();							
									
						}
						else
						{
							echo 'Un problème est survenu.';
						} ?>
						
						
						
						// Check for this user's meeting with this business and other businesses
						<?php $sql = "Select * from appointment where user_id='$user_id'";

						if($result = $mysqli->query($sql))
						{
									$countbgh = $result->num_rows;
									
									if($countbgh == 0)
									{
										
									}
									else
									{
										while($row = mysqli_fetch_array($result))
										{
											$title = $row["title"];
											$start = $row["start"];
											$end = $row["end"];
											
											$confirmed = $row["confirmed"];
											
											$appID = $row["id"];
											
											$OID = $row["owner_id"];
											$BID = $row["biz_id"];
											
											if($previousEvent == 1)
															{
																echo ',';
																$previousEvent --;
															}
											
											?>
											{
													title: '<?php echo $title?>',
													start: '<?php echo $start?>',
													end:   '<?php echo $end?>',
													appID : '<?php echo $appID?>',
													ownerID:'<?php echo $OID?>',
													userID:'<?php echo $user_id?>',
													
													<?php
														if($OID == 0 && $BID == 0)
														{
																		?>
																			color: '#763568' // Iris
																		<?php
															
														}
														else
														{
															$sqlOwner = "Select * from users where user_id='$OID'";
															if($resOwner = $mysqli->query($sqlOwner))
															{
																$ownerINF = mysqli_fetch_array($resOwner);
																$owner_name_I = $ownerINF['username'];
																$ProfileAvatar = $ownerINF['avatar'];
																if (empty($ProfileAvatar))
																{ 
																	$owner_img_I =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
																}
																elseif (!empty($ProfileAvatar))
																{
																	$owner_img_I =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
																}
																?>
																owner_name: '<?php echo $owner_name_I ?>',
																owner_img: '<?php echo $owner_img_I ?>',
																<?php
																
																$sqlB = "Select * from business where unique_biz='$BID'";
																if($resB = $mysqli->query($sqlB))
																{
																	$BINF = mysqli_fetch_array($resB);
																	$B_name_I = $BINF['business_name'];
																	$B_img_I = $BINF['featured_image'];
																	$B_adr_I = $BINF['address_1'];
																	$B_adr2_I = $BINF['address_2'];
																	$B_city_I = $BINF['city'];
																	?>
																	business_name: '<?php echo $B_name_I ?>',
																	business_img: '<?php echo $B_img_I ?>',
																	business_adr: '<?php echo $B_adr_I ." ".$B_adr2_I." ".", ".$B_city_I ?>',
																	<?php
																	
																	if($confirmed == 0)
																	{
																		?>
																			color: '#B71C0C' // Red
																		<?php
																	}
																	else
																	{
																		?>
																			color: '#6AC1B8' //Blue
																		<?php
																	}
																}
															}
														}
														
												
													?>
													
													
													
											}
											<?php
											$previousEvent ++;			
										}
									}
									
									$result->close();	
									
									
						}
						else
						{
							echo 'Un problème est survenu.';
						} ?>
						
						
						
						// Check for this user's demands
						<?php $sql = "Select * from appointment where owner_id='$user_id'";

						if($result = $mysqli->query($sql))
						{
									$count = $result->num_rows;
									
									if($count == 0)
									{
										
									}
									else
									{
										while($row = mysqli_fetch_array($result))
										{
											$title = $row["title"];
											$start = $row["start"];
											$end = $row["end"];
											
											$confirmed = $row["confirmed"];
											
											$appID = $row["id"];
											$OID = $user_id;
											$UID = $row["user_id"];
											$BID = $row["biz_id"];
											
											if($previousEvent == 1)
															{
																echo ',';
																$previousEvent --;
															}
											
											?>
											{
													title: '<?php echo $title?>',
													start: '<?php echo $start?>',
													end:   '<?php echo $end?>',
													userID: '<?php echo $UID?>',
													ownerID: '<?php echo $OID?>',
													appID : '<?php echo $appID?>',
													
													<?php
														
														$sqlUser = "Select * from users where user_id='$UID'";
														if($resUser = $mysqli->query($sqlUser))
														{
															$userINF = mysqli_fetch_array($resUser);
															$user_name_I = $userINF['username'];
															$ProfileAvatar = $userINF['avatar'];
															if (empty($ProfileAvatar))
															{ 
																$user_img_I =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
															}
															elseif (!empty($ProfileAvatar))
															{
																$user_img_I =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
															}
															?>
															user_name: '<?php echo $user_name_I ?>',
															user_img: '<?php echo $user_img_I ?>',
															<?php
														}
												
													?>
													
													<?php
														
														$sqlB = "Select * from business where unique_biz='$BID'";
														if($resB = $mysqli->query($sqlB))
														{
															$BINF = mysqli_fetch_array($resB);
															$B_name_I = $BINF['business_name'];
															$B_img_I = $BINF['featured_image'];
															$B_adr_I = $BINF['address_1'];
															$B_adr2_I = $BINF['address_2'];
															$B_city_I = $BINF['city'];
															?>
															business_name: '<?php echo $B_name_I ?>',
															business_img: '<?php echo $B_img_I ?>',
															business_adr: '<?php echo $B_adr_I ." ".$B_adr2_I." ".", ".$B_city_I ?>',
															<?php
														}
												
													?>
													
													
													<?php 
													
															if($confirmed == 0)
															{
																?>
																	color: '#E67E22' //orange
																<?php
															}
															else
															{
																?>
																	color: '#006442' //Green
																<?php
															}
													
													?>
													
													
													
											}
											<?php
											$previousEvent ++;			
										}	
									}
									$result->close();										
									
						}
						else
						{
							echo 'Un problème est survenu.';
						} ?>
						
			],
			eventClick: function(calEvent, jsEvent, view) 
			{
				
				
							if(calEvent.color == '#E0E0E0' || calEvent.color == '#000')
							{
								
							}
							else if(calEvent.color == '#6AC1B8' || calEvent.color == '#B71C0C')
							{
								$( "#modalButtEdit" ).trigger( "click" );
								
								$("#owner-name").html("Rendez-vous avec " + calEvent.owner_name);
								$("#owner-img").html('<img src="thumbs.php?src=' + calEvent.owner_img +'&amp;h=90&amp;w=90&amp;q=100" alt="' + calEvent.owner_name + '" class="img-circle">');
								$("#business-img").html('<img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/' + calEvent.business_img + '&amp;h=150&amp;w=250&amp;q=100" alt="' + calEvent.business_name + '" >');
								$("#biz-name").html(calEvent.business_name);
													
								var startTime = moment(calEvent.start).format();
								var endTime = moment(calEvent.end).format();
								
								var d = new Date(startTime);
								var n = d.getDay();
								var day = getDayName(n);
								
								var date = startTime.substring(0,10);
								var dateAr = date.split('-');
								var newDate = dateAr[1] + '-' + dateAr[2] + '-' + dateAr[0].slice(-2);
								
								var startTime = startTime.substring(11,16);
								var endTime = endTime.substring(11,16);
								
								$("#display-title").html(calEvent.title);
								
								$("#day").html(day + " le " + newDate );
								$("#start-time").html(" de " + startTime + " à " + endTime);
								$("#eventID").val(calEvent.appID);
								$("#eventUserID").val(calEvent.userID);
								$("#eventOwnerID").val(calEvent.ownerID);
								
								$("#adr").html("Adresse : " +calEvent.business_adr);
							}
							else if(calEvent.color == '#E67E22')
							{
								
								$("#modalButtConfirm" ).trigger( "click" );
								
								$("#user-name").html(calEvent.user_name + " a demandé un rendez-vous");
								$("#user-img").html('<img src="thumbs.php?src=' + calEvent.user_img +'&amp;h=90&amp;w=90&amp;q=100" alt="' + calEvent.user_name + '" class="img-circle">');
								$("#business-img_owner").html('<img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/' + calEvent.business_img + '&amp;h=150&amp;w=250&amp;q=100" alt="' + calEvent.business_name + '" >');
								$("#biz-nameU").html(calEvent.business_name);
								
								var startTime = moment(calEvent.start).format();
								var endTime = moment(calEvent.end).format();
								
								var d = new Date(startTime);
								var n = d.getDay();
								var day = getDayName(n);
								
								var date = startTime.substring(0,10);
								var dateAr = date.split('-');
								var newDate = dateAr[1] + '-' + dateAr[2] + '-' + dateAr[0].slice(-2);
								
								var startTime = startTime.substring(11,16);
								var endTime = endTime.substring(11,16);
								
								$("#display-titleU").html(calEvent.title);
								
								$("#dayU").html(day + " le " + newDate );
								$("#start-timeU").html(" de " + startTime + " à " + endTime);
								$("#eventIDU").val(calEvent.appID);
								$("#eventUserIDU").val(calEvent.userID);
								$("#eventOwnerIDU").val(calEvent.ownerID);
								
								$("#adrU").html("Adresse : " +calEvent.business_adr);

							}
							else if(calEvent.color == '#006442')
							{
								$("#modalButtView" ).trigger( "click" );
								
								$("#user-nameV").html(calEvent.user_name + " a demandé un rendez-vous");
								$("#user-imgV").html('<img src="thumbs.php?src=' + calEvent.user_img +'&amp;h=90&amp;w=90&amp;q=100" alt="' + calEvent.user_name + '" class="img-circle">');
								$("#business-img_ownerV").html('<img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/' + calEvent.business_img + '&amp;h=150&amp;w=250&amp;q=100" alt="' + calEvent.business_name + '" >');
								$("#biz-nameUV").html(calEvent.business_name);
								
								var startTime = moment(calEvent.start).format();
								var endTime = moment(calEvent.end).format();
								
								var d = new Date(startTime);
								var n = d.getDay();
								var day = getDayName(n);
								
								var date = startTime.substring(0,10);
								var dateAr = date.split('-');
								var newDate = dateAr[1] + '-' + dateAr[2] + '-' + dateAr[0].slice(-2);
								
								var startTime = startTime.substring(11,16);
								var endTime = endTime.substring(11,16);
								
								$("#display-titleUV").html(calEvent.title);
								
								$("#dayUV").html(day + " le " + newDate );
								$("#start-timeUV").html(" de " + startTime + " à " + endTime);
								$("#eventIDUV").val(calEvent.appID);
								$("#eventUserIDUV").val(calEvent.userID);
								$("#eventOwnerIDUV").val(calEvent.ownerID);
								
								$("#adrUV").html("Adresse : " + calEvent.business_adr);
								
							}
							else if(calEvent.color == '#763568')//Iris
							{
								
								$("#modalButtAgenda" ).trigger( "click" );
								
								var startTime = moment(calEvent.start).format();
								var endTime = moment(calEvent.end).format();
								
								var d = new Date(startTime);
								var n = d.getDay();
								var day = getDayName(n);
								
								var date = startTime.substring(0,10);
								var dateAr = date.split('-');
								var newDate = dateAr[1] + '-' + dateAr[2] + '-' + dateAr[0].slice(-2);
								
								var startTime = startTime.substring(11,16);
								var endTime = endTime.substring(11,16);
								
								$("#display-titleA").html(calEvent.title);
								$("#eventIDAgenda").val(calEvent.appID);
							}
				
			}
		});
		
		
		// Cancel a meeting 
		$('#cancel').click(function()
					{
						
						var id = $("#eventID").val();
						var userID = $("#eventUserID").val();
						var ownerID = $("#eventOwnerID").val();
						
						$.ajax({
											type: "POST",
											url: "delete_appointment.php",
											data:{ 
												   id 	  : id,
												   userID : userID,
												   ownerId : ownerID
												 },
											
								});
								
								location.reload();
					});
		
		// Cancel a meeting 
		$('#cancel2').click(function()
					{
						
						var id = $("#eventIDUV").val();
						var userID = $("#eventUserIDUV").val();
						var ownerID = $("#eventOwnerIDUV").val();
						
						$.ajax({
											type: "POST",
											url: "delete_appointment.php",
											data:{ 
												   id 	  : id,
												   userID : userID,
												   ownerId : ownerID
												 },
											
								});
								
								location.reload();
					});
		
		// Reject meeting
		$('#reject').click(function()
					{
						
						var id = $("#eventIDU").val();
						var userID = $("#eventUserIDU").val();
						var ownerID = $("#eventOwnerIDU").val();
						
						$.ajax({
											type: "POST",
											url: "delete_appointment.php",
											data:{ 
												   id 	  : id,
												   userID : userID,
												   ownerId : ownerID
												 },
											
								});
								
								location.reload();
					});
		
		
		// confirm meeting
		$('#confirm').click(function()
					{
						
						var id = $("#eventIDU").val();
						var userID = $("#eventUserIDU").val();
						var ownerID = $("#eventOwnerIDU").val();
						
						$.ajax({
											type: "POST",
											url: "confirm_appointment.php",
											data:{ 
												   id 	  : id,
												   userID : userID,
												   ownerId : ownerID
												 },
											
								});
								
								location.reload();
					});
		
	});
	
	
	

</script>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		width: 98%;
		margin: 0 auto;
	}
	
	.fc-other-month .fc-day-number { display:none;}

</style>



</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=257885121076317";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrap">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://<?php echo $SiteLink; ?>"><img src="images/logo.png" class="logo" alt="<?php echo $SiteTitle;?>"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      	
			
       <ul class="nav navbar-nav navbar-right">
      
              
			<?php
				
				if(!isset($_SESSION['email']))
				{ ?>
					<li class="dropdown"> 
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="images/round.png" style="width: 30px; height: 30px;">
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="login">Connexion</a></li>
							<li><a href="register">Rejoindre la Communauté</a></li>
						</ul>
					</li>
				  <?php
				}
				else
				{
							
							?>
					<!--<li><a href="produit" data-toggle="tooltip" title="Ecrire un avis" ><img src="images/pen.png" style="width: 35px; height: 30px; "></a></li>
					<li><a href="message" data-toggle="tooltip" title="Messages" ><img src="images/note.png" style="width: 35px; height: 35px; "></a></li>
					<li><a href="forum" data-toggle="tooltip" title="Discussions" ><img src="images/discussion.png" style="width: 35px; height: 30px; "></a></li>
					<li><a href="#" data-toggle="tooltip" title="Bourse" ><img src="images/money.png" style="width: 30px; height: 30px; "></a></li>
					<li><a href="profile"><img src="thumbs.php?src=<?php //echo $ProfilePic;?>&amp;h=30&amp;w=30&amp;q=100" class="img-circle"></a></li>-->
					
					<li><a href="index" data-toggle="tooltip" title="Home"><img src = "images/home-button.png" class= "icon-header"><div class="header_options">Accueil</div></a></li>
      
					<li>
						<a href="my_business" data-toggle="tooltip" title="Ajouter votre Activité Professionnelle"><img src = "images/suitcase.png" class= "icon-header">
							<div class="header_options">Services</div>
							<kbd id="display_unread_services" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					
					<li>
						<a href="my_reviews" data-toggle="tooltip" title="Ecrire un avis" >
							<img src = "images/pencil.png" class= "icon-header">
							<div class="header_options">Avis Produit</div>
							<kbd id="display_unread_comments" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					
					<li class="dropdown">
						<a href="message" data-toggle="tooltip" title="Messages"><img src = "images/chat.png" class="icon-header">
							<div class="header_options">Messages</div>
							<kbd id="display_unread_messages" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					<li>
						<a href="forum" data-toggle="tooltip" title="Discussions" ><img src = "images/discussion.png" class= "icon-header">
							<div class="header_options">Forum</div>
							<kbd id="display_unread_responses" class="notification-index dropdown-toggle" ></kbd>
						</a></li>
					<li><a href="#" data-toggle="tooltip" title="Bourse" ><img src = "images/money-bag.png" class= "icon-header"><div class="header_options">Récompense</div></a></li>
					<li><a href="profile"><img src="thumbs.php?src=<?php echo $LoggedUserAvatarPic;?>&amp;h=30&amp;w=30&amp;q=100" class="img-circle"></a></li>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="images/tool.png" style="width: 20px; height: 20px; margin-top:8px">
						<span class="caret"></span>
						</a>
						
						<ul class="dropdown-menu" role="menu">
							<li><a href="settings">Paramètres</a></li>
							<li><a href="my_business">Mes Services</a></li>
							<li><a href="bookmarks">Bookmarks</a></li>
							<li><a href="search_user">Rechercher des amis</a></li>
							<li><a href="manage_appointment.php">Gérer mes rendez-vous</a></li>
							<li><a href="signaler">Signaler un problème</a></li>
							<li><a href="logout">Déconnexion</a></li>
						</ul>
					</li>

				 <?php
				}
			?>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!--container--> 
  </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid search-bar">

<div class="container">

<div class="row">

<form role="search" method="get" action="search.php">
         <div class="form-group">
         <input class="form-control input-lg" id="type" name="type" placeholder="Type" type="hidden" value ='business'>
         <div class="col-md-4 col-desktop-only">
			<h2>Rechercher des services</h2>
            </div><!--div col-md-5-->
         
         <div class="col-md-4">
            <input type="text" class="form-control" id="term" name="term" placeholder="Recherche"> 			
            </div><!--div col-md-5-->
            <div class="col-md-4">
            
            <select class="form-control" id="city" name="city">
                      <option value="all">Toutes les villes</option>
                      <?php
if($SearchCity = $mysqli->query("SELECT city_id, city FROM city")){

    while($SearchRow = mysqli_fetch_array($SearchCity)){
				
?>
                      <option value="<?php echo $SearchRow['city'];?>"><?php echo $SearchRow['city'];?></option>
                      <?php

}

	$SearchCity->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

?>
                    </select>
             </div><!--div col-md-5-->
         </div>  
         <div class="col-btn">               
             <button type="submit" class="btn btn-danger btn-width"><i class="glyphicon glyphicon-search"></i> <span class="col-mobile-only">Rechercher</span></button>
       </div><!--col-btn-->  
       </form>
</div><!--row-->       
</div><!--container-->
</div><!-- /.container-fluid -->