<?php
							//----------- Get list of recipients -----------------
							if($recipients = $mysqli->query("Select Distinct receiverID from messages where user_id = '$id'"))
							{
								
								$array_recipients = array();
								
								while($row = $recipients->fetch_assoc())
								{
										array_push($array_recipients,$row["receiverID"]);
										
								}
								
								$recipients->free();
							}
							else
							{
								echo '<div class="no-display-error">';
									echo "Les messages n'ont pas pu être afficher.";
								echo '</div>';
								return;
							}
							
							//-------------------------------------------------------------------
							
							//----------- Get list of user id who sent a message -----------------
							if($received = $mysqli->query("Select Distinct user_id from messages where receiverID = '$id'"))
							{
								
								$array_received = array();
								
								while($row1 = $received->fetch_assoc())
								{
										array_push($array_received,$row1["user_id"]);
										
								}
								
								$received->free();
							}
							else
							{
								echo '<div class="no-display-error">';
									echo "Les messages n'ont pas pu être afficher.";
								echo '</div>';
								return;
							}
							
							
							//----------------------------------------------------------------------------------
							
							// ----------- Erase duplicates, Get last message from al conversations -----------
							$list = array();
							
							
							
							for($i = 0 ; $i < sizeof($array_recipients) ; $i++)
							{
								
								for($y = 0 ; $y < sizeof($array_received) ; $y++)
								{
									
									if($array_recipients[$i] == $array_received[$y] )// if match
									{
										
										if($getM = $mysqli->query("Select idMessage, message, max(dateMessage) As 'RecentDate' from messages where user_id='$id' And receiverId= '$array_recipients[$i]'"))
										{
											while($row2 = $getM->fetch_assoc())
											{
												$idMessage = $row2["idMessage"];
												$dateMessage = $row2["RecentDate"];
												$message = $row2["message"];
											}
											$getM-> free();
											
											
											if($getM2 = $mysqli->query("Select idMessage, message, max(dateMessage) As 'RecentDate' from messages where user_id='$array_recipients[$i]' And receiverId= '$id'"))
											{
												
												while($row2 = $getM2->fetch_assoc())
												{
													$idMessage2 = $row2["idMessage"];
													$dateMessage2 = $row2["RecentDate"];
													$message2 = $row2["message"];
												}
												$getM2-> free();
												
												$dateMessage2 = strtotime($dateMessage2);
												$dateMessage  = strtotime($dateMessage);
												
												if($dateMessage2 < $dateMessage)
												{
													array_push($list,array($idMessage,$message,$dateMessage,$array_recipients[$i],'1')); //1 user sent message , 0 : user received message
												}
												elseif($dateMessage2 > $dateMessage)
												{
													array_push($list,array($idMessage2,$message2,$dateMessage2,$array_recipients[$i],'0')); //1 user sent message , 0 : user received message
												}
												elseif($dateMessage2 == $dateMessage)
												{
													if($idMessage < $idMessage2)
													{
														array_push($list,array($idMessage2,$message2,$dateMessage2,$array_recipients[$i],'0'));
													}
													elseif($idMessage > $idMessage2)
													{
														array_push($list,array($idMessage,$message,$dateMessage,$array_recipients[$i],'1'));
													}
												}
												
											}
											else
											{
												echo '<div class="no-display-error">';
												echo "Les messages n'ont pas pu être afficher.";
												echo '</div>';
												return;
											}
										}
										else
										{
											echo '<div class="no-display-error">';
												echo "Les messages n'ont pas pu être afficher.";
											echo '</div>';
											return;
										}
										
										unset($array_recipients[$i]);
										unset($array_received[$y]);
										$array_recipients = array_values($array_recipients);
										$array_received= array_values($array_received);
									}
									
								}
							}
							
							
							// ---------- Put remaining conversations in queue --------
							if(! empty($array_recipients))
							{
								for($i = 0 ; $i < sizeof($array_recipients) ; $i++)
								{
									if($getMess = $mysqli->query("Select idMessage, message, max(dateMessage) As 'RecentDate' from messages where user_id='$id' And receiverId= '$array_recipients[$i]'"))
									{
										while($row2 = $getMess->fetch_assoc())
											{
												$idMessage = $row2["idMessage"];
												$dateMessage = $row2["RecentDate"];
												$message = $row2["message"];
												
												array_push($list,array($idMessage,$message,$dateMessage,$array_recipients[$i],'1'));
											}
											$getMess-> free();
									}
									else
									{
										echo '<div class="no-display-error">';
												echo "Les messages n'ont pas pu être afficher.";
											echo '</div>';
											return;
									}
									
								}
								
							}
							
							if(! empty($array_received))
							{
								for($i = 0 ; $i < sizeof($array_received) ; $i++)
								{
									if($getMess2 = $mysqli->query("Select idMessage, message, max(dateMessage) As 'RecentDate' from messages where user_id='$array_received[$i]' And receiverId= '$id'"))
									{
										while($row2 = $getMess2->fetch_assoc())
										{
											$idMessage2 = $row2["idMessage"];
											$dateMessage2 = $row2["RecentDate"];
											$message2 = $row2["message"];
											
											array_push($list,array($idMessage2,$message2,$dateMessage2,$array_received[$i],'0'));
										}
										$getMess2-> free();
									}
									else
									{
										echo '<div class="no-display-error">';
												echo "Les messages n'ont pas pu être afficher.";
											echo '</div>';
											return;
									}
								}
								
							}
							
							
							
							// ----------------- Sort list by recent date and display------------------------------
							for($i = 0 ; $i < sizeof($list) ; $i++)
							{
								$idU = $list[$i][3];
								if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE user_id='$idU'"))
								{
									$ProfileInfo = mysqli_fetch_array($ProfileSql);
									$name = $ProfileInfo['username'];
									$ProfileAvatar = $ProfileInfo['avatar'];
									
									if (empty($ProfileAvatar))
									{ 
										$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
									}
									elseif (!empty($ProfileAvatar))
									{
										$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
									}
									$ProfileSql->close();
									
									echo '<div class="conversation">';
									
										echo '<div class="conv">';
											echo '<div class="pPic">'; ?>
												<img src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=60&amp;w=60&amp;q=100" class="img-circle">
											<?php
											echo '</div>';
											
											echo '<div class="recipent-message">';
												echo '<div class="recipient-name">';
													echo $name;
												echo '</div>';
												
												echo '<div class="message-overview">';
													echo $list[$i][1];
												echo '</div>';
											echo '</div>';
											
											echo '<div class="date-message">';
											echo '17 march';
											echo '</div>';
											
										echo '</div>';
										
									echo '</div>';
									
								}
								else
								{
									echo '<div class="no-display-error">';
												echo "Les messages n'ont pas pu être afficher.";
											echo '</div>';
											return;
								}
							}
							
							
					?>