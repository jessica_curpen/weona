<?php 

include("db.php");

			?>								<select class="form-control input-lg" id="category" name="category" placeholder="Catégories">
											
												<option value="all" selected>Catégories</option>
												  <?php
															if($SelectCat = $mysqli->query("SELECT cat_id, parent_id, category FROM categories"))
															{	
																$subcat = array();
																$maincat = array();
																
																while($CatRow = mysqli_fetch_array($SelectCat))
																{
																	if($CatRow['parent_id'] == 0)
																	{
																		array_push($maincat,$CatRow['cat_id']);
																		array_push($maincat,$CatRow['category']);
																	}
																	else
																	{
																		array_push($subcat,$CatRow['parent_id'],$CatRow['cat_id'],$CatRow['category']);
																	}
																
																}

																for($i=0 ; $i<sizeof($maincat) ; $i++)
																{
																				$currentID = $maincat[$i++];
																	?>
																				<option value="<?php echo $currentID;?>"><div class="catStyle"><?php echo utf8_encode($maincat[$i]);?></div></option>
																				
																	<?php
																				for($y=0 ; $y<sizeof($subcat) ; $y++)
																				{
																					if($currentID == $subcat[$y])
																					{
																						$y++;
																						?>
																									<option value="<?php echo $subcat[$y];?>"><?php $y++; echo utf8_encode($subcat[$y]);?></option>
																						<?php
																					}
																				}
																				
																}
													

																

																$SelectCat->close();
									
															}
															else
															{
								
																 printf("Rien à afficher pour le moment.");
															}

													?>
										</select>
	