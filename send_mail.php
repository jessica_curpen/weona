<?php
include ("db.php");

if($_POST)
{	
	if(!isset($_POST['inputYourname']) || strlen($_POST['inputYourname'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez donner votre nom</div>');
	}
	if(!isset($_POST['inputEmail']) || strlen($_POST['inputEmail'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez donner une adresse mail</div');
	}
	
	$email_address = $_POST['inputEmail'];
	
	if (filter_var($email_address, FILTER_VALIDATE_EMAIL)) 
	{
  	// The email address is valid
	} 
	else 
	{
  		die('<div class="alert alert-danger" role="alert">Veuillez donner une adresse mail valide</div>');
	}
	
	if(!isset($_POST['inputSubject']) || strlen($_POST['inputSubject'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">L\'objet ne peut pas être vide</div>');
	}
	if(!isset($_POST['inputMessage']) || strlen($_POST['inputMessage'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Votre messsage ne peut pas être vide</div>');
	}
	
	if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'"))
	{

		$Settings = mysqli_fetch_array($SiteSettings);
	
		$SiteSettings->close();
	
	}
	else
	{
    
		printf("<div class='alert alert-danger alert-pull'>Il semble y avoir eu un problème</div>");
	}

	$SiteName		 	 = $Settings['site_title'];
	$SiteContact	 	 = $Settings['site_email'];
	$FromName		 	 = $mysqli->escape_string($_POST['inputYourname']);
	$FromEmail		 	 = $mysqli->escape_string($_POST['inputEmail']);
	$FrominputSubject	 = $mysqli->escape_string($_POST['inputSubject']);
	$FromMessage	 	 = $mysqli->escape_string($_POST['inputMessage']);

	require_once('class.phpmailer.php');

	$mail = new PHPMailer() ;

	$mail->AddReplyTo($FromEmail, $FromName);

	$mail->SetFrom($FromEmail, $FromName);

	$mail->AddReplyTo($FromEmail, $FromName);

	$mail->AddAddress($SiteContact, $SiteName);

	$mail->Subject = $FrominputSubject;

	$mail->MsgHTML($FromMessage);

	if(!$mail->Send()) 
	{?>

		<div class="alert alert-danger" role="alert">Erreur mail</div>

	<?php 
	} 
	else 
	{?>

		<div class="alert alert-success" role="alert">Votre message a été envoyé. Nous vous re contacterons bientôt</div>

	<?php 
	}

}

?>