<?php
	session_start() ;
	include('db.php');
	include('function.php');
	require_once('class.phpmailer.php');

	$LoggedUserEmail = $_SESSION['email'];
	$GetUser = $mysqli->query("SELECT * FROM users WHERE email='$LoggedUserEmail'");
	$UserInfo = mysqli_fetch_array($GetUser);
	$UserId = $UserInfo['user_id'];
	$GetUser->close();
	
	$id_discussion = mysqli_real_escape_string($mysqli,$_POST["id"]);
	$reply = mysqli_real_escape_string($mysqli,$_POST["reply"]);
	$idU = mysqli_real_escape_string($mysqli,$_POST["idU"]);

	$date_asked = date("Y/m/d H:i:s");
	
	$GetUser = $mysqli->query("SELECT user_id FROM discussions WHERE id_discussion='$id_discussion'");
	$UserInfo = mysqli_fetch_array($GetUser);
	$check = $UserInfo['user_id'];
	$GetUser->close();
	
	
	
	// If owner of the discussion replies
	if($check == $UserId)
	{
		$sql = "Insert into participation (idDiscussion, user_id, reply, date_rep,seen,open) values ('$id_discussion', '$idU', '$reply', '$date_asked',1,1)";
		$res = $mysqli->query($sql);
		$idInsert = $mysqli->insert_id;
		
		
		//notify other users
		$sql = $mysqli->query("Select * from forum_notification where id_discussion = '$id_discussion'");
		$CountRows = mysqli_num_rows($sql);	
		$part = $idInsert.":";
					
		if($CountRows > 0)
		{
			$sql = $mysqli->query("Update forum_notification set seen = '0' , open = '0' , id_participation = '$part' where id_discussion = '$id_discussion'");

		}
	}
	else
	{
		$sql = "Insert into participation (idDiscussion, user_id, reply, date_rep) values ('$id_discussion', '$idU', '$reply', '$date_asked')";
		$res = $mysqli->query($sql);
		$idInsert = $mysqli->insert_id;
		

		
		//Get action Info
		if($action = $mysqli->query("SELECT * FROM actions WHERE action='reply_discussion'"))
		{

				$actionInfo = mysqli_fetch_array($action);
				
				$actionPoints = $actionInfo['points'];
				$actionCoins = $actionInfo['coins'];


				$action->close();
		}
		else
		{
				printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");;
		}	
		
		
		
		
				//Send mail : get site info
				//----------------------------------------------------
				$squ = $mysqli->query("SELECT * FROM settings WHERE id='1'");
				$settings = mysqli_fetch_array($squ);
				
				$Active 			 = $settings['active'];
				$SiteName		 	 = $settings['site_title'];
				$SiteContact	 	 = $settings['site_email'];
				$from				 = $settings['site_title'];
				
		
		//Send mail : Send mail to replier
		//----------------------------------------------------
		$sql 	 = $mysqli->query("Select user_id, sujet from discussions where id_discussion = '$id_discussion'");
		$res 	 = mysqli_fetch_array($sql);
		$user_id = $res["user_id"];
		$sujet   = $res["sujet"];
		
		
		$sqlR = $mysqli->query("Select username, firstname, lastname, email, idRanking, money, points from users where user_id = '$idU'");
		$resR = mysqli_fetch_array($sqlR);
		
		$firstnameR 	= $resR["firstname"];
		$nameR 			= $resR["username"];
		$lastnameR 		= $resR["lastname"];
		$emailR			= $resR["email"];
		$money			= $resR["money"];
		$points			= $resR["points"];
		$idRanking		= $resR["idRanking"];
		
		$money = $actionCoins + $money;
		$points = $actionPoints + $points;
		
		$ToName		 	 = $firstnameR." ".$lastnameR;
					$FromEmail		 	 = $emailR ;
					$FrominputSubject	 = 'Vous avez répondu dans une discussion';
					$FromMessage	 	 = 'Cher '.$ToName.',
															<br/>
															Vous avez répondu dans la discussion <span style="font-weight: bold">'.$sujet.'</span>.
															Vous avez gagné '.$actionCoins.' pièces d\'or et '.$actionPoints.'points.
															<br/><br/>
															
															Sincèrement,
															<br/>
															'.$from;
					$FromMessage = utf8_decode($FromMessage);
					$FrominputSubject = utf8_decode($FrominputSubject);
					
					$mail = new PHPMailer() ;

					$mail->AddReplyTo($FromEmail, $from);

					$mail->SetFrom($FromEmail, $from);

					$mail->AddReplyTo($FromEmail, $from);

					$mail->AddAddress($SiteContact, $SiteName);

					$mail->Subject = $FrominputSubject;

					$mail->MsgHTML($FromMessage);

					$mail->Send();
					
					getRank($mysqli, $idRanking, $money, $idU , $firstnameR,  $lastnameR , $emailR, $SiteContact, $SiteName, $from);
				
		//Send mail : Send mail to owner
		//----------------------------------------------------
	
				
	$sql = $mysqli->query("Select firstname, lastname, email from users where user_id = '$user_id'");
	$res = mysqli_fetch_array($sql);
	
	$firstname 	= $res["firstname"];
	$lastname 	= $res["lastname"];
	$email 		= $res["email"];
	
	$ToName		 	 = $firstname." ".$lastname;
					$FromEmail		 	 = $email ;
					$FrominputSubject	 = 'Vous avez reçu une réponse dans votre discussion';
					$FromMessage	 	 = 'Cher '.$ToName.',
															<br/>
															Vous avez reçu une nouvelle réponse dans votre discussion <span style="font-weight: bold">'.$sujet.'</span> de la part de '.$nameR.'.
															<br/><br/>
															
															Sincèrement,
															<br/>
															'.$from;
					$FromMessage = utf8_decode($FromMessage);
					$FrominputSubject = utf8_decode($FrominputSubject);
					

					$mail = new PHPMailer() ;

					$mail->AddReplyTo($SiteContact, $from);

					$mail->SetFrom($SiteContact, $from);

					$mail->AddReplyTo($FromEmail, $from);

					$mail->AddAddress($FromEmail, $SiteName);

					$mail->Subject = $FrominputSubject;

					$mail->MsgHTML($FromMessage);

					$mail->Send();
					
					
					
					
					
					
					//Notify other users
					//-----------------------------------------------------------------
					
					$sql = $mysqli->query("Select * from forum_notification where id_discussion = '$id_discussion' And id_user = '$UserId'");
					
					$userIs = mysqli_num_rows($sql);	
					$res = mysqli_fetch_array($sql);
					
					//add user to notify list 
					if($userIs == 0)
					{
						$add = $mysqli->query("Insert into forum_notification(id_discussion, id_user, seen, open) values ('$id_discussion', '$UserId', '1', '1')");	
					}
					
					
					//notify other users
					$sql = $mysqli->query("Select * from forum_notification where id_discussion = '$id_discussion' And id_user <> $UserId");
					$CountRows = mysqli_num_rows($sql);	
					$part = $idInsert.":";
					
					if($CountRows > 0)
					{
						$sql = $mysqli->query("Update forum_notification set seen = '0' , open = '0' , id_participation = '$part' where id_discussion = '$id_discussion'");
					}
					
	}
?>