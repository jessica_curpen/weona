<?php 
session_start();

include("db.php");

//Get Site Settings

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteLink = $Settings['site_link'];
	
	$SiteTitle = $Settings['site_title'];
	
	$FaceBook = $Settings['fb_page'];
	
	$Twitter = $Settings['twitter_link'];
	
	$Pinterest = $Settings['pinterest_link'];
	
	$Gplus = $Settings['google_pluse_link'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

//Get User Info

if(isset($_SESSION['email'])){
	
$LoggedUser = $_SESSION['username'];
$LoggedUserEmail = $_SESSION['email'];

if($GetUser = $mysqli->query("SELECT * FROM users WHERE email='$LoggedUserEmail'")){

    $UserInfo = mysqli_fetch_array($GetUser);
	
	$LoggedUsername = strtolower($UserInfo['username']);
	
	$LoggedUser = $UserInfo['username'];
	
	$LoggedUserLink   = preg_replace("![^a-z0-9]+!i", "-", $LoggedUsername);
	
	$LoggedUserLink	  = strtolower($LoggedUserLink);

	$UserId = $UserInfo['user_id'];
	
	$UserEmail = $UserInfo['email'];
	
	$LoggedUserAvatar = $UserInfo['avatar'];
		
	$GetUser->close();
	
}else{
     
	 printf("Il semble y avoir eu un problème");
	 
}

}else{

$UserId = 0;	
	
}


if (empty($LoggedUserAvatar))
	{ 
		$LoggedUserAvatarPic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
	}
	elseif (!empty($LoggedUserAvatar))
	{
		$LoggedUserAvatarPic =  'http://'.$SiteLink.'/avatars/'.$LoggedUserAvatar;
	}

//Ads

if($AdsSql = $mysqli->query("SELECT * FROM advertisements WHERE id='1'")){

    $AdsRow = mysqli_fetch_array($AdsSql);
	
	$Ad1 = $AdsRow['ad1'];
	$Ad2 = $AdsRow['ad2'];
	$Ad3 = $AdsRow['ad3'];

    $AdsSql->close();

}else{
	
     printf("Il semble y avoir eu un problème");
}

$id = $mysqli->escape_string($_GET['id']);


if($Biz = $mysqli->query("SELECT * FROM business LEFT JOIN categories ON categories.cat_id=business.cid WHERE business.active=1 AND business.biz_id='$id'")){
	
	$BizRow = mysqli_fetch_array($Biz);
	
	$BizName = stripslashes($BizRow['business_name']);
	
	$PostLink = preg_replace("![^a-z0-9]+!i", "-", $BizName);
	$PostLink = urlencode(strtolower($PostLink));
	
	$longDescription = stripslashes($BizRow['description']);
	$strDescription = strlen ($longDescription);
	if ($strDescription > 160) {
	$Description = substr($longDescription,0,157).'...';
	}else{
	$Description = $longDescription;}
	
	$CName = $BizRow['category'];
	$CLink = preg_replace("![^a-z0-9]+!i", "-", $CName);
	$CLink = urlencode($CLink);
	$CLink = strtolower($CLink);
	
	$SId = $BizRow['sid'];
	
	$Biz->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

$mysqli->query("UPDATE settings SET site_views=site_views+1 WHERE id=1");

$mysqli->query("UPDATE business SET hits=hits+1 WHERE biz_id='$id'");

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $BizName;?> | <?php echo $SiteTitle;?></title>
<meta name="description" content="<?php echo $Description;?>" />
<meta name="keywords" content="<?php echo $BizRow['tags'];?>" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fb_app_id']; ?>" /> 
<meta property="og:url"             content="http://<?php echo $SiteLink; ?>" /> 
<meta property="og:title"           content="<?php echo $BizName;?>" />
<meta property="og:description" 	content="<?php echo $Description;?>" /> 
<meta property="og:image"           content="http://<?php echo $SiteLink;?>/uploads/<?php echo $BizRow['featured_image'];?>" /> 
<!--End Facebook Meta Tags-->

<link href="templates/<?php echo $Settings['template'];?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/style.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/animate.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery.min.js"></script>	
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.raty.js"></script>
<script src="js/wow.min.js"></script>

<script>
function popup(e){var t=700;var n=400;var r=(screen.width-t)/2;var i=(screen.height-n)/2;var s="width="+t+", height="+n;s+=", top="+i+", left="+r;s+=", directories=no";s+=", location=no";s+=", menubar=no";s+=", resizable=no";s+=", scrollbars=no";s+=", status=no";s+=", toolbar=no";newwin=window.open(e,"windowname5",s);if(window.focus){newwin.focus()}return false}
$(function() {
$.fn.raty.defaults.path = 'templates/default/images';
});
new WOW().init();
</script>

</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=257885121076317";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrap">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://<?php echo $SiteLink; ?>"><img src="images/logo.png" class="logo" alt="<?php echo $SiteTitle;?>"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      	
			
       <ul class="nav navbar-nav navbar-right">
      
              
			<?php
				
				if(!isset($_SESSION['username']))
				{ ?>
					<li class="dropdown"> 
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="images/round.png" style="width: 30px; height: 30px;">
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="login">Connexion</a></li>
							<li><a href="register">Rejoindre la Communauté</a></li>
						</ul>
					</li>
				  <?php
				}
				else
				{
							if (empty($ProfileAvatar))
							{ 
								$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
							}
							elseif (!empty($ProfileAvatar))
							{
								$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
							}
							
							?>
					<!--<li><a href="produit" data-toggle="tooltip" title="Ecrire un avis" ><img src="images/pen.png" style="width: 35px; height: 30px; "></a></li>
					<li><a href="message" data-toggle="tooltip" title="Messages" ><img src="images/note.png" style="width: 35px; height: 35px; "></a></li>
					<li><a href="forum" data-toggle="tooltip" title="Discussions" ><img src="images/discussion.png" style="width: 35px; height: 30px; "></a></li>
					<li><a href="#" data-toggle="tooltip" title="Bourse" ><img src="images/money.png" style="width: 30px; height: 30px; "></a></li>
					<li><a href="profile"><img src="thumbs.php?src=<?php //echo $ProfilePic;?>&amp;h=30&amp;w=30&amp;q=100" class="img-circle"></a></li>-->
					
					<li><a href="index" data-toggle="tooltip" title="Home"><img src = "images/home-button.png" class= "icon-header"><div class="header_options">Accueil</div></a></li>
      
					<li>
						<a href="my_business" data-toggle="tooltip" title="Ajouter votre Activité Professionnelle"><img src = "images/suitcase.png" class= "icon-header">
							<div class="header_options">Services</div>
							<kbd id="display_unread_services" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					
					<li>
						<a href="my_reviews" data-toggle="tooltip" title="Ecrire un avis" >
							<img src = "images/pencil.png" class= "icon-header">
							<div class="header_options">Avis Produit</div>
							<kbd id="display_unread_comments" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					
					<li class="dropdown">
						<a href="message" data-toggle="tooltip" title="Messages"><img src = "images/chat.png" class="icon-header">
							<div class="header_options">Messages</div>
							<kbd id="display_unread_messages" class="notification-index dropdown-toggle" ></kbd>
						</a>
					</li>
					<li>
						<a href="forum" data-toggle="tooltip" title="Discussions" ><img src = "images/discussion.png" class= "icon-header">
							<div class="header_options">Forum</div>
							<kbd id="display_unread_responses" class="notification-index dropdown-toggle" ></kbd>
						</a></li>
					<li><a href="#" data-toggle="tooltip" title="Bourse" ><img src = "images/money-bag.png" class= "icon-header"><div class="header_options">Récompense</div></a></li>
					<li><a href="profile"><img src="thumbs.php?src=<?php echo $LoggedUserAvatarPic;?>&amp;h=30&amp;w=30&amp;q=100" class="img-circle"></a></li>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="images/tool.png" style="width: 20px; height: 20px; margin-top:8px">
						<span class="caret"></span>
						</a>
						
						<ul class="dropdown-menu" role="menu">
							<li><a href="settings">Paramètres</a></li>
							<li><a href="my_business">Mes Services</a></li>
							<li><a href="bookmarks">Bookmarks</a></li>
							<li><a href="search_user">Rechercher des amis</a></li>
							<li><a href="manage_appointment.php">Gérer mes rendez-vous</a></li>
							<li><a href="signaler">Signaler un problème</a></li>
							<li><a href="logout">Déconnexion</a></li>
						</ul>
					</li>
					
				 <?php
				}
			?>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!--container--> 
  </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid search-bar">

<div class="container">

<div class="row">

<form role="search" method="get" action="search.php">
         <div class="form-group">
         <input class="form-control input-lg" id="type" name="type" placeholder="Type" type="hidden" value ='business'>
         <div class="col-md-4 col-desktop-only">
			<h2>Rechercher des services</h2>
            </div><!--div col-md-5-->
         
         <div class="col-md-4">
            <input type="text" class="form-control" id="term" name="term" placeholder="Recherche"> 			
            </div><!--div col-md-5-->
            <div class="col-md-4">
            
            <select class="form-control" id="city" name="city">
                      <option value="all">Toutes les villes</option>
                      <?php
if($SearchCity = $mysqli->query("SELECT city_id, city FROM city")){

    while($SearchRow = mysqli_fetch_array($SearchCity)){
				
?>
                      <option value="<?php echo $SearchRow['city'];?>"><?php echo $SearchRow['city'];?></option>
                      <?php

}

	$SearchCity->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

?>
                    </select>
             </div><!--div col-md-5-->
         </div>  
         <div class="col-btn">               
             <button type="submit" class="btn btn-danger btn-width"><i class="glyphicon glyphicon-search"></i> <span class="col-mobile-only">Rechercher</span></button>
       </div><!--col-btn-->  
       </form>
</div><!--row-->       
</div><!--container-->
</div><!-- /.container-fluid -->