<?php include("header.php");

if(!isset($_SESSION['email']))
{?>
<script type="text/javascript">
function leave() 
{
	window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php 
}
else
{?>


<div class="container container-main">
<script>
$(document).ready(function()
{
	$('.star-rates').raty
	({
		readOnly: true,
	  score: function() 
	  {
		return $(this).attr('data-score');
	  }
	});
});
</script>

			<div class="col-md-3">

					<div class="new-disc">
						<a href="submit"> <div class="button-style raised iris">
							  <div class="center" fit>Ajouter un service</div>
							  <paper-ripple fit></paper-ripple>
							</div>
						</a>
					</div>
					
					<!--Tableau de bord Forum-->
					<div class="categories-disc" style="margin-top: 10px">
						<div class="col-shadow">
							<div class="row">
							
									<div class="col-md-12"> <div class="tab-forum"> <a href="my_business"> Mes services </a> </div></div>
									
									<div class="col-md-12">
										<div class="tab-service-notif">
											
										</div>
									</div>	
									<div class="col-md-12"> <div class="tab-forum"> <a href="bookmarks"> Mes signets </a></div></div>
							</div>
						</div>	
					</div>

			</div>

			<div class="col-md-8">
			
					<script type="text/javascript" src="js/jquery.form.js"></script>

					<div class="col-shadow">
							<div class="biz-title-2">
								<h1>Mes signets</h1>
							</div>
					  
							<div class="col-desc" id="display-posts">
							
								<?php

										if($PostSql = $mysqli->query("SELECT * FROM bookmarks LEFT JOIN business ON bookmarks.bizid=business.biz_id WHERE bookmarks.user_id=$UserId ORDER BY bookmarks.bm_id DESC LIMIT 0, 12"))
										{
												$CountRows = mysqli_num_rows($PostSql);	

												while ($PostRow = mysqli_fetch_array($PostSql))
												{
													
													$longTitle = stripslashes($PostRow['business_name']);
													
													$strTitle = strlen ($longTitle);
													if ($strTitle > 25) 
													{
														$PostTitle = substr($longTitle,0,23).'...';
													}
													else
													{
														$PostTitle = $longTitle;
													}
	
													$PostLink = preg_replace("![^a-z0-9]+!i", "-", $longTitle);
													$PostLink = urlencode(strtolower($PostLink));
													
													$longDescription = stripslashes($PostRow['description']);
													$strDescription = strlen ($longDescription);
													
													if ($strDescription > 70) 
													{
														$Description = substr($longDescription,0,67).'...';
													}
													else
													{
														$Description = $longDescription;
													}
	
													$Tel = stripslashes($PostRow['phone']);
													$City = stripslashes($PostRow['city']);
													$Site = stripslashes($PostRow['website']);
	
													if(!empty($Tel))
													{
														$Telephone = $Tel;
													}
													else
													{
														$Telephone = "N/A";		
													}
													
													?>
													
													<div class="img-thumbs">

														<div class="right-caption span4">
														  <img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $PostRow['featured_image'];?>&amp;h=110&amp;w=140&amp;q=100" alt="<?php echo $longTitle;?>">
														  
														  <div class="col-caption" data-id="<?php echo $PostRow['biz_id'];?>">
																<a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><h2><?php echo $longTitle;?></h2></a>
																
																	
																	<div class="col-rate">    
																		<p class="font-small"><span class="star-rates"  data-score="<?php echo $PostRow['avg'];?>"></span> <?php echo $PostRow['reviews'];?> Avis| 	
																		<?php echo stripslashes($PostRow['hits']);?> Vus
																	</div>
																	
																

																<p><?php echo $Description;?></p>
																
																
																<div>
																	
																		<span class="fa fa-home"></span> <?php echo $City;?>
																		<span class="fa fa-phone"></span> <?php echo $Telephone;?>
																	
																	
																		<?php 
																		if(!empty($Site))
																		{?>
																			<span class="fa fa-link"></span> 
																			<a href="<?php echo $Site;?>" target="_blank">Site Web</a>
																		<?php 
																		}
																		else
																		{?>
																			<span class="fa fa-link"></span> N/A
																		<?php 
																		}?>
																
																</div>
																
																<p>
																	<a class="edit-links btnDelete" href="delete_biz-<?php echo $PostRow['biz_id'];?>"><span class="fa fa-remove"></span> Supprimer le signet</a>
																</p>
														  </div>
														  
														</div>
														
													</div>
													
													<?php
												}
										}
								?>
								
							</div>
					</div>
			</div>
</div>

<?php
}?>