<?php 
		include("header.php");
?>


<div class="container container-main">


		<div class="col-md-8">

				<script type="text/javascript" src="js/jquery.form.js"></script>
				
				<!--Script for sendind data to form without redirecting-->
				<script>
		
						$(document).ready(function()
						{
							
							$('#RegisterForm').on('submit', function(e)
							{
								e.preventDefault();
								$('#submitButton').attr('disabled', ''); // disable upload button
								//show uploading message
								$("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
								
								$(this).ajaxSubmit({
								target: '#output',
								success:  afterSuccess //call function after success
								});
							});
							
						});
		 
						function afterSuccess()
						{	
							 
							$('#submitButton').removeAttr('disabled'); //enable submit button
						   
						}
				</script>

				
				<div class="col-shadow">
				
						<div class="biz-title-2">
							<h1>Register</h1>
						</div>
						  
						<div class="col-desc">

								<div id="output"></div>
								
								<form id="RegisterForm" class="forms" action="submit_register.php" method="post">
								
										<div class="form-group">
												<label for="inputUsername">Nom Utilisateur</label>
												<div class="input-group">
												   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
													<input type="text" class="form-control" name="inputUsername" id="inputUsername" placeholder="Nom Utilisateur" required>
												</div>
										</div>
										
            
										<div class="form-group">
												<label for="inputPrenom">Prenom</label>
												<div class="input-group">
												   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
													<input type="text" class="form-control" name="inputPrenom" id="prenom" placeholder="Prenom" required>
												</div>
										</div>
					
					
										<div class="form-group">
										<label for="inputNom">Nom</label>
											<div class="input-group">
											   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
												<input type="text" class="form-control" name="inputNom" id="nom" placeholder="Nom" required>
											</div>
										</div>
										
			
										<div class="form-group">
										<label for="inputEmail">Email</label>
											<div class="input-group">
											   <span class="input-group-addon">@</span>
												<input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Adresse mail" required>
											</div>
										</div>
			
			
										<div class="form-group">
											<label for="inputActivity">Votre secteur d'activité professionel</label>
												<div class="input-group">
												   <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
													<select class="form-control" name="inputActivity" id="inputActivity" placeholder="Domaine d'activités">
															<option value="">Domaine d'activités</option>
																	<?php
																	
																		$PostSql = $mysqli->query("SELECT * FROM secteurs where parent_sector = '0'");
																		
																		while ($PostRow = mysqli_fetch_array($PostSql))
																		{
																				?>
																					<option value="<?php echo $PostRow["secteur_id"] ?>"><?php echo utf8_encode($PostRow["nom"])?></option>
																				<?php
																		}
																		
																	?>
													</select>
												</div>
										</div>
			
			
										<div class="form-group">
										  <label for="inputJob">Votre Métier</label>
										  <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
											<select class="form-control" id="inputJob" name="inputJob">
											  <option value="">Métiers</option>
											  
											</select>
										  </div>
										</div>

<script>
$(document).ready(function(){

    $('#inputActivity').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_metier.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) 
			{
                console.log(response);
                $("#inputJob").html(response);
            },
        });
    }); 

});
</script>		

										<div class="form-group">
													<label for="inputPassword">Mot de Passe</label>
														<div class="input-group add-on">
														   <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
															<input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Mot de Passe" required>
															<span class="input-group-addon" id="syntaxTrue" style="display:none"><span class="glyphicon glyphicon-ok" style="color:#006442"></span></span>
															<span class="input-group-addon" id="syntaxFalse" style="display:none"><span class="glyphicon glyphicon-remove" style="color:#C3272B"></span></span>
														</div>
														
														<p id="syntax-msg" class="text-danger small"> </p>
										</div>
										
										

										<div class="form-group">
													<label for="inputConfirmPassword">Confirmer Mot de passe</label>
														<div class="input-group">
														   <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
															<input type="password" class="form-control" name="inputConfirmPassword" id="inputConfirmPassword" placeholder="Confirmer Mot de passe" required>
															<span class="input-group-addon" id="matchTrue" style="display:none"><span class="glyphicon glyphicon-ok" style="color:#006442"></span></span>
															<span class="input-group-addon" id="matchFalse" style="display:none"><span class="glyphicon glyphicon-remove" style="color:#C3272B"></span></span>
														</div>
														<p id="match-msg" class="text-danger small"> </p>
										</div>         

										
										
                                         <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Créer compte</button>
										 
										 
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
      
<?php if(!empty($Ad2)){?>
<div class="col-shadow col-ads">
<?php echo $Ad2;?>
</div><!--col-shadow-->
<?php } ?>   

</div><!--col-md-8-->

<div class="col-md-4">
<?php include("side_bar.php");?>
</div><!--col-md-4-->


</div><!--container-->

<?php include("footer.php");?>