<?php include("header_home.php");

?>

<div class="promo">

		<div class="container">
				
				<div class="writings">
					
					<div class="catch">L'expert du quotidien, c'est vous !</div>
					<div class="subcatch"> Rejoignez la communauté pour partager vos expériences, donner votre avis et noter les produits et les services. </div>
				
				</div>


				<div class="front-search ">

					<form role="search" method="get" action="search.php">
					
							<div class="container-fluid">
							
								<div class="row no-gutter">
								
										<div class="form-group col-md-3">
										
												<input type="text" class="form-control input-lg" id="term" name="term" placeholder="Que recherchez-vous ?">
										
										</div>
										
										
										<div class="form-group col-md-3">
										
												<select class="form-control input-lg" id="city" name="city">
													  <option value="all">Villes</option>
													  <?php
																if($SelectCity = $mysqli->query("SELECT city_id, city FROM city")){

																	while($CityRow = mysqli_fetch_array($SelectCity))
																	{
																				
														?>
																	<option value="<?php echo $CityRow['city'];?>"><?php echo $CityRow['city'];?></option>
														<?php

																	}

																	$SelectCity->close();
										
																}
																else
																{
									
																	 printf("Rien à afficher pour le moment.");
																}

														?>
											</select>
										
										</div>
										
										
										<div class="form-group col-md-3">
										
												<?php include("catList.php");	?>	
										
										</div>
										
										<div class="form-group col-md-2">
										
												<?php include("typeList.php");	?>	
										
										</div>
										<style type="text/css">
										   @media (max-width : 992px) {
											.go 
											{
												clear: both ;
											}
										   }
										</style>
										<div class="form-group col-md-1 go">
										
											<button type="submit" class="btn btn-lg btn-danger" id="searchButton"><i class="glyphicon glyphicon-search"></i> </button>
											
										</div>
							
								</div>
								
							</div>
						
					</form>

				</div><!--front-search-->
		</div><!--container-->

</div><!--promo-->

<script>
$(document).ready(function()
{
	$('.star-rates').raty
	({
		readOnly: true,
		score: function() 
		{
			return $(this).attr('data-score');
	    }
	});
});
</script>

<div class="container">

<div class="page-title"><h1><h1>Avis récemment écrites</h1></h1></div>

<?php

if($FeatSql = $mysqli->query("SELECT * FROM produits LEFT JOIN categories ON categories.cat_id=produits.cid ORDER BY produits.date DESC LIMIT 8"))
{

	$CountFeat = mysqli_num_rows($FeatSql);	

	while ($FeatRow = mysqli_fetch_array($FeatSql))
	{
	
		$longFeat = stripslashes($FeatRow['produit_name']);
		$strFeat = strlen ($longFeat);
		
		if ($strFeat > 50) 
		{
			$FeatTitle = substr($longFeat,0,47).'...';
		}
		else
		{
			$FeatTitle = $longFeat;
		}
	
		$FeatLink = preg_replace("![^a-z0-9]+!i", "-", $longFeat);
		$FeatLink = urlencode(strtolower($FeatLink));
	
		$FeatDescription = stripslashes($FeatRow['avis']);
		$strDescription = strlen ($FeatDescription);
		
		$FCName = $FeatRow['category'];
		$FCLink = preg_replace("![^a-z0-9]+!i", "-", $FCName);
		$FCLink = urlencode($FCLink);
		$FCLink = strtolower($FCLink);
		

		

?>

<div class="col-sm-12 col-xs-12 col-md-4 col-lg-3 col-box">
 
	<div class="grid wow fadeInUp"> 
	<div class="inside-pad">
		<div class="author-review-pic">
				<?php
					
					$idauth = $FeatRow['user_id'];
					$sql = "Select username, avatar, idRanking from users where user_id='$idauth'";
					
					if($res = $mysqli->query($sql))
					{
						$row = mysqli_fetch_array($res);
						
						$ProfileAvatar = $row["avatar"];
						$Profilename = $row["username"];
						$idrank = $row['idRanking'];
						
						
						$res -> close();
						
						if (empty($ProfileAvatar))
						{ 
							$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
						}
						elseif (!empty($ProfileAvatar))
						{
							$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
						}
						
						?>
						
						
						<?php
						$sqlrank = "Select rank from ranking where idRanking='$idrank'";
					
						if($rankquery = $mysqli->query($sqlrank))
						{
							$rowRank = mysqli_fetch_array($rankquery);
							
							$rankName = $rowRank["rank"];
						}
						else
						{
							echo 'Error Ranking';
						}
						?>
						
						
							<div class="info-box-details-img">
									<a href="user_profile-<?php echo $idauth;?>-<?php echo $Profilename;?>"><img src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=40&amp;w=40&amp;q=100" class="img-circle"></a>
								
									<div class= "info-styling"> 
										<img src="images/medal.png" style="width: 20px; height: 20px"> <?php echo $rankName;?>  
									</div>
							</div>
							
							<div class="info-box-details-name-rank">
								<a href="user_profile-<?php echo $idauth;?>-<?php echo $Profilename;?>"><?php echo $Profilename; ?></a> a écrit un avis sur
								<h2>
									<a href="avis-<?php echo $FeatRow['idProduit'];?>-<?php echo $FeatLink;?>"><div class="heading-color"><?php echo $FeatTitle;?></div></a>
								</h2>
							</div>
						
						
						<?php 
					}
					else
					{
						echo 'Unknown user';
					}
				?>
				 
		</div>
 
		
        
        
			<a class="over-label" href="category-<?php echo $FeatRow['cid'];?>-<?php echo $FCLink;?>"><?php echo utf8_encode($FeatRow['category']);?></a>
			<div class="img-box-styling">
				<a href="avis-<?php echo $FeatRow['idProduit'];?>-<?php echo $FeatTitle;?>">
					<img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $FeatRow['featured_image'];?>&amp;h=300&amp;w=450&amp;q=100" alt="<?php echo $FeatTitle;?>">
				</a>
			</div>
		
		<div class = "wrap-long-text-review">
			<p><?php echo $FeatDescription;?></p>
		</div>
    
		<div class="post-info-bottom">
			<div class="col-rate">    
				<span class="star-rates"  data-score="<?php echo $FeatRow['avg'];?>"></span> 
			</div>
			
				<?php  get_likes($FeatRow['idProduit'], $mysqli) ?>
		</div>	
	<?php
		if(isset($_SESSION['username']))
		{
	?>
				<div class="review-popularity">
						<?php  utility($FeatRow['idProduit'], $UserId, $mysqli, $idauth) ?>
				</div>
	<?php
		}
	?>
		</div>
	</div><!-- /.grid -->  
    
</div><!-- /.col-sm-12 col-xs-12 col-md-4 col-lg-4 -->

<?php     
	}
$FeatSql->close();
}else{
     printf("There Seems to be an issue");
}
if($CountFeat==0){
?>
<div class="col-note">Il n'y a rien à afficher pour le moment.</div>
<?php }?>


</div><!--container-->

<div class="container-fluid container-color">
<div class="container">
<h1>Faites connaître vos services à de potentiels clients</h1>

<h3>Inscrivez votre entreprise gratuitement sur WeOna !</h3>

<a class="btn btn-danger btn-lg" href="submit" style="white-space: normal">Créer une page pour mon entreprise</a> 

</div><!--container-->
</div><!--container-fluid-->

<div class="container">

<div class="page-title"><h1>Services recemment ajoutés</h1></div>

<?php

if($PostSql = $mysqli->query("SELECT * FROM business LEFT JOIN categories ON categories.cat_id=business.cid WHERE business.active=1 ORDER BY business.biz_id DESC LIMIT 6"))
{

	$CountRows = mysqli_num_rows($PostSql);	

	while ($PostRow = mysqli_fetch_array($PostSql))
	{
	
	$longTitle = stripslashes($PostRow['business_name']);
	$strTitle = strlen ($longTitle);
	if ($strTitle > 25) {
	$PostTitle = substr($longTitle,0,23).'...';
	}else{
	$PostTitle = $longTitle;}
	
	$PostLink = preg_replace("![^a-z0-9]+!i", "-", $longTitle);
	$PostLink = urlencode(strtolower($PostLink));
	
	$longDescription = stripslashes($PostRow['description']);
	$strDescription = strlen ($longDescription);
	if ($strDescription > 70) {
	$Description = substr($longDescription,0,67).'...';
	}else{
	$Description = $longDescription;}
	
	$Tel = stripslashes($PostRow['phone']);
	$City = stripslashes($PostRow['city']);
	$Site = stripslashes($PostRow['website']);
	
	if(!empty($Tel)){
		$Telephone = $Tel;
	}else{
		$Telephone = "N/A";		
	}
	
	$CName = $PostRow['category'];
	$CLink = preg_replace("![^a-z0-9]+!i", "-", $CName);
	$CLink = urlencode($CLink);
	$CLink = strtolower($CLink);
	

?>

<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 col-box">
 
	<div class="grid wow fadeInUp"> 
 
		<a class="over-label" href="category-<?php echo $PostRow['cid'];?>-<?php echo $CLink;?>"><?php echo utf8_encode($PostRow['category']);?></a>
        
        <a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>">
			<img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $PostRow['featured_image'];?>&amp;h=300&amp;w=500&amp;q=100" alt="<?php echo $PostTitle;?>">
		</a>
    
		<h2><a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><?php echo $PostTitle;?></a></h2>
		<p><?php echo $Description;?></p>
    
		<div class="post-info-bottom">
			<div class="col-rate">    
				<span class="star-rates"  data-score="<?php echo $PostRow['avg'];?>"></span> <?php echo $PostRow['reviews'];?> Avis
			</div>

			<div class="info-row"><span class="fa fa-home"></span> <?php echo $City;?></div>
			
			<div class="info-row"><span class="fa fa-phone"></span> <?php echo $Telephone;?></div>
			
			<?php 
				if(!empty($Site))
				{?>
			<div class="info-row"><span class="fa fa-link"></span> <a href="<?php echo $Site;?>" target="_blank">Site Web</a></div>
				<?php 
				}
				else
				{?>
			<div class="info-row"><span class="fa fa-link"></span> N/A</div>
		<?php }?>
		</div>
 
  </div><!-- /.grid -->  
    
</div><!-- /.col-sm-12 col-xs-12 col-md-4 col-lg-4 -->

<?php     
	}
$PostSql->close();
}else{
     printf("There Seems to be an issue");
}
if($CountRows==0){
?>
<div class="col-note">Il n'y a rien à afficher pour le moment.</div>
<?php }?>

</div><!--container-->

<div class="container">

<?php if(!empty($Ad2)){?>
    <div class="col-shadow col-ads-long"> <?php echo $Ad2;?> </div>
    <!--col-shadow-->
<?php }?>

</div><!--container-->

<?php include("footer.php");?>