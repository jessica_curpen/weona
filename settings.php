<?php include("header_settings.php");

if(!isset($_SESSION['email'])){?>
<script type="text/javascript">
function leave() {
window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php }else{?>


<div class="container container-main">

<div class="col-md-8">

<script type="text/javascript" src="js/jquery.form.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/bootstrap-filestyle.min.js"></script> 
<script>
$(function(){
    $('#birthday').datepicker({
    format: 'yyyy/mm/dd',
    startDate: '-3y'
})
});

//Upload Profile Photo

$(document).ready(function()
{
	$('#inputfile').on('change', function()
	{
		$("#preview").html('');
		$("#output-msg").html('<div class"alert alert-info">Importation en cours...</div>');


		$("#PictureForm").ajaxForm(
		{
			dataType:'json',
			success:function(json){
			   $('#preview').html(json.img);
			   $('#output-msg').html(json.msg);
			}
		}).submit();

	});
});

$(function()
{

	$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Choisir une photo"});

});
</script>

<?php

if($Profile = $mysqli->query("SELECT * FROM users WHERE user_id='$UserId'")){

    $ProfileRow = mysqli_fetch_array($Profile);
	
	$Gender = $ProfileRow['gender'];
	
	$mid = $ProfileRow['mid'];
	
	$pid = $ProfileRow['pid'];
	
	$Profile->close();
	
}else{
    
	 printf("<div class='alert alert-danger alert-pull'>Il semble y avoir eu un problème</div>");
}	

?>

<div class="col-shadow">
      <div class="biz-title-2">
        <h1>Importer une photo de profil</h1>
      </div>
      <div class="col-desc">
      
     <div id="uploading"></div>
<div id="output-msg"></div>

<form action="avatar.php" method="post" name="PictureForm" id="PictureForm" enctype="multipart/form-data">
        <!-- begin image label and input -->
		<label>Image (gif, jpg, png)</span>
    </label>
		<input type="file" size="45" name="inputfile" id="inputfile" /><!-- end image label and input -->
 
      </form><!-- end form -->
      
<div id="preview"></div> 
      
      
    </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->  
 
<script>
$(document).ready(function()
{
    $('#FromProfile').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-profile").html('<div class="alert alert-info">Veuillez patienter...</div>');
        $(this).ajaxSubmit({
        target: '#output-profile',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{
    $('#submitButton').removeAttr('disabled'); //enable submit button
}
</script> 
 
<div class="col-shadow">
      <div class="biz-title-2">
        <h1>Modification de vos Informations</h1>
      </div>
<div class="col-desc">

<?php

if($Profile = $mysqli->query("SELECT * FROM users WHERE user_id='$UserId'")){

    $ProfileRow = mysqli_fetch_array($Profile);
	
	$Gender = $ProfileRow['gender'];
	$firstname = $ProfileRow['firstname'];
	$lastname = $ProfileRow['lastname'];
	$tags = $ProfileRow['tags'];
	$mid = $ProfileRow['mid'];
	$pid = $ProfileRow['pid'];
	$Profile->close();
	
}else{
    
	 printf("<div class='alert alert-danger alert-pull'>Un problème est survenu. Merci de ré-essayer.</div>");
}	

?>

<div id="output-profile"></div>

<form action="submit_profile.php" id="FromProfile" method="post" >

<div class="form-group">
    <label for="uName">Nom Utilisateur</label>
    
    <input type="text" class="form-control" disabled="disabled" name="uName" id="uName" value="<?php echo $ProfileRow['username'];?>"/>
</div><!--/ form-group -->

<div class="form-group">
    <label for="firstname">Prenom</label>
    
    <input type="text" class="form-control" name="firstname" id="inputPrenom" value="<?php echo $firstname;?>"/>
</div><!--/ form-group -->

<div class="form-group">
    <label for="lastname">Nom</label>
    
    <input type="text" class="form-control" name="lastname" id="inputNom" value="<?php echo $lastname;?>"/>
</div><!--/ form-group -->

<div class="form-group">
	<label for="sex">Sexe</label>
    <select class="form-control" name="sex" id="sex">
   	<?php if(!empty($Gender)){?>
    <option value="<?php echo $Gender;?>"><?php echo $Gender;?></option>
    <?php }?>
    <option value="">Choisir</option>
	<option value="Male">Homme</option>
    <option value="Female">Femme</option>
    </select>    
</div><!--/ form-group -->

<div class="form-group">    
    <label for="birthday">Anniversaire</label>
    
    <input type="text" class="form-control" name="birthday" id="birthday" value="<?php echo $ProfileRow['birthday'];?>" />
</div><!--/ form-group -->


<!--/
	<div class="form-group">    
		<label for="uEmail">Email</label>
   
		<input type="text" class="form-control" name="uEmail" id="uEmail" placeholder="Entrez une adresse mail valide"/>
	</div>
 -->
<div class="form-group">    
    <label for="country">Pays</label>
    
    <input readonly type="text" class="form-control" name="country" id="country" value="<?php echo $ProfileRow['country'];?>" />
</div><!--/ form-group -->


<div class="form-group">    
    <label for="city">Ville</label>
    
    <input type="text" class="form-control" name="city" id="city" value="<?php echo $ProfileRow['city'];?>" />
</div><!--/ form-group -->


<div class="form-group">    
    <label for="about">Dites en plus sur vous</label>
    
    <textarea name="about" class="form-control" cols="40" rows="5" ><?php echo $ProfileRow['about'];?></textarea>
</div><!--/ form-group -->


<div class="form-group">
    <label for="workplace">Vous travaillez chez</label>
    
    <input type="text" class="form-control" name="workplace" id="workplace" value="<?php echo $ProfileRow['workplace'];?>"/>
</div><!--/ form-group -->



<div class="form-group">
    <label for="workin">Dans le domaine</label>
	
	<div class="input-group">
                   <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
			<select class="form-control" name="inputActivity" id="inputActivity" placeholder="Domaine d'activités">
			<option value="">Domaine d'activités</option>
					<?php
					
						$PostSql = $mysqli->query("SELECT * FROM secteurs where parent_sector = '0'");
						
						while ($PostRow = mysqli_fetch_array($PostSql))
						{
								$secId 	 = $PostRow["secteur_id"];
								$secName = $PostRow["nom"];
								
								if($secId == $mid)
								{
									?>
										<option selected value="<?php echo $secId ?>"><?php echo $secName?></option>
									<?php
								}
								else
								{
								
									?>
										<option value="<?php echo $secId ?>"><?php echo $secName?></option>
									<?php
									
								}
						}
						
					?>
			</select>
	</div>
</div><!--/ form-group -->

	<div class="form-group">
                  <label for="inputJob">Votre Métier</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-briefcase"></span></span>
                    <select class="form-control" id="inputJob" name="inputJob">
                      <option value="">Métiers</option>
					  <?php
					  
						if($mid !=null)
						{
							$PostSql = $mysqli->query("SELECT * FROM secteurs where parent_sector = '$mid'");
							
							while ($PostRow = mysqli_fetch_array($PostSql))
							{
									$secId 	 = $PostRow["secteur_id"];
									$secName = $PostRow["nom"];
									
									if($secId == $pid)
									{
										?>
											<option selected value="<?php echo $secId ?>"><?php echo $secName?></option>
										<?php
									}
									else
									{
									
										?>
											<option value="<?php echo $secId ?>"><?php echo $secName?></option>
										<?php
										
									}
							}
						}
						?>
					  
                    </select>
                  </div>
                </div>
				
	 <script>
$(document).ready(function(){

    $('#inputActivity').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_metier.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) 
			{
                console.log(response);
                $("#inputJob").html(response);
            },
        });
    }); 

});



</script>	



<div class="form-group">
                  <label for="inputTags">Tags</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" data-role="tagsinput" name="inputTags" id="inputTags" value = "<?php echo $ProfileRow['tags'];?>" >
                  </div>
                </div>

    <button type="submit" class="btn btn-lg btn-danger pull-right" id="submitButton">Mettre à jour</button>
  
</form>      
 
</div>
      <!--col-desc--> 
    </div>
    <!--col-shadow--> 

<script>
$(document).ready(function()
{
    $('#FromPassword').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#outputmsg").html('<div class="alert alert-info">Submiting... Please wait...</div>');
        $(this).ajaxSubmit({
        target: '#outputmsg',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{
    $('#submitButton').removeAttr('disabled'); //enable submit button
}
</script>    
    
<div class="col-shadow">
      <div class="biz-title-2">
        <h1>Modifier le mot de passe</h1>
      </div>
<div class="col-desc">

<div id="outputmsg"></div>

<form action="submit_password.php" id="FromPassword" method="post" >

<div class="form-group">
    <label for="nPassword">Mot de passe actuel</label>
    <input type="password" class="form-control" name="nPassword" id="uPassword" placeholder="Entrez votre mot de passe actuel" />
</div><!--/ form-group -->
<div class="form-group">    
     <label for="uPassword">Nouveau mot de passe</label>
    <input type="password" class="form-control" name="uPassword" id="uPassword" placeholder="Entrez le nouveau mot de passe" />
</div><!--/ form-group -->
<div class="form-group">    
     <label for="cPassword">Confirmer Mot de passe</label>
    <input type="password" class="form-control" name="cPassword" id="cPassword" placeholder="Confirmer le nouveau mot de passe" />
</div><!--/ form-group -->
    
  <button type="submit" class="btn btn-lg btn-danger pull-right" id="submitButton">Modifier</button>
  

</form>

</div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->                  

</div><!--col-md-8-->

<div class="col-md-4">
<?php } include("side_bar.php");?>
</div><!--col-md-4-->


</div><!--container-->

<?php include("footer.php");?>