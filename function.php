<?php
	
	function getMonth($month) 
	{
		if($month == 1){return $month= "Janvier";}
		if($month == 2){return $month= "Février";}
		if($month == 3){return $month= "Mars";}
		if($month == 4){return $month= "Avril";}
		if($month == 5){return $month= "Mai";}
		if($month == 6){return $month= "Juin";}
		if($month == 7){return $month= "Juillet";}
		if($month == 8){return $month= "Août";}
		if($month == 9){return $month= "Septembre";}
		if($month == 10){return $month= "Octobre";}
		if($month == 11){return $month= "Novembre";}
		if($month == 12){return $month= "Décembre";}
	}

	function getFrenchDay($day)
	{
		if($day == "Mon"){return $day = "Lun";}
		if($day == "Tue"){return $day = "Mar";}
		if($day == "Wed"){return $day = "Mer";}
		if($day == "Thu"){return $day = "Jeu";}
		if($day == "Fri"){return $day = "Ven";}
		if($day == "Sat"){return $day = "Sam";}
		if($day == "Sun"){return $day = "Dim";}
	}
	
	function getFullFrenchDay($day)
	{
		if($day == "Mon"){return $day = "Lundi";}
		if($day == "Tue"){return $day = "Mardi";}
		if($day == "Wed"){return $day = "Mercredi";}
		if($day == "Thu"){return $day = "Jeudi";}
		if($day == "Fri"){return $day = "Vendredi";}
		if($day == "Sat"){return $day = "Samedi";}
		if($day == "Sun"){return $day = "Dimanche";}
	}
	
	function getFinalDate($dateMessage)
	{
		$today = date_create(date("Y-m-d"));
		$dm = date_create($dateMessage);
															
		$day1 = date_format($today, 'd');
		$day2 = date_format($dm, 'd');
															
		$month1 = date_format($today, 'm');
		$month2 = date_format($dm, 'm');
															
		$year1 = date_format($today, 'Y');
		$year2 = date_format($dm, 'Y');
															
		if( ($day1 == $day2) && ($month1 == $month2) && ($year1 == $year2) )
		{
			echo date_format($dm, 'H:i');
		}
		elseif(($month1 == $month2) && ($year1 == $year2))
		{
			$diff = date_diff($dm,$today);
			$days = $diff->format('%a');
																
			$timestamp = strtotime($dateMessage);
			$day = date('D', $timestamp);
			$month = date('n', $timestamp);
																
			$day = getFrenchDay($day);
			$month = getMonth($month);
																
			if($days < 1)
			{
				echo 'Hier'.' ';
				echo date_format($dm, 'H:i');
			}
			elseif($days > 1 || $days < 6)
			{
				echo $day.'  ';
				echo date_format($dm, 'H:i');
			}
			else
			{
				echo date_format($dm, 'd').'  ';
				echo $month;
			}
		}
		elseif($year1 > $year2)
		{
			$timestamp = strtotime($dateMessage);
			$month = date('n', $timestamp);
			$month = getMonth($month);
			echo $month.'  ';
			echo date_format($dm, 'Y');
		}
		else
		{
			$timestamp = strtotime($dateMessage);
			$day = date('D', $timestamp);
			$month = date('n', $timestamp);
																
			$day = getFrenchDay($day);
			$month = getMonth($month);
																
			echo date_format($dm, 'd').'  ';
			echo $month;
		}
	}
	
	function FinalDate($dateMessage)
	{
		$today = date_create(date("Y-m-d"));
		$dm = date_create($dateMessage);
															
		$day1 = date_format($today, 'd');
		$day2 = date_format($dm, 'd');
															
		$month1 = date_format($today, 'm');
		$month2 = date_format($dm, 'm');
															
		$year1 = date_format($today, 'Y');
		$year2 = date_format($dm, 'Y');
															
		if( ($day1 == $day2) && ($month1 == $month2) && ($year1 == $year2) )
		{
			$t= date_format($dm, 'H:i');
		}
		elseif(($month1 == $month2) && ($year1 == $year2))
		{
			$diff = date_diff($dm,$today);
			$days = $diff->format('%a');
																
			$timestamp = strtotime($dateMessage);
			$day = date('D', $timestamp);
			$month = date('n', $timestamp);
																
			$day = getFrenchDay($day);
			$month = getMonth($month);
																
			if($days < 1)
			{
				$t =  'Hier'.' '.date_format($dm, 'H:i');
			}
			elseif($days > 1 || $days < 6)
			{
				$t = $day.'  '.date_format($dm, 'H:i');
			}
			else
			{
				$t = date_format($dm, 'd').'  '.$month;
			}
		}
		elseif($year1 > $year2)
		{
			$timestamp = strtotime($dateMessage);
			$month = date('n', $timestamp);
			$month = getMonth($month);
			$t= $month.'  '.date_format($dm, 'Y');
		}
		else
		{
			$timestamp = strtotime($dateMessage);
			$day = date('d', $timestamp);
			$month = date('n', $timestamp);
																
			$day = getFrenchDay($day);
			$month = getMonth($month);
																
			$t = date_format($dm, 'd').'  '.$month;
		}
		return $t;
	}
	
	
	
	function convertDate($fulldate)
	{
		$dm = date_create($fulldate);
		
		$day 	= date_format($dm, 'd');
		$month 	= date_format($dm, 'm');
		$year	= date_format($dm, 'Y');
		
		$timestamp = strtotime($fulldate);
		$dayN = date('D', $timestamp);
		
		/*$month	= date('n', $fulldate);
		$day	= date('d', $fulldate);
		$year	= date('y', $fulldate);*/
		
		$dayName	= getFullFrenchDay($dayN);
		$monthName = getMonth($month);
		
		$date = $dayName." ".$day." ".$monthName." ".$year ;
		return $date;
	}
	
	
	function getTime($start, $end)
	{
		$dmStart = date_create($start);
		$dmEnd = date_create($end);
		
		$tStart	= date_format($dmStart, 'H:i');
		$tEnd	= date_format($dmEnd, 'H:i');
		
		return $tStart." et ".$tEnd;
	}
	
	
	function getDayNum($day)
{
	if($day =="Lun" || $day == "Mon")
	{
		return 1;
	}
	
	if($day == "Mar" || $day == "Tue")
	{
		return 2;
	}
	
	if($day == "Mer"  || $day == "Wed")
	{
		return 3;
	}
	
	if($day == "Jeu"  || $day == "Thu")
	{
		return 4;
	}
	
	if($day == "Ven" || $day == "Fri")
	{
		return 5;
	}
	
	if($day == "Sam"  || $day == "Sat")
	{
		return 6;
	}
	
	if($day == "Dim" || $day == "Sun")
	{
		return 0;
	}
}

	
	
	
	function getRank($mysqli,$idRanking, $points, $Uid , $firstname,  $lastname , $email, $SiteContact, $SiteName, $from)
	{
				//-------------------------Get Ranking Info
			if($executeRanking = $mysqli->query("SELECT * FROM ranking WHERE idRanking='$idRanking'"))
			{

				$rankingInfo = mysqli_fetch_array($executeRanking);
				
				$rank 			 	 = $rankingInfo['rank'];
				$rankmin 			 = $rankingInfo['min_points'];
				$rankmax 			 = $rankingInfo['max_points'];
				
				
				
				if($points >= $rankmin && $points < $rankmax)
				{
						// Keep rank
				}
				elseif($points >= $rankmax)
				{
					$executeRanking2 = $mysqli->query("SELECT * FROM ranking WHERE min_points='$rankmax'");
					
					$rankingInfo2 = mysqli_fetch_array($executeRanking2);
					
					$rank 			 	 = $rankingInfo2['rank'];
					$rankid			 	 = $rankingInfo2['idRanking'];
					$rankmin 			 = $rankingInfo2['min_points'];
					$rankmax 			 = $rankingInfo2['max_points'];
					
					$mysqli->query("Update users set  idRanking= '$rankid' where user_id='$Uid'");
					
					$executeRanking2->close();
					
					$ToName		 	 = $firstname." ".$lastname;
					$FromEmail		 	 = $email ;
					$FrominputSubject	 = "Félicitations. Vous êtes monté en rang";
					$FromMessage	 	 = "Dear ".$ToName.",
																<br/>
																Vous avez été promu ". $rank .".
																<br/><br/>
																
																All the best,
																<br/>
																".$from;

					require_once('class.phpmailer.php');

					$mail = new PHPMailer() ;

					$mail->AddReplyTo($FromEmail, $from);

					$mail->SetFrom($FromEmail, $from);

					$mail->AddReplyTo($FromEmail, $from);

					$mail->AddAddress($SiteContact, $SiteName);

					$mail->Subject = $FrominputSubject;

					$mail->MsgHTML($FromMessage);

					$mail->Send();
				}
				$executeRanking->close();
			}
			else
			{
				 
			}
	}

	
	function get_likes($idProduit, $mysqli)
	{
		
		$getReviewInfoSql = "Select * from likes_produits where idProduit = '$idProduit'";
		
		if($getReviewInfoSqlFetch = $mysqli->query($getReviewInfoSql))
		{
			$likes_count   = $getReviewInfoSqlFetch->num_rows;
			
			if($likes_count > 0)
			{
				
				?>
				
			<div class="num_likes" id="num_likesID">
				<div class="subnumLikes">
					<?php echo $likes_count ; ?>
				</div>
				<div class="subnumLikes2">
					<img src = "images/hands.png" style="width: 20px; height: 20px;">
				</div>
			</div>
				
				<?php
				
			}
			else
			{
				?>
				
			<div class="num_likes hide" id="num_likesID">
				<div class="subnumLikes">
					0
				</div>
				<div class="subnumLikes2">
					<img src = "images/hands.png" style="width: 20px; height: 20px;">
				</div>
			</div>
				
				<?php
			}
	
		}
		else
		{
			printf("<div class='alert alert-danger alert-pull'>Unable to get number of likes</div>");
		}
		
	}
	
	function utility($idProduit, $idSessionUser, $mysqli, $idauth )
	{
				
			if($idauth == $idSessionUser)
			{
				?>
				<div class="user-post"> Vous avez écrit cet avis</div>
				<?php
			}
			else
			{
							$sql = "Select * from likes_produits where idProduit = '$idProduit' And idUser= '$idSessionUser'";
							
							if($res = $mysqli->query($sql))
							{
								$alreadyliked   = $res->num_rows;
								
								if($alreadyliked == 1)
								{
									?>
									
									<div class="useful"> Utile </div>
									
									<?php
								}
								elseif($alreadyliked > 1)
								{
									printf("<div class='alert alert-danger alert-pull'>Problem with the number of likes</div>");
								}
								elseif($alreadyliked == 0)
								{
									?>
									<div class="usefullness"> 
										Cet avis est utile
										<input readonly class="hidden-cat" name="idSUser" value="<?php echo $idSessionUser ;?>">
										<input readonly class="hidden-cat" name="idProd" value="<?php echo $idProduit?> ;">
										<input readonly class="hidden-cat" name="idAut" value="<?php echo $idauth?> ;">
									</div>
									<div class="useful2">Utile </div>
									<?php
								}
							}
							else
							{
								printf("<div class='alert alert-danger alert-pull'>Unable to get number of likes</div>");
							}
			}
				
	}
	
	
	
	
	function like_product_review($id,$idauteur,$idProd, $mysqli )
	{
		//Get User Info
		$userInfo = $mysqli->query("SELECT * FROM users WHERE user_id='$id'");
		
		$user = mysqli_fetch_array($userInfo);

		$firstname	= $user["firstname"];
		$lastname	= $user["lastname"];
		$username 	= $user["username"];
		
		
		$userInfo->close();
		
		
		//----------------Get Action for like review
		$actionInfo = $mysqli->query("SELECT * FROM actions WHERE action='like_review'");
		$action = mysqli_fetch_array($actionInfo);

		$points = $action["points"];
		$coins  = $action["coins"];

		$actionInfo->close();
		
		
		
		//----------------Get Action for like review
		$actionInfo = $mysqli->query("SELECT * FROM actions WHERE action='review_liked'");
		$action = mysqli_fetch_array($actionInfo);

		$Authpoints = $action["points"];
		$Authcoins  = $action["coins"];

		$actionInfo->close();
		
		
		//Get Product Info
		$productInfo = $mysqli->query("SELECT produit_name FROM produits WHERE idProduit='$idProd'");
		$product = mysqli_fetch_array($productInfo);

		$prodname = $product["produit_name"];
	
		$productInfo->close();
		
		
		//Send Mail
		$subject = "Vous avez aimé un avis.";
		$message = ",
						<br/>
						Vous avez aimé un avis sur ".$prodname.". Vous avez gagné ".$points." points et ".$coins." pièces.
						<br/><br/>
																
						All the best,
						<br/>
					";
					
		updateUserRank($id, $points, $coins,$mysqli, $subject, $message);	
		
		//Send Mail
		$subject2 = $firstname." ".$lastname." a aimé votre avis";
		$message2 = ",
						<br/>
						".$firstname." ".$lastname." a aimé votre avis sur ".$prodname.". Vous avez gagné ".$points." points et ".$coins." pièces.
						<br/><br/>
																
						All the best,
						<br/>
					";
		
		
		updateUserRank($idauteur, $Authpoints, $Authcoins,$mysqli, $subject2, $message2 );
		
	}
	
	function updateUserRank($id, $points, $coins, $mysqli, $subject, $message)
	{
		//Get User Info
		$userInfo = $mysqli->query("SELECT * FROM users WHERE user_id='$id'");
		
		$user = mysqli_fetch_array($userInfo);

		$Userpoints = $user["points"];
		$Usercoins  = $user["money"];
		$idRanking	= $user["idRanking"];
		$firstname	= $user["firstname"];
		$lastname	= $user["lastname"];
		$username 	= $user["username"];
		$email		= $user["email"];
		
		$userInfo->close();
		
		
		$Userpoints = $points + $Userpoints;
		$Usercoins  = $coins  + $Usercoins;
		
		//Update User Info
		$userInfo = $mysqli->query("Update users set money='$Usercoins', points='$Userpoints' where user_id='$id'");		
		
		//Get Site Info
		$squ = $mysqli->query("SELECT * FROM settings WHERE id='1'");
			
		$Settings = mysqli_fetch_array($squ);
				
		$SiteName		 	 = $Settings['site_title'];
		$SiteContact	 	 = $Settings['site_email'];
		$from				 = $Settings['site_title'];
		
		$squ->close();
		
		$ToName		 	 = $firstname." ".$lastname;
		$FromEmail		 	 = $email ;
		$FrominputSubject	 = $subject ;
		$FromMessage	 	 = "Dear ".$ToName.$message.$from ;
		
		require_once('class.phpmailer.php');

		$mail = new PHPMailer() ;

		$mail->AddReplyTo($FromEmail, $from);

		$mail->SetFrom($FromEmail, $from);

		$mail->AddReplyTo($FromEmail, $from);

		$mail->AddAddress($SiteContact, $SiteName);

		$mail->Subject = $FrominputSubject;

		$mail->MsgHTML($FromMessage);

		$mail->Send();
		
		
		getRank($mysqli,$idRanking, $Userpoints, $id , $firstname,  $lastname , $email, $SiteContact, $SiteName, $from);
		
	}

?>