<?php
			session_start();
			include('db.php');
			include('function.php');

			//-------------------------Get Site Info
			if($squ = $mysqli->query("SELECT * FROM settings WHERE id='1'"))
			{

				$settings = mysqli_fetch_array($squ);
				
				$Active 			 = $settings['active'];
				$SiteName		 	 = $settings['site_title'];
				$SiteContact	 	 = $settings['site_email'];
				$from				 = $settings['site_title'];
				$squ->close();
			}
			else
			{
				 printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");;
			}


			//---------------------------Get action Info
			if($action = $mysqli->query("SELECT * FROM actions WHERE action='add_prod_review'")){

				$actionInfo = mysqli_fetch_array($action);
				
				$actionPoints = $actionInfo['points'];
				$actionCoins = $actionInfo['coins'];

				$action->close();
			}
			else
			{
				 printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");;
			}


			//-----------------------------------Get user info

			$Uname = $_SESSION['username'];
			$Uemail = $_SESSION['email'];

			if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$Uemail'"))
			{

				$UserRow = mysqli_fetch_array($UserSql);

				$Uid 		= $UserRow['user_id'];
				$Uname 		= $UserRow['username'];
				$firstname  = $UserRow['firstname'];
				$lastname 	= $UserRow['lastname'];
				$email 	    = $UserRow['email'];
				$money 	    = $UserRow['money'];
				$Uname		= $UserRow['username'];
				$points     = $UserRow['points'];
				$idRanking  = $UserRow['idRanking'];
				
				$UserSql->close();
				
			}
			else
			{
				 
				 printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");
				 
			}



$UploadDirectory	= 'uploads/';
 

if (!@file_exists($UploadDirectory)) {
	//destination folder does not exist
	die("Make sure Upload directory exist!");
}

if($_POST)
{		
	if(!isset($_POST['inputProdname']) || strlen($_POST['inputProdname'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez entrer un nom de produit valide</div>');
	}
	
	if(!isset($_POST['score']) || strlen($_POST['score'])== "")
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Vous devez donner une note</div>');
	}
	
	if(!isset($_POST['inputReview']) || strlen($_POST['inputReview'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Votre avis est trop court</div>');
	}
	
	if(!isset($_FILES['inputImage']))
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez ajouter une image</div>');
	}
	
	if($_FILES['inputImage']['error'])
	{
		//File upload error encountered
		die(upload_errors($_FILES['inputImage']['error']));
	}
	
	if(!isset($_POST['inputCategory']) || strlen($_POST['inputCategory'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez choisir une catégorie</div>');
	}
	

	$FileName			= strtolower($_FILES['inputImage']['name']); 
	$ImageExt			= substr($FileName, strrpos($FileName, '.')); 
	$FileType			= $_FILES['inputImage']['type']; 
	$FileSize			= $_FILES['inputImage']["size"]; 
	$RandNumber   		= rand(0, 9999999999); 
	
	
	$ProdName			= $mysqli->escape_string($_POST['inputProdname']);
	$ProdReview			= $mysqli->escape_string($_POST['inputReview']);
	$ProdScore			= $mysqli->escape_string($_POST['score']);
	$Category           = $mysqli->escape_string($_POST['inputCategory']);
	$Sub	            = $mysqli->escape_string($_POST['inputSubcategory']);
	
	$Tags               = $mysqli->escape_string($_POST['inputTags']);
	$Uniqid 			= $mysqli->escape_string($_GET['id']);
	

	
	switch(strtolower($FileType))
	{
		//allowed file types
		case 'image/jpeg': //jpeg file
			break;
		case 'image/png': //png file
		break;
		default:
			die('<div class="alert alert-danger" role="alert">Le format de l\'image n\'est pas reconnue. Veuillez importer des photos au format PNG ou JPEG.</div>'); //output error
	}
	
	function clean($string) 
	{
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
  
	//Image File Title will be used as new File name
	$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($ProdName));
	$NewFileName = clean($NewFileName);
	$NewFileName = $NewFileName.'_'.$RandNumber.$ImageExt;

	//Rename and save uploded image file to destination folder.
   if(move_uploaded_file($_FILES['inputImage']["tmp_name"], $UploadDirectory . $NewFileName ))
   {
	
		
// Insert info into database table.. do w.e!
$mysqli->query("INSERT INTO produits(produit_name, avis, featured_image, cid, sid, tags, avg, unique_biz, user_id) VALUES ('$ProdName','$ProdReview', '$NewFileName','$Category','$Sub','$Tags','$ProdScore','$Uniqid','$Uid')");

$money = $actionCoins + $money;
$points = $actionPoints + $points;

	$mysqli->query("Update users set money='$money', points = '$points' where user_id='$Uid'");

	
	
	$ToName		 	 = $firstname." ".$lastname;
	$FromEmail		 	 = $email ;
	$FrominputSubject	 = utf8_decode("Votre avis a été posté");
	$FromMessage	 	 = utf8_decode($ToName.",
												<br/>
												Votre avis a été posté.
												Vous avez gagné ".$actionCoins." pièces et ".$actionPoints."points.
												<br/><br/>
												
												Cordialement,
												<br/>
												".$from);

	require_once('class.phpmailer.php');

	$mail = new PHPMailer() ;

	$mail->AddReplyTo($FromEmail, $from);

	$mail->SetFrom($SiteContact, $from);

	

	$mail->AddAddress($Uemail);

	$mail->Subject = $FrominputSubject;

	$mail->MsgHTML($FromMessage);

	$mail->Send();


?>

<script>

	$('#SubmitForm').delay(1000).resetForm(1000);
	$('#SubmitForm').delay(1000).slideUp(1000);
	$('#')
	
	$(document).ready(function()
	{
		$("#SubmitHours #submitButton").prop('disabled', false);
		$("#imageform #submitButton").prop('disabled', false);
	});
</script>

<?php

		getRank($mysqli, $idRanking, $points, $Uid , $firstname,  $lastname , $email, $SiteContact, $SiteName, $from); 
	
		die('<div class="alert alert-success" role="alert">Votre avis a été ajouté avec succès.</div>');
		
   
   }
   else
   {
   		die('<div class="alert alert-danger" role="alert">Un problème est survenu. Avis non sauvegardé</div>');
   } 
   
 
}

function upload_errors($err_code) {
	switch ($err_code) { 
        case UPLOAD_ERR_INI_SIZE: 
            return '<div class="alert alert-danger" role="alert">La taille de l\'image est trop grande !</div>'; 
        case UPLOAD_ERR_FORM_SIZE: 
            return '<div class="alert alert-danger" role="alert">La taille de l\'image est trop grande !</div>'; 
        case UPLOAD_ERR_PARTIAL: 
            return '<div class="alert alert-danger" role="alert">Votre service a été créée mais l\'image n\'a pas été importé.</div>'; 
        case UPLOAD_ERR_NO_FILE: 
            return '<div class="alert alert-danger" role="alert">Votre service a été créée mais l\'image n\'a pas été importé.</div>'; 
        case UPLOAD_ERR_NO_TMP_DIR: 
            return '<div class="alert alert-danger" role="alert">Il semble y avoir eu un problème.</div>'; 
        case UPLOAD_ERR_CANT_WRITE: 
            return '<div class="alert alert-danger" role="alert">Il semble y avoir eu un problème.</div>'; 
        case UPLOAD_ERR_EXTENSION: 
            return '<div class="alert alert-danger" role="alert">Il semble y avoir eu un problème.</div>'; 
        default: 
            return '<div class="alert alert-danger" role="alert">Il semble y avoir eu un problème.</div>'; 
    }  
} 
?>