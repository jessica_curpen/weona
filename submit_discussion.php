<?php

	session_start() ;
	include('db.php');
	include('function.php');
	
	if($squ = $mysqli->query("SELECT * FROM settings WHERE id='1'"))
	{

				$settings = mysqli_fetch_array($squ);
				
				$Active = $settings['active'];
				$SiteName		 	 = $settings['site_title'];
				$SiteContact	 	 = $settings['site_email'];
				$from				 = $settings['site_title'];

				$squ->close();
	}
	else
	{
				printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");;
	}


	//Get action Info
	if($action = $mysqli->query("SELECT * FROM actions WHERE action='create_discussion'"))
	{

			$actionInfo = mysqli_fetch_array($action);
			
			$actionPoints = $actionInfo['points'];
			$actionCoins = $actionInfo['coins'];


			$action->close();
	}
	else
	{
			printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");;
	}
	
	
	
	//Get user info

	$Uname = $_SESSION['username'];
	$Uemail = $_SESSION['email'];

	if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$Uemail'"))
	{

			$UserRow = mysqli_fetch_array($UserSql);

			$id = $UserRow['user_id'];
			$Uname = $UserRow['username'];
			$firstname = $UserRow['firstname'];
			$lastname = $UserRow['lastname'];
			$email = $UserRow['email'];
			$money = $UserRow['money'];
			$points = $UserRow['points'];
			$idRanking = $UserRow['idRanking'];
			
			$UserSql->close();
	
	}
	else
	{
     
		printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Try again</div>");
	 
	}




	if($_POST)
	{
				if(!isset($_POST['sujet-disc']) || strlen($_POST['sujet-disc'])<1)
				{
					//required variables are empty
					die('<div class="alert alert-danger" role="alert">Veuillez entrer un titre valide</div>');
				}
				
				if(!isset($_POST['description-disc']) || strlen($_POST['description-disc'])<1)
				{
					//required variables are empty
					die('<div class="alert alert-danger" role="alert">Veuillez entrer une description valide</div>');
				}
				
				if(!isset($_POST['inputCategory']) || strlen($_POST['inputCategory'])<1)
				{
					//required variables are empty
					die('<div class="alert alert-danger" role="alert">Veuillez choisir une catégorie</div>');
				}
	
	


				$title              = $mysqli->escape_string($_POST['sujet-disc']);
				$description        = $mysqli->escape_string($_POST['description-disc']);
				$cat_id             = $mysqli->escape_string($_POST['inputCategory']);
				$sid	            = $mysqli->escape_string($_POST['inputSubcategory']);

				$sql = "Insert into discussions (user_id, sujet, description, closed, cid, sid) values ('$id', '$title', '$description', '0', '$cat_id', '$sid')";

				
				
				if($res = $mysqli->query($sql))
				{

					$money = $actionCoins + $money;
					$points = $actionPoints + $points;
					
					$mysqli->query("Update users set money='$money', points = '$points' where user_id='$id'");
					
					
					$ToName		 	 = $firstname." ".$lastname;
					$FromEmail		 	 = $email ;
					$FrominputSubject	 = "Vous avez ".utf8_encode('crée')." une nouvelle discussion";
					$FromMessage	 	 = "Cher ".$ToName.",
															<br/>
															Vous avez commencé une nouvelle discussion.
															Vous avez gagné ".$actionCoins." pièces d'or et ".$actionPoints."points.
															<br/><br/>
															
															Sincèrement,
															<br/>
															".$from;
					$FromMessage = utf8_decode($FromMessage);

					require_once('class.phpmailer.php');

					$mail = new PHPMailer() ;

					$mail->AddReplyTo($FromEmail, $from);

					$mail->SetFrom($FromEmail, $from);

					$mail->AddReplyTo($FromEmail, $from);

					$mail->AddAddress($SiteContact, $SiteName);

					$mail->Subject = $FrominputSubject;

					$mail->MsgHTML($FromMessage);

					$mail->Send();
					
					getRank($mysqli, $idRanking, $money, $id , $firstname,  $lastname , $email, $SiteContact, $SiteName, $from);

					
				}

				
				?>
				
<script type="text/javascript">
function leave() {
window.location = "forum";
}
setTimeout("leave()", 1000);
</script>
				<?php		
		
	die('<div class="alert alert-success">La discussion a été créer. Vous aller être redirigé vers le forum</div>');
		

   }else{
   		die('<div class="alert alert-danger">Un problème est survenu. Veuillez ré-essayer.</div>');
   } 
?>