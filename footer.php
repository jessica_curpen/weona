<footer class="main-footer">

<div class="container">

<div class="footer-center"><a href="advertise">Publicité</a>&nbsp; | &nbsp;<a  href="privacy_policy">Vie privée</a>&nbsp; | &nbsp;<a  href="tos">Termes</a>&nbsp; | &nbsp;<a href="about_us">A propos de Weona</a>&nbsp; | &nbsp;<a href="contact_us">Contactez Nous</a></div><!--footer-center-->

<div class="footer-center">
<a class="footer-btns fa fa-facebook" href="<?php echo $FaceBook;?>" target="_blank"></a>
<a class="footer-btns fa fa-twitter" href="<?php echo $Twitter;?>" target="_blank"></a>
<a class="footer-btns fa fa-google-plus" href="<?php echo $Gplus;?>" target="_blank"></a>
<a class="footer-btns fa fa-pinterest-p" href="<?php echo $Pinterest;?>" target="_blank"></a>

</div><!--footer-center-->

<div class="footer-center copyright">
&#169; <?php echo date("Y");?> <?php echo $SiteTitle;?>
</div><!--footer-center-->

</div> <!--container-->

</footer>

<?php if(!empty($Ad3)){?><div class="col-ad-mobile"><?php echo $Ad3;?></div><?php }?>

</div><!--wrap-->
</body>
</html>