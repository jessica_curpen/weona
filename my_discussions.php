<?php

		include("header.php");
		
		$Uname = $_SESSION['username'];
		$Uemail = $_SESSION['email'];


		if($ProfileSql = $mysqli->query("SELECT * FROM users WHERE email='$Uemail'"))
		{
			$ProfileInfo = mysqli_fetch_array($ProfileSql);
			
			$name 			= $ProfileInfo['username'];	
			$id 			= $ProfileInfo['user_id'];	
			$ProfileAvatar	= $ProfileInfo['avatar'];
			
			$ProfileSql->close();
		}
		else
		{
			 printf("Un problème est survenu."); 
		}

		if (empty($ProfileAvatar))
		{ 
				$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
		}
		elseif (!empty($ProfileAvatar))
		{
				$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
		}
?>

<div class="container container-main2">

		<div class="col-md-3">
		
			<div class="new-disc">
				<div class="button-style raised iris" id="start-discussion">
				  <div class="center" fit>Commencer une discussion</div>
				  <paper-ripple fit></paper-ripple>
				</div>
			</div>
			
			<!--Tableau de bord Forum-->
			<div class="categories-disc" style="margin-top: 10px">
			<div class="col-shadow">
				<div class="row">
				
						<div class="col-md-12"> <div class="tab-forum"> <a href="forum.php"> Discussions récentes </a> </div></div>
						<div class="col-md-12"> <div class="tab-forum"> <a href="my_discussions.php"> Mes discussions </a></div></div>
						<div class="col-md-12">
							<div class="tab-forum-notif">
								
							</div>
						</div>	
				</div>
			</div>	
			</div>
		
		</div>
		
		
		<div class="col-md-7">
		
				<div class="col-shadow">
				
					<div class="biz-title-2">
						<h1>Vos discussions</h1>
					</div>
					
					<div class="container">
						<?php
								
								if($sql = $mysqli->query("Select * from discussions where user_id= '$id'"))
								{
									$NumD = $sql->num_rows;
									
									if($NumD == 0)
									{
										echo '<p>Vous n\'avez pas encore créer de discussions</p>';	
									}
									else
									{
										while($row = mysqli_fetch_array($sql))
										{
												
												$id 			= $row["id_discussion"];
												$id_auteur 		= $row["user_id"];
												$sujet 			= $row["sujet"];
												$description 	= $row["description"];
												$date 			= $row["date_post"];
												$cat_id 		= $row["cid"];
												$closed 		= $row["closed"];
												
												$description = stripslashes($description);
												
												$sujetTrade	 = stripslashes($sujet);
												$sujetTrade = preg_replace("![^a-z0-9]+!i", "-", $sujetTrade);
												
												$catsql = "Select * from categories where cat_id='$cat_id'";
							
												if($cat = $mysqli->query($catsql))
												{
													$category = mysqli_fetch_array($cat);
													
													$name = $category["category"];
													$parent_id = $category["parent_id"];
													
													$cat->close();
												}
												else
												{
													printf("Un problème est survenu.");
												}
												
												
												if($par =  $mysqli->query("Select * from participation where idDiscussion= '$id'"))
												{
														$numParticipation = $par->num_rows ;
														$replies = $numParticipation." réponses" ;
												}
												
												
												?>
												<div style="padding: 10px">
												
														<h2> <a href="discussion-<?php echo $id ; ?>-<?php echo $sujetTrade ; ?>"><?php echo $sujet ?></a> </h2>
														<div class="cat-details" style="padding-top: 6px ;"> 
															<?php 
																if($parent_id == '0')
																{
																	echo utf8_encode($name)." - ".$replies	;
																}
																else
																{
																	$parent_cat = "Select * from categories where cat_id='$parent_id'";
																	
																	if($pcat = $mysqli->query($parent_cat))
																	{
																		$category = mysqli_fetch_array($pcat);
										
																		$name2 = utf8_encode($category["category"]);
																		$parent_id = $category["parent_id"];
																		
																		$pcat->close();
																		
																		echo $name2. ' > '.$name.' - '.$replies;
																	}
																	else
																	{
																		echo 'Un problème est survenue';
																	}
																}
															?>
														</div>
														<p style="margin-top: 25px">	 
															<div style="text-align: justify ;"> <?php echo $description ?> </div> 
														</p>
														<p style="margin-top: 10px">
															<div class="date">Date de post : <?php getFinalDate($date) ?> - 
															<?php
																	if($closed == 0)
																	{
																			echo 'Discussion Ouverte';
																	}
																	else
																	{
																			echo 'Discussion Fermée';
																	}
															?>
														
															</div>
														</p>
												</div>
												<?php
										}
									}
								}
								else
								{
									echo "<p>Une erreur empêche l'affichage de vos discussions. Veuillez nous en excuser.</p>";
								}
						?>
					</div>
					
				</div>
		
		</div>

</div>
<script type="text/javascript">
window.onbeforeunload = function(){
  setTimeout("leave()", 1000);
};

</script>
<?php include("footer.php");?>