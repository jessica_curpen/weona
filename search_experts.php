<?php 		

			if($city == 'all')
			{
				$PostSql = $mysqli->query("SELECT * FROM users WHERE about LIKE '%$term%' OR tags LIKE '%$term%' ORDER BY user_id ASC LIMIT 0, 2");
			}
			else
			{
				$PostSql = $mysqli->query("SELECT * FROM users WHERE city='$city' AND (users.about LIKE '%$term%' OR users.tags LIKE '%$term%') ORDER BY user_id ASC LIMIT 0, 2");
			}
			
	
			$CountRows = mysqli_num_rows($PostSql);	
			
			?>
			
			<div class="col-md-2">
				<input id="term_value" name="term_value" value="<?php echo $term ?>" class="user-iden">
				<input id="city" name="city" value="<?php echo $city ?>" class="user-iden" >
				<input id="type" name="type" value="4" readonly  class="user-iden" >
			</div>
			
			<div class="col-md-8">
			<div class="container">
				<ul class="list-group" id="display_experts">
			
			<?php

			while ($PostRow = mysqli_fetch_array($PostSql))
			{		
				$username 			= $PostRow['username'];
				$firstname 			= $PostRow['firstname'];
				$lastname 			= $PostRow['lastname'];
				$ProfileAvatar 		= $PostRow['avatar'];
				$idRanking 			= $PostRow['idRanking'];
				$id 				= $PostRow['user_id'];
				$workplace 			= $PostRow['workplace'];
				
				if (empty($ProfileAvatar))
				{ 
					$ProfilePic =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
				}
				elseif (!empty($ProfileAvatar))
				{
					$ProfilePic =  'http://'.$SiteLink.'/avatars/'.$ProfileAvatar;
				}
				
				//Get Number of Followers
				$follower = $mysqli->query("SELECT * FROM followers WHERE idFollowed='$id'");
				$NumFollowers = $follower->num_rows;

				//Get Ranking
				$userRank = $mysqli->query("SELECT rank, image FROM ranking WHERE idRanking='$idRanking'");
				$rankInfo = mysqli_fetch_array($userRank);

				$rankname = $rankInfo["rank"];
				$rankimage = $rankInfo["image"];

				$userRank->close();

				$last_id = $PostRow['user_id'];
			?>
				<script type="text/javascript"> 
						var last_id = <?php echo $last_id ?> ; 
				</script>
				
				<li class="list-group-item">
			
					  <div class="row no-gutter " >
						<div class="col-sm-2">
							<img style="margin-left: 10px ; margin-right: 10px" src="thumbs.php?src=<?php echo $ProfilePic;?>&amp;h=85&amp;w=85&amp;q=80" class="img-rounded">
						</div>
						<div class="col-sm-6">
							<a href="user_profile-<?php echo $id;?>-<?php echo $username;?>"><h2><?php echo $username?>	</h2></a>
							<?php 
							if($workplace != null)
							{
								echo '<p class="text-muted">Travaille chez '.$workplace.'</p>' ;
							}
							?>
							<table style="margin-top : 10px" class="pull-bottom">
									<tr>
									
											<td class="center"> <img src="images/badges/group.png" style="width: 20px; height: 20px "></td>
											
											<td class="center" style="margin-left: 4px"><div class="infostyle"><?php echo $NumFollowers ?> Abonnés</div></td>
											 
											<td style="width : 10px"> </td>
											
											<td class="center"> <img src="<?php echo $rankimage ?>" style="width: 30px; height: 30px "></td>
											
											<td class="center" style="margin-left: 4px"><div class="infostyle"><?php echo $rankname ?></div></td>
											
											<td style="width : 10px"> </td>
									
									</tr>
							</table>
						</div>
						<div class="col-sm-4">
							<table class="pull-right">
								
										<tr>
											<td> <div class="side-button-style">Suivre</div></td>
										</tr>
										
										<tr>
											<td> <div style="height: 5px"></div></td>
										</tr>
										
										<tr>
											<td><div class="side-button-style">Contacter</div></td>
										</tr>
							
							</table>
						</div>
					  </div>
					
			</li>
		
			

			<?php
			}

	?> 
			</ul></div></div>
			
			<div class="col-md-2">
			</div>
	<?php
	
if($CountRows==0){
?>
<div class="col-box-search">
<div class="search-title"><h3>Votre recherche pour <span class="tt-text">"<?php echo $term;?>"</span> n'a retourné aucun résultats</h3></div>
<ul class="search-again">
<li>Assurez vous d'avoir bien écrit le mot</li>
<li>Essayer avec des mots clés différents</li>
<li>Essayer avec des mots clés générals</li>
</ul>
</div>



<?php 
}