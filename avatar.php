<?php
	include('db.php');
	session_start();
	
	if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteName = $Settings['site_title'];
	$SiteURL  = $Settings['site_link'];
	$Em = $Settings['site_email'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("Il semble y avoir un problème");
}


	$Uname = $_SESSION['username'];
	$Uemail = $_SESSION['email'];

	//if($UserSql = $mysqli->query("SELECT * FROM users WHERE username='$Uname'"))
	if($UserSql = $mysqli->query("SELECT * FROM users WHERE email='$Uemail'"))
	{

		$UserRow = mysqli_fetch_array($UserSql);

		$uid = $UserRow['user_id'];
		
		$avatrimage = $UserRow['avatar'];
		$Firstname = $UserRow['firstname'];
		$Lastname = $UserRow['lastname'];
		$Email = $UserRow['email'];
		
		$UserSql->close();
	}
	else
	{
		 printf("<div class='alert alert-danger alert-pull'>Une erreur est survenue. Veuillez ré-essayer.</div>");
	}
	
	$path = "avatars/";

	$valid_formats = array("jpg", "png", "gif", "jpeg");
	
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
	{
			$name = $_FILES['inputfile']['name'];
			$size = $_FILES['inputfile']['size'];
			
			if(strlen($name))
			{
					list($txt, $ext) = explode(".", $name);
	
					if(in_array($ext,$valid_formats))
					{
							if($size<(1024*1024)) // Image size max 1 MB
							{
								$actual_image_name = time().$uid.".".$ext;
								$tmp = $_FILES['inputfile']['tmp_name'];
	
								if(move_uploaded_file($tmp, $path.$actual_image_name))
								{
									if($mysqli->query("UPDATE users SET avatar='$actual_image_name' WHERE user_id='$uid'"))
									{
										$FromName			 = $SiteName;
										$FromEmail			 = $Em;
										$FromSubject		 = "La photo de profil a été importé !";
										
										$FromMessage = $Firstname.",
												<br/>
												Ta photo de profil a été importé avec succès !
												<br/><br/>
												
												Merci,
												<br/>
												".$FromName;
									
									
										require_once('class.phpmailer.php');

										$mail             = new PHPMailer(); 

										$mail->AddReplyTo($FromEmail, $FromName);

										$mail->SetFrom($FromEmail, $FromName);

										$mail->AddAddress($Email);

										$mail->Subject = $FromSubject;

										$mail->MsgHTML($FromMessage);
									
										if(!$mail->Send()) 
										{

											?>

											<div class="alert alert-danger" role="alert">Erreur d'envoi</div>

										<?php 

										}  
										
										echo json_encode(array('img'=>"<img src='avatars/".$actual_image_name."' class='preview' width='150'/>",'msg'=>"<div class='alert alert-info'>Votre photo a été importé avec succès !</div>"));
										
										return;

										if (!empty($avatrimage)) 
										{
											unlink("avatars/$avatrimage");
										}
									}

								}
								else
									echo json_encode(array('msg'=>"<div class='alert alert-danger'>Une erreur est survenue. Veuillez ré-essayer.</div>"));
									return;

							}
							else
								echo json_encode(array('msg'=>"<div class='alert alert-danger'>La Taille de l'image ne doit pas excéder 1MB</div>"));
								return;

					}
					else
						echo json_encode(array('msg'=>"<div class='alert alert-danger'>Format de l'image non reconnue</div>"));
						return;

			}
			else
				echo json_encode(array('msg'=>"<div class='alert alert-danger'>Veuillez choisir une image</div>"));
				return;
		exit;
	}
?>