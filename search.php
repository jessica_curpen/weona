<?php include("header.php");

$term = $mysqli->escape_string($_GET['term']);

$city = $mysqli->escape_string($_GET['city']);

$category = $mysqli->escape_string($_GET['category']);

$type = $mysqli->escape_string($_GET['type']);

?>

<div class="container container-main" id="display-posts">

			<div class="page-title"><h1>Votre recherche pour "<?php echo $term;?>"</h1></div>

			<script>
			$(document).ready(function()
			{
				$('.star-rates').raty({
					readOnly: true,
				  score: function() 
				  {
					return $(this).attr('data-score');
				  }
			});
			});
			</script>

<?php



if($type == "business")
{
	include("search_business.php");
}?>



<!-- Type == Avis -->
<?php
if($type == "avis")
{
	include("search_products.php");
}?>



<!-- Type == Discussion -->
<?php
if($type == "discussion")
{
	include("search_discussions.php");
}?>



<!-- Type == Professionnel -->
<?php
if($type == "professionnel")
{
	include("search_experts.php");
}?>

</div><!--container-->

<div class="container center">
	<p id="loader" style="display:none">
		<img src="images/loading_spinner.gif" width="50px" ; height="50px" style="margin-bottom : 20px">
	</p>
</div>


<nav id="page-nav"><a href="data_search.php?page=2&amp;term=<?php echo $term;?>&amp;city=<?php echo $city;?>"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#display-posts').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.col-box',     //
		loading: {
          				finishedMsg: 'Fin de la recherche',
          				img: 'templates/<?php echo $Settings['template'];?>/images/loader.gif'
	}
	}, function(newElements, data, url){
		
		$('.star-rates').raty({
		readOnly: true,
  		score: function() {
   		 return $(this).attr('data-score');

  		}
		});
		$('.star-rates').raty('reload');	
	});	

</script>


<?php include("footer.php");?>