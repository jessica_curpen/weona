<?php
include ("db.php");

if($_POST)
{	
	if(!isset($_POST['inputRecovery']) || strlen($_POST['inputRecovery'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger" role="alert">Veuillez donner une adresse mail</div');
	}
	
	$EmailAddress = $_POST['inputRecovery'];
	
	if (filter_var($EmailAddress, FILTER_VALIDATE_EMAIL)) {
  	// The email address is valid
	} else {
  		die('<div class="alert alert-danger" role="alert">Veuillez donner une adresse mail valide</div>');
	}
		
if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteName = $Settings['site_title'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

//Get User Details

if($UserCheck = $mysqli->query("SELECT * FROM users WHERE email='$EmailAddress'")){

   	$GetUserInfo = mysqli_fetch_array($UserCheck);
	
	$UserCount= mysqli_num_rows($UserCheck);

	$UserId = $GetUserInfo["user_id"];

   	$UserCheck->close();
   
	}else{
   
    printf("Il semble y avoir eu un problème");

}

if ($UserCount==1){

$UserName 			 = $GetUserInfo['username'];
$Firstname			 = $GetUserInfo['firstname'];
$Lastname			 = $GetUserInfo['lastname'];
$UserId				 = $GetUserInfo['user_id'];	

$ToContact	 	 	 = $mysqli->escape_string($_POST['inputRecovery']);
$FromName			 = $Settings['site_title'];
$FromEmail			 = $Settings['site_email'];
$FromSubject		 = "Votre nouveau mot de passe ".$SiteName;
$SiteURL			 = $Settings['site_link'];


function rand_string( $length ) {

$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#@!%^&*";
return substr(str_shuffle($chars),0,$length);

}
$NewPassword = rand_string(10);
$EncryptPassword = md5($NewPassword);

$FromMessage = $Firstname.', '.',
<br/>
Votre mot de passe a été ré-initialiser.
<br/><br/>
Nom utilisateur: '.$UserName.'
<br/>
Mot de passe: '.$NewPassword.'
<br/><br/>
Pour changer ce mot de passe, veuillez vous rendre sur la page : http://'.$SiteURL.'/settings.html après votre connexion avec ce mot de passe.
<br/>
<br/>
Merci,
<br/>
'.$FromName;

$FromMessage = utf8_encode($FromMessage);

require_once('class.phpmailer.php');

$mail             = new PHPMailer(); ;

$mail->AddReplyTo($FromEmail, $FromName);

$mail->SetFrom($FromEmail, $FromName);

$mail->AddAddress($ToContact);

$mail->Subject = $FromSubject;

$mail->MsgHTML($FromMessage);

if(!$mail->Send()) {

?>

<div class="alert alert-danger" role="alert">Erreur mail</div>

<?php 

} else {

$mysqli->query("UPDATE users SET password='$EncryptPassword' WHERE user_id='$UserId'") or die (mysqli_error());	
	
?>

<div class="alert alert-success" role="alert">Votre mot de passe a été ré-initialisé. Veuillez vous rendre sur votre mail pour le changer</div>

<?php }

}else{ ?>
	
<div class="alert alert-danger" role="alert">Nous ne trouvons pas votre adresse mail. Veuillez ré-essayer.</div>
	
<?php }

}


?>