<?php include("header.php");
if(!isset($_SESSION['email'])){?>
<script type="text/javascript">
function leave() {
window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php }else{?>
  <div class="container container-main">
  
	<div class="col-md-3">

			<div class="new-disc">
				<a href="submit"> <div class="button-style raised iris">
					  <div class="center" fit>Ajouter un service</div>
					  <paper-ripple fit></paper-ripple>
					</div>
				</a>
			</div>
			
			<!--Tableau de bord Forum-->
			<div class="categories-disc" style="margin-top: 10px">
			<div class="col-shadow">
				<div class="row">
				
						<div class="col-md-12"> <div class="tab-forum"> <a href="my_business"> Mes services </a> </div></div>
						
						<div class="col-md-12">
							<div class="tab-service-notif">
								
							</div>
						</div>	
						<div class="col-md-12"> <div class="tab-forum"> <a href="bookmarks"> Mes signets </a></div></div>
				</div>
			</div>	
			</div>

</div>
  
  
    <div class="col-md-8"> 
      <script type="text/javascript" src="js/jquery.form.js"></script> 
      <script src="js/bootstrap-tagsinput.min.js"></script> 
      <script src="js/bootstrap-filestyle.min.js"></script> 
      <script>
$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours de sauvegarde</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

$(function(){

$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Choisir une photo"});

});
</script>

<?php 

$TimeNow			= time();
$RandNumber   		= rand(0, 9999);
$UniqNumber			= $UserId.$RandNumber.$TimeNow;	

?>

    <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Informations sur le service</h1>
      </div>
      <div class="col-desc">
              <div id="output"></div>
			  
              <form id="SubmitForm" class="forms" action="submit_business.php?id=<?php echo $UniqNumber;?>" enctype="multipart/form-data" method="post">
                <div class="form-group">
                  <label for="inputBizname">Nom du service</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputBizname" id="inputBizname" placeholder="Nom du service">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputDescription">Description</label>
                  <textarea class="form-control" id="inputDescription" name="inputDescription" rows="3" placeholder="Dîtes en plus sur votre service"></textarea>
                </div>
                <div class="form-group">
                  <label for="inputLineOne">Adresse 1</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputLineOne" id="inputLineOne" placeholder="Adresse 1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputLineTwo">Adresse 2</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputLineTwo" id="inputLineTwo" placeholder="Adresse 2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCity">Ville</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCity" name="inputCity">
                      <option value="">Choisir une ville</option>
                      <?php
if($SelectCity = $mysqli->query("SELECT city_id, city FROM city")){

    while($CityRow = mysqli_fetch_array($SelectCity)){
				
?>
                      <option value="<?php echo $CityRow['city'];?>"><?php echo $CityRow['city'];?></option>
                      <?php

}

	$SelectCity->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPhone">Numéro de téléphone</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputPhone" id="inputPhone" placeholder="Numéro de téléphone">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputWeb">Adresse Web</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputWeb" id="inputWeb" placeholder="URL Site Web">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail">Email </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputEmail" id="inputEmail" placeholder="Adresse mail">
                  </div>
                </div>
                
                <div class="form-group">
                  <!--<label for="inputMenu">Menu Web Address</label>-->
                  <!--<div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>-->
                    <input type="hidden" class="form-control" name="inputMenu" id="inputMenu" placeholder="Web Address to Your Menu">
                  <!--</div>-->
                </div>
                
                <div class="form-group">
                  <label for="inputImage">Image</label>
                  <input type="file" name="inputImage" id="inputImage" class="filestyle" data-iconName="glyphicon-picture" data-buttonText="Choisir une Image">
                </div>
                <div class="form-group">
                  <label for="inputCategory">Categorie</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCategory" name="inputCategory">
                      <option value="">Choisir une catégorie</option>
						<?php
	
if($SelDes = $mysqli->query("Select cat_description from categories where parent_id = '0' Group by cat_description Order by cat_description ASC"))
								{
											while($descRow = mysqli_fetch_array($SelDes))
											{
													$description = $descRow["cat_description"];
													
													
													echo '<optgroup label="'.utf8_encode($description).'">';
													
													if($SelectCategories = $mysqli->query("SELECT cat_id, category  FROM categories WHERE cat_description='$description' Order by category ASC"))
													{
															while($categoryRow = mysqli_fetch_array($SelectCategories))
															{
																?>
																<option value="<?php echo $categoryRow['cat_id'];?>"><?php echo utf8_encode($categoryRow['category']);?></option>
																<?php
															}	
															
													}
													else
													{
														printf("There Seems to be an issue");
													}
													
													echo '</optgroup>';
											}
								}
								else
								{
									printf("There Seems to be an issue");
								}
								
						?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputSubcategory">Sous categorie </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputSubcategory" name="inputSubcategory">
                      <option value="">Choisir une sous catégorie</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputFacebook">Adresse Facebook</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputFacebook" id="inputFacebook" placeholder="Web Address to Facebook Page">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTwitter">Adresse Twitter</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputTwitter" id="inputTwitter" placeholder="Web Address to Twitter">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPinterest">Adresse Pinterest</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputPinterest" id="inputPinterest" placeholder="Web Address to Pinterest">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTags">Mots Clés</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" data-role="tagsinput" name="inputTags" id="inputTags">
                  </div>
                </div>
                <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Créer Service</button>
              </form>
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
      <script>
$(document).ready(function(){

    $('#inputCategory').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_subcategory.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) {
                console.log(response);
                $("#inputSubcategory").html(response);
            },
        });
    }); 

});

$(document).ready(function()
{
    $('#SubmitHours').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-1").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output-1',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

$(document).ready(function()
{
$("#SubmitHours #submitButton").prop('disabled', true);
$("#imageform #submitButton").prop('disabled', true);
});

</script>
      <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Horaires d'ouvreture</h1>
      </div>
      <div class="col-desc">
            
            <p class="note">Vous pouvez ajouter des horaires d'ouverture une fois votre service créée.</p>
            
            
            
              <div id="output-1"></div>
              <form id="SubmitHours" class="forms" action="submit_hours.php?id=<?php echo $UniqNumber;?>" method="post">
              <div class="row">
               <div class="input-group-2">
                <div class="col-xs-4">
                  <label for="inputDay">Jour</label>
                  <select class="form-control" id="inputDay" name="inputDay">
                    <option>Lun</option>
                    <option>Mar</option>
                    <option>Mer</option>
                    <option>Jeu</option>
                    <option>Ven</option>
                    <option>Sam</option>
                    <option>Dim</option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <label for="inputFrom">De</label>
                  <select class="form-control" id="inputFrom" name="inputFrom">
                    <option>00.00  </option>
                    <option>00.30  </option>
                    <option>01.00  </option>
                    <option>01.30  </option>
                    <option>02.00  </option>
                    <option>02.30  </option>
                    <option>03.00  </option>
                    <option>03.30  </option>
                    <option>04.00  </option>
                    <option>04.30  </option>
                    <option>05.00  </option>
                    <option>05.30  </option>
                    <option>06.00  </option>
                    <option>06.30  </option>
                    <option>07.00  </option>
                    <option>07.30  </option>
                    <option>08.00  </option>
                    <option>08.30  </option>
                    <option selected="selected">09.00  </option>
                    <option>09.30  </option>
                    <option>10.00  </option>
                    <option>10.30  </option>
                    <option>11.00  </option>
                    <option>11.30  </option>
                    <option>12.00   </option>
                    <option>12.30  </option>
                    <option>13.00  </option>
                    <option>13.30  </option>
                    <option>14.00  </option>
                    <option>14.30  </option>
                    <option>15.00  </option>
                    <option>15.30  </option>
                    <option>16.00  </option>
                    <option>16.30  </option>
                    <option>17.00  </option>
                    <option>17.30  </option>
                    <option>18.00  </option>
                    <option>18.30  </option>
                    <option>19.00  </option>
                    <option>19.30  </option>
                    <option>20.00  </option>
                    <option>20.30  </option>
                    <option>21.00  </option>
                    <option>21.30  </option>
                    <option>22.00  </option>
                    <option>22.30  </option>
                    <option>23.00  </option>
                    <option>23.30  </option>
                  </select>
                </div>
                <div class="col-xs-4">
                  <label for="inputTo">A</label>
                  <select class="form-control" id="inputTo" name="inputTo">
                    <option>00.00  </option>
                    <option>12.30  </option>
                    <option>01.00  </option>
                    <option>01.30  </option>
                    <option>02.00  </option>
                    <option>02.30  </option>
                    <option>03.00  </option>
                    <option>03.30  </option>
                    <option>04.00  </option>
                    <option>04.30  </option>
                    <option>05.00  </option>
                    <option>05.30  </option>
                    <option>06.00  </option>
                    <option>06.30  </option>
                    <option>07.00  </option>
                    <option>07.30  </option>
                    <option>08.00  </option>
                    <option>08.30  </option>
                    <option>09.00  </option>
                    <option>09.30  </option>
                    <option>10.00  </option>
                    <option>10.30  </option>
                    <option>11.00  </option>
                    <option>11.30  </option>
                    <option>12.00  </option>
                    <option>12.30  </option>
                    <option>13.00  </option>
                    <option>13.30  </option>
                    <option>14.00  </option>
                    <option>14.30  </option>
                    <option>15.00  </option>
                    <option>15.30  </option>
                    <option>16.00  </option>
                    <option>16.30  </option>
                    <option selected="selected">17.00  </option>
                    <option>17.30  </option>
                    <option>18.00  </option>
                    <option>18.30  </option>
                    <option>19.00  </option>
                    <option>19.30  </option>
                    <option>20.00  </option>
                    <option>20.30  </option>
                    <option>21.00  </option>
                    <option>21.30  </option>
                    <option>22.00  </option>
                    <option>22.30  </option>
                    <option>23.00  </option>
                    <option>23.30  </option>
                  </select>
                </div>
                </div>
                </div><!--row-->
                 <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Ajouter Horaire</button>
              </form>
               
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
      
<script type="text/javascript">

$(document).ready(function()
{
    $('#imageform').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-gallery").html('<div class="alert alert-info" role="alert">En cours d\'importation.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output-gallery',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

</script>     
   
<div class="col-shadow">
      <div class="biz-title-2">
        <h1>Ajouter plus de photo</h1>
      </div>
      <div class="col-desc">
      
      <p class="note">Vous pouvez ajouter plus de photos une fois le service créée.</p>
            
              <div id="output-gallery"></div>

<form id="imageform" action="upload_gallery.php?id=<?php echo $UniqNumber;?>" enctype="multipart/form-data" method="post">

<div class="form-group">
<label for="inputfile">Choisir les photos</label>
<input type="file" name="photos[]" id="photo-img" class="filestyle" multiple data-iconName="glyphicon-picture" data-buttonText="Choisir les Photos">
</div>

<button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Importer</button>

</form>
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
   
    </div><!--col-md-8-->
    
    
    <div class="col-md-4">
      <?php include("side_bar.php");?>
    </div>
    <!--col-md-4--> 
    
  </div>
  <!--container-->
  
<?php } include("footer.php");?>