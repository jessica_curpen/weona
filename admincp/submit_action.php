<?php

include('../db.php');

if($_POST)
{	

		
	if(!isset($_POST['inputAction']) || strlen($_POST['inputAction'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter desired action.</div>');
	}
	
	if(!isset($_POST['inputDescription']) || strlen($_POST['inputDescription'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter description for your new action.</div>');
	}
	
	if(!isset($_POST['inputSum']) || strlen($_POST['inputSum'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the sum for your new action.</div>');
	}
	
	if(!isset($_POST['inputCoins']) || strlen($_POST['inputCoins'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the amount of coins gained for this action</div>');
	}
	
	
	$Ranking			= $mysqli->escape_string($_POST['inputAction']);
	
	$RankingDescription	= $mysqli->escape_string($_POST['inputDescription']);
	
	$RankingSum			= $mysqli->escape_string($_POST['inputSum']);
	
	$RankingCoins			= $mysqli->escape_string($_POST['inputCoins']);
	
	$mysqli->query("INSERT INTO actions(action, description, points, coins) VALUES ('$Ranking', '$RankingDescription', '$RankingSum', '$RankingCoins')");
	
	
		die('<div class="alert alert-success" role="alert">New action added successfully.</div>');

		
   }
   else
   {
   	
		die('<div class="alert alert-danger" role="alert">There seems to be a problem. please try again.</div>');
  
	}


?>