<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Admin CP</li>
  <li>Actions</li>
  <li>Manage actions</li>
  <li class="active">Edit actions</li>
</ol>

<div class="page-header">
  <h3>Edit Ranking <small>Edit website actions</small></h3>
</div>

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#categoryForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Submitting.. Please wait..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>

<section class="col-md-8">

<div class="panel panel-default">

    <div class="panel-body">
    
<?php

$id = $mysqli->escape_string($_GET['id']); 

if($Ranking = $mysqli->query("SELECT * FROM actions where idAction='$id'")){

    $RankingRow = mysqli_fetch_array($Ranking);
	
    $Ranking->close();
	
}else{
    
	 printf("There Seems to be an issue");
}


?>    

<div id="output"></div>

<form id="categoryForm" action="update_action.php?id=<?php echo $id;?>" method="post">

<div class="form-group">
        <label for="inputTitle">Action</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputTitle" readonly name="inputTitle" class="form-control" placeholder="Action Name" value="<?php echo $RankingRow['action'];?>">
    </div>
</div>


<div class="form-group">
<label for="inputDescription">Description</label>
<textarea class="form-control" id="inputDescription" name="inputDescription" rows="3" placeholder="Edit description of your ranking"><?php echo $RankingRow['description'];?></textarea>
</div>

<div class="form-group">
<label for="inputSum">Points </label>
<textarea class="form-control" id="inputSum" name="inputSum" rows="3" placeholder="Edit sum of your ranking"><?php echo $RankingRow['points'];?></textarea>
</div>

<div class="form-group">
    <label for="inputTitle">Coins</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputCoins" name="inputCoins" class="form-control" placeholder="Enter your the amount of coins gained for this action" value="<?php echo $RankingRow['coins'];?>">
    </div>
</div>

</div><!-- panel body -->

<div class="panel-footer clearfix">

<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Update Action</button>

</div><!--panel-footer clearfix-->

</form>


</div><!--panel panel-default-->  

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>