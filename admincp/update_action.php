<?php
session_start();

include('../db.php');

if($_POST)
{	

	$id = $mysqli->escape_string($_GET['id']);

		
	if(!isset($_POST['inputTitle']) || strlen($_POST['inputTitle'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter desired action.</div>');
	}
	
	if(!isset($_POST['inputDescription']) || strlen($_POST['inputDescription'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter description for your action.</div>');
	}
	
	if(!isset($_POST['inputSum']))
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter sum for action.</div>');
	}
	
	if(!isset($_POST['inputCoins']) || strlen($_POST['inputCoins'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the amount of coins gained for this action</div>');
	}
	
	
	$RankingTitle			= $mysqli->escape_string($_POST['inputTitle']);
	
	$RankingDescription	    = $mysqli->escape_string($_POST['inputDescription']);
	
	$RankingSum	            = $mysqli->escape_string($_POST['inputSum']);
	
	$RankingCoins			= $mysqli->escape_string($_POST['inputCoins']);
	
	
	$mysqli->query("UPDATE actions SET action='$RankingTitle', description='$RankingDescription', points='$RankingSum', coins='$RankingCoins' WHERE idAction='$id'");
	
	
	die('<div class="alert alert-success" role="alert">Action updated successfully.</div>');

		
   }else{
   	
		die('<div class="alert alert-danger" role="alert">There seems to be a problem. please try again.</div>');
  
}


?>