<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Admin CP</li>
  <li>Ranking</li>
  <li>Manage ranking</li>
  <li class="active">Edit Ranking</li>
</ol>

<div class="page-header">
  <h3>Edit Ranking <small>Edit website ranking</small></h3>
</div>

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#categoryForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Submitting.. Please wait..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>

<section class="col-md-8">

<div class="panel panel-default">

    <div class="panel-body">
    
<?php

$id = $mysqli->escape_string($_GET['id']); 

if($Ranking = $mysqli->query("SELECT * FROM ranking where idRanking='$id'")){

    $RankingRow = mysqli_fetch_array($Ranking);
	
    $Ranking->close();
	
}else{
    
	 printf("There Seems to be an issue");
}


?>    

<div id="output"></div>

<form id="categoryForm" action="update_ranking.php?id=<?php echo $id;?>" method="post">

<div class="form-group">
        <label for="inputTitle">Ranking</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputTitle" name="inputTitle" class="form-control" placeholder="Edit ranking" value="<?php echo $RankingRow['rank'];?>">
    </div>
</div>


<div class="form-group">
<label for="inputDescription">Ranking Description</label>
<textarea class="form-control" id="inputDescription" name="inputDescription" rows="3" placeholder="Edit description of your ranking"><?php echo $RankingRow['description'];?></textarea>
</div>

<div class="form-group">
        <label for="inputTitle">Min Points</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputRankingSumMin" name="inputRankingSumMin" class="form-control" placeholder="Enter the minimum points required for this rank" value = "<?php echo $RankingRow['min_points'];?>">
    </div>
</div>

<div class="form-group">
        <label for="inputTitle">Max Points</label>
    <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon fa  fa-info"></span></span>
      <input type="text" id="inputRankingSumMax" name="inputRankingSumMax" class="form-control" placeholder="Enter the maximum points required for this rank" value = "<?php echo $RankingRow['max_points'];?>" >
    </div>
</div>

</div><!-- panel body -->

<div class="panel-footer clearfix">

<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Update Ranking</button>

</div><!--panel-footer clearfix-->

</form>


</div><!--panel panel-default-->  

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>