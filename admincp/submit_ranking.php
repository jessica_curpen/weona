<?php

include('../db.php');

if($_POST)
{	

		
	if(!isset($_POST['inputRanking']) || strlen($_POST['inputRanking'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter desired ranking.</div>');
	}
	
	if(!isset($_POST['inputDescription']) || strlen($_POST['inputDescription'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter description for your new ranking.</div>');
	}
	
	if(!isset($_POST['inputRankingSumMin']) || strlen($_POST['inputRankingSumMin'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the minimum points required for this ranking.</div>');
	}
	
	if(!isset($_POST['inputRankingSumMax']) || strlen($_POST['inputRankingSumMax'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the maximum points required for this ranking.</div>');
	}
	
	
	$Ranking			= $mysqli->escape_string($_POST['inputRanking']);
	
	$RankingDescription	= $mysqli->escape_string($_POST['inputDescription']);
	
	$RankingSumMin		= $mysqli->escape_string($_POST['inputRankingSumMin']);
	
	$RankingSumMax			= $mysqli->escape_string($_POST['inputRankingSumMax']);
	
	
	
	$mysqli->query("INSERT INTO ranking(rank, description, min_points, max_points) VALUES ('$Ranking', '$RankingDescription', '$RankingSumMin', '$RankingSumMax')");
	
	
		die('<div class="alert alert-success" role="alert">New ranking added successfully.</div>');

		
   }
   else
   {
   	
		die('<div class="alert alert-danger" role="alert">There seems to be a problem. please try again.</div>');
  
	}


?>