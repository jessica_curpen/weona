<?php
session_start();

include('../db.php');

if($_POST)
{	

	$id = $mysqli->escape_string($_GET['id']);

		
	if(!isset($_POST['inputTitle']) || strlen($_POST['inputTitle'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter desired ranking.</div>');
	}
	
	if(!isset($_POST['inputDescription']) || strlen($_POST['inputDescription'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter description for your ranking.</div>');
	}
	
	if(!isset($_POST['inputRankingSumMin'])|| strlen($_POST['inputRankingSumMin'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the minimum points required for this ranking.</div>');
	}
	
	if(!isset($_POST['inputRankingSumMax'])|| strlen($_POST['inputRankingSumMax'])<1)
	{
		//required variables are empty
		die('<div class="alert alert-danger">Please enter the maximum points required for this ranking.</div>');
	}
	
	
	$RankingTitle			= $mysqli->escape_string($_POST['inputTitle']);
	
	$RankingDescription	= $mysqli->escape_string($_POST['inputDescription']);
	
	$RankingSumMin	= $mysqli->escape_string($_POST['inputRankingSumMin']);
	
	$RankingSumMax	= $mysqli->escape_string($_POST['inputRankingSumMax']);
	
	
	$mysqli->query("UPDATE ranking SET rank='$RankingTitle', description='$RankingDescription', min_points='$RankingSumMin', max_points='$RankingSumMax' WHERE idRanking='$id'");
	
	
	die('<div class="alert alert-success" role="alert">Ranking updated successfully.</div>');

		
   }else{
   	
		die('<div class="alert alert-danger" role="alert">There seems to be a problem. please try again.</div>');
  
}


?>