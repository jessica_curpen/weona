<nav id="left-menu">

	<ul>
	
				<li><a class="fa fa-dashboard" href="index.php"><span>Dashboard</span></a></li>
		 
				<li class="has-sub"><a class="fa fa-align-left" href="#"><span>Settings</span></a>
						<ul>
							 <li><a href="settings.php"><span>Site Settings</span></a></li>
							 <li><a href="home_settings.php"><span>Home Page Settings</span></a></li>
						</ul>
				</li>
				
	   
			   <li class="has-sub"><a class="fa fa-th-large" href="#"><span>Categories</span></a>
			   <ul>
					 <li><a href="new_category.php"><span>Add New Category</span></a></li>
					 <li><a href="manage_categories.php"><span>Manage Categories</span></a></li>
					 <li><a href="new_subcategory.php"><span>Add New Subcategory</span></a></li>
					 <li><a href="manage_subcategories.php"><span>Manage Subcategories</span></a></li>
				  </ul>
			   </li>
			   
			   
			   <li class="has-sub"><a class="fa fa-th-large" href="#"><span>Actions</span></a>
			   <ul>
					 <li><a href="new_action.php"><span>Add New Action</span></a></li>
					 <li><a href="manage_actions.php"><span>Manage Actions</span></a></li>
				  </ul>
			   </li>
			   
			   
				<li class="has-sub"><a class="fa fa-th-large" href="#"><span>Cities</span></a>
			   <ul>
					 <li><a href="new_city.php"><span>Add New City</span></a></li>
					 <li><a href="manage_cities.php"><span>Manage Cities</span></a></li>
				  </ul>
			   </li>
			   
			   
			   <li class="has-sub"><a class="fa fa-align-left" href="#"><span>Ranking</span></a>
			   <ul>
					 <li><a href="new_ranking.php"><span>Add Ranking</span></a></li>
					 <li><a href="manage_ranking.php"><span>Manage Ranking</span></a></li>
				  </ul>
			   </li>
			   
			   
			   <li class="has-sub"><a class="fa fa-align-left" href="#"><span>Business Listings</span></a>
			   <ul>	
					 <li><a href="approved_businesses.php"><span>Approved Businesses</span></a></li>
					 <li><a href="pending_businesses.php"><span>Pending Businesses</span></a></li>
				  </ul>
			   </li>
			   
			   
				<li class="has-sub"><a class="fa fa-align-left" href="#"><span>Reviews</span></a>
			   <ul>	
					 <li><a href="approved_reviews.php"><span>Approved Reviews</span></a></li>
					 <li><a href="pending_reviews.php"><span>Pending Reviews</span></a></li>
				  </ul>
			   </li>
			   
			   
			   <li><a class="fa fa-users" href="users.php"><span>Users</span></a></li>
			   
			   <li><a class="fa fa-user" href="administrator.php"><span>Administrator</span></a></li>
			   
			   <li><a class="fa fa-indent" href="ads.php"><span>Advertisements</span></a></li>
				 <li class="has-sub"><a class="fa fa-align-left" href="#"><span>Manage Pages</span></a>
					<ul>
					 <li><a href="about.php"><span>About Page</span></a></li>
					 <li><a href="privacy_policy.php"><span>Privacy Policy Page</span></a>
					 <li><a href="tnc.php"><span>Terms of Use Page</span></a>
					 <li><a href="advertise.php"><span>Advertise Page</span></a>
				  </ul>
			   </li>
			   
			   <li class="last"><a class="fa fa-book" href="logs.php" target="_blank"><span>Logs</span></a></li>
			   
			   <li class="last"><a class="fa fa-eye" href="../" target="_blank"><span>View Site</span></a></li>
			   
	</ul>
	
</nav>