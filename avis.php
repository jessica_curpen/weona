<?php include("header_avis.php");


$Tags  = stripslashes($BizRow['tags']);
$BizUid  = stripslashes($BizRow['user_id']);

$BizLink = preg_replace("![^a-z0-9]+!i", "-", $BizName);
$bizLink = urlencode(strtolower($BizName));



	

?>

<script src="js/jquery.form.js"></script>
<script>
$(function(){
$('#rate-biz').raty({readOnly: true, score:<?php echo $avg;?>});
});

$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
		$("html, body").animate({ scrollTop: 0 }, "fast");
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi..Veuillez patienter</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}


</script>

<div class="container container-biz">
  <div class="col-md-8">
    <div class="col-shadow">
      <div class="biz-title">
        <h1><?php echo $BizRow['produit_name'];?></h1>
         </div>
      <!--biz-title--> 
      <img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $BizRow['featured_image'];?>&amp;h=400&amp;w=900&amp;q=100" alt="<?php echo $BizRow['produit_name'];?>">
      <input type="hidden"  name="idProd" value ="<?php echo $id?>">
      
        <div class="col-desc">

        
				<?php 
				
					if(!empty($Tags))
					{?>
							<div class="info-biz-row">
								<span class="fa fa-tag"></span> Mots Clés:
								<?php
										$arr=explode(",",$Tags);
										foreach ($arr as $TagValue) 
										{
											$ShowTags = preg_replace("![^a-z0-9]+!i", "", $TagValue);
											$ShowTags = strtolower($ShowTags);
											echo '<a href="tags-'.$ShowTags.'">'.$TagValue.'</a>&nbsp;';
										}
								?>
							</div>
					 <?php 
					}?>
					
				<div class="info-biz-row">
					<span class="fa fa-chevron-circle-right"></span> Categorie: <a href="category-<?php echo $BizRow['cid'];?>-<?php echo $CLink;?>"><?php echo $CName;?></a>
					<?php
						if($SId>0)
						{
			
							if($SubCat = $mysqli->query("SELECT * FROM categories WHERE cat_id='$SId'"))
							{

								$SubCatRow = mysqli_fetch_array($SubCat);
								
								$SCName = $SubCatRow['category'];
								$SCLink = preg_replace("![^a-z0-9]+!i", "-", $SCName);
								$SCLink = urlencode($SCLink);
								$SCLink = strtolower($SCLink);	
					?>
								/ <a href="subcategory-<?php echo $SubCatRow['cat_id'];?>-<?php echo $SCLink;?>"><?php echo $SCName;?></a>
					<?php
		
								$SubCat->close();
	
							}
							else
							{
								printf("Il semble y avoir un problème");
							}	
			
						}
		
					?>
				</div>
		
		
				<div class="info-biz-row">
					
					<span class="fa fa-pencil"></span> Auteur :
					<?php
			
						if($idAuthor == $UserId)
						{
						?>
								<a href="user_profile-<?php echo $UserId; ?>-<?php echo $LoggedUserLink; ?>">
								Vous
								</a>avez écrit cet avis<?php
						}
						else
						{	
			
								if($author = $mysqli->query("SELECT * FROM users WHERE user_id='$idAuthor'"))
								{
										$authorInfo = mysqli_fetch_array($author);
										
										$firstname = $authorInfo["firstname"];
										$lastname  = $authorInfo["lastname"] ;
										$username  = $authorInfo["username"] ;
								
										$author -> close();
								
								?>
									<a href="user_profile-<?php echo $idAuthor; ?>-<?php echo $username; ?>">
										<?php echo $username;?>
									</a>
								<?php
								}
								else
								{
									printf("Il semble y avoir un problème");
								}
						}
					?>
					
				</div>
		</div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
    <div class="col-shadow">
      <div class="col-desc">
        <h2>Avis</h2>
		
		<div id="rate-biz" class="pull-prod-info"></div>
        
		<div><p><?php echo nl2br($BizRow['avis']);?></p></div>
		
		
		<div class="row">
		
			<div class="col-md-12">
				<div class="avis-likes"> 
					<?php  get_likes($id, $mysqli) ?>
				</div>
			</div>
			
			
			
			<div class="col-md-12">
		
		<?php
		if(isset($_SESSION['username']))
		{
				if($idAuthor == $UserId)
				{
					/*?>	
						<div class="col-desc">
							<div class="user-post"> 
								Vous avez posté cet avis
							</div>
					  </div>
					<?php*/
				}
				else
				{
					
						$sqllike = $mysqli->query("Select * from likes_produits where idProduit = '$id' And idUser = '$UserId'");
						$liked = $sqllike->num_rows;
						
						if($liked == 0)
						{?>
							<div class="usefullnessII"> 
									Cet avis est utile
									
									<input type="text" class="hidden-cat" name="idSUser" value ="<?php echo $UserId?>">
									<input type="text" class="hidden-cat" name="idAut" value ="<?php echo $idAuthor?>">
							</div>
								
							<div class="hide" id="liked">
									<div class="useful">Utile</div>
							</div><?php
						}
						else
						{
							?>
							
								<div class="useful">Utile</div>
							
						  <?php
						}
				}
		}
		?>
		
			</div>
		</div>
      </div>
      <!--col-desc--> 

    </div>
    <!--col-shadow-->
	
	
	
	<!-- if the review has comments -->
	<?php
	
		if(isset($_SESSION['username']))
		{
			if($respose = $mysqli->query("SELECT * FROM avis_participation WHERE id_avis='$id' Order by date ASC"))
			{
				$num_responses = $respose->num_rows;
				
				if($num_responses > 0)
				{
						?>
						<div class="col-shadow">
							<div class="col-desc">
							<h2>Commentaires</h2>
					<?php
					
					while($ResRow = mysqli_fetch_array($respose))
					{
						$responseUSerID =  $ResRow["user_id"] ;
						$comment =  $ResRow["comment"] ;
						
						
						
								if($resUSER = $mysqli->query("SELECT * FROM users WHERE user_id='$responseUSerID'"))
								{
									$getRU = mysqli_fetch_array($resUSER);
									
									$resUname 	= $getRU["username"];
									$resUid		= $getRU["user_id"];
									$resUavatar	= $getRU["avatar"];
									
									$resUsername = strtolower($getRU['username']);
		
									$resUserLink   = preg_replace("![^a-z0-9]+!i", "-", $resUsername);
									
									$resUserLink	  = strtolower($resUserLink);
									
									if (empty($resUavatar))
									{ 
										$ProfilePicU =  'http://'.$SiteLink.'/templates/'.$Settings['template'].'/images/avatar.jpg';
									}
									elseif (!empty($resUavatar))
									{
										$ProfilePicU =  'http://'.$SiteLink.'/avatars/'.$resUavatar;
									}
									?>
									<div class="container">
									
										<div class="row no-gutter" style="margin-top: 10px ; margin-bottom : 10px ; border-bottom : 1px solid #F2F1EF">
												
												<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" >
													
													<div class="img-profile">
															<img src="thumbs.php?src=<?php echo $ProfilePicU;?>&amp;h=50&amp;w=50&amp;q=100" alt="<?php echo ucfirst($resUname);?>" class="img-circle">
													</div><!--img-profile-->
												</div>
												
												<div class="col-xs-12 col-sm-12 col-md-11 col-lg-11" style="margin-top: 16px" >
												
													<div>
													
														<a href="user_profile-<?php echo $resUid; ?>-<?php echo $resUserLink; ?>"><?php echo $resUname?> </a> <?php echo $comment?>
														
													</div>
													
												</div>
										
										</div>
									
									</div>
									<?php
								}
								
								
						
					}
					?>
						</div>
					</div>
					<?php
			}
			
		}}
			
	?>
	
	<!-- end -->
	<?php if(isset($_SESSION['username']))
		{ ?>
	<div class="col-shadow">
      <div class="col-desc">
        
		<h2>Répondre</h2>
		
		<div class="form-group">
		  <label for="comment">Commenter</label>
		  <textarea class="form-control" rows="5" id="commentArea" name="commentArea"></textarea>
		</div>
		
		<button id="comment" class="btn btn-danger btn-lg btn-block"> Poster</button>
		
      </div>
	  
      <!--col-desc--> 

		</div><?php }?>
    <!--col-shadow-->
	
	
    <?php if(!empty($Ad2)){?>
    <div class="col-shadow col-ads"> <?php echo $Ad2;?> </div>
    <!--col-shadow-->
    <?php }?>
    <div id="output"></div>
    
    <script>


$(document).ready(function()
{
		$('.star-rates').raty({
			readOnly: true,
			score: function() 
			{
				return $(this).attr('data-score');
			}
		});
});
</script>
 
    
  </div>
  <!--col-md-8-->
  

  
</div>
<!--container-biz--> 

<?php include("footer.php");?>