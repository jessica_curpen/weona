<?php include("header.php");

if(!isset($_SESSION['email'])){?>
<script type="text/javascript">
function leave() {
window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php }else{?>

<div class="container container-main">

			<div class="col-md-3"> 
					
					<div class="new-disc">
						<a href="produit"> <div class="button-style raised iris">
							  <div class="center" fit>Poster un avis</div>
							  <paper-ripple fit></paper-ripple>
							</div>
						</a>
					</div>
					
					<!--Tableau de bord Forum-->
					<div class="categories-disc" style="margin-top: 10px">
						<div class="col-shadow">
							<div class="row">
							
									<div class="col-md-12"> <div class="tab-forum"> <a href="my_reviews"> Mes avis </a> </div></div>
									
									<div class="col-md-12">
										<div class="tab-avis-notif">
											
										</div>
										
										<div class="tab-avis-notif2">
											
										</div>
									</div>	
									
							</div>
						</div>	
					</div>
				
				</div>
		


<div class="col-md-8">

			<script type="text/javascript" src="js/jquery.form.js"></script>

			<div class="col-shadow">
			
				<div class="biz-title-2">
					<h1>Gérer vos Avis</h1>
				</div>
				
				<div class="col-desc" id="display-posts">

					<script>
						$(document).ready(function()
						{
							$('.biz-rate').raty
								({
									readOnly: true,
									score: function() 
									{
										return $(this).attr('data-score');

									}
								});
						});
						
						
						$(document).ready(function()
						{
									//Delete	
									$('a.btnDelete').on('click', function (e) 
									{
										e.preventDefault();
										var id = $(this).closest('div').data('id');
										$('#myModal').data('id', id).modal('show');
									});

									$('#btnDelteYes').click(function () 
									{
										var id = $('#myModal').data('id');
										var dataString = 'id='+ id ;
										$('[data-id=' + id + ']').parent().parent().remove();
										$('#myModal').modal('hide');
										
										//ajax
										$.ajax
										({
												type: "POST",
												url: "delete_product.php",
												data: dataString,
												cache: false,
												success: function(html)
												{
													//$(".fav-count").html(html);
													$("#output").html(html);
												}
										});
										//ajax ends
									});
						});
					</script>

					<div id="output"></div>

<?php

if($PostSql = $mysqli->query("SELECT * FROM produits WHERE user_id = '$UserId' ORDER BY date DESC LIMIT 0, 12"))
{

		$CountRows = mysqli_num_rows($PostSql);	

		while ($PostRow = mysqli_fetch_array($PostSql))
		{
	
				$longTitle = stripslashes($PostRow['produit_name']);
	
				$PostLink = preg_replace("![^a-z0-9]+!i", "-", $longTitle);
				$PostLink = urlencode(strtolower($PostLink));
	
				$longDescription = stripslashes($PostRow['avis']);
				$strDescription = strlen ($longDescription);
				
				if ($strDescription > 70) 
				{
					$Description = substr($longDescription,0,67).'...';
				}
				else
				{
					$Description = $longDescription;
				}
	

?>
	
<div class="img-thumbs">

    <div class="right-caption span4">
      <img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $PostRow['featured_image'];?>&amp;h=110&amp;w=140&amp;q=100" alt="<?php echo $longTitle;?>">
      <div class="col-caption" data-id="<?php echo $PostRow['idProduit'];?>">
        <a href="avis-<?php echo $PostRow['idProduit'];?>-<?php echo $PostLink;?>"><h2><?php echo $longTitle;?></h2></a>
		
        <p class="font-small">
			<span class="biz-rate" data-score="<?php echo stripslashes($PostRow['avg']);?>"></span> 
			
			<?php echo stripslashes($PostRow['hits']);?> Vus
		</p>
		
        <p><?php echo $Description;?></p>
        <p>
        
        <a class="edit-links btnDelete" href="delete_product-<?php echo $PostRow['idProduit'];?>"><span class="fa fa-remove"></span> Supprimer votre avis</a>
        </p>
      </div>
    </div>
 </div>
 
<?php     
	}
$PostSql->close();
}else{
     printf("There Seems to be an issue");
}
if($CountRows==0){
?>
<div class="col-note">Vous n'avez écrit aucun avis.</div>
<?php }?>

  </div>
      <!--col-desc--> 
      
      </div>
    <!--col-shadow-->
      
<nav id="page-nav"><a href="data_my_business.php?page=2"></a></nav>

<script src="js/jquery.infinitescroll.min.js"></script>
	<script src="js/manual-trigger.js"></script>
	
	<script>
	
	
	$('#display-posts').infinitescroll({
		navSelector  : '#page-nav',    // selector for the paged navigation 
      	nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      	itemSelector : '.img-thumbs',     //
		loading: {
          				finishedMsg: 'Fin de la liste',
          				img: 'templates/<?php echo $Settings['template'];?>/images/loader.gif'
	}
	}, function(newElements, data, url){
		
		$('.biz-rate').raty({
	   readOnly: true,
       score: function() {
       return $(this).attr('data-score');

       }
		});
		$('.biz-rate').raty('reload');	
	});	

</script>      
      
<?php if(!empty($Ad2)){?>
<div class="col-shadow col-ads">
<?php echo $Ad2;?>
</div><!--col-shadow-->
<?php } ?>      

</div><!--col-md-8-->

<div class="col-md-4">
<?php include("side_bar.php");?>
</div><!--col-md-4-->

</div><!--container-->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Confirmation</h4>

            </div>
            <div class="modal-body">
				<p>Voulez vous supprimer cet avis ?</p>
                <p class="text-warning"><small>Vous perdrez tous les posts liés à cet avis</small></p>		
            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDelteYes">Supprimer</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php } include("footer.php");?>