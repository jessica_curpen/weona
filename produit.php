<?php 
	include("header.php");
	
	if(!isset($_SESSION['email']))
	{?>
				<script type="text/javascript">
						function leave() 
						{
								window.location = "login";
						}
						setTimeout("leave()", 2);
				</script>
	<?php 
	}
	else
	{?>
		<div class="container container-main">
		
				<div class="col-md-3"> 
					
					<div class="new-disc">
						<a href="produit"> <div class="button-style raised iris">
							  <div class="center" fit>Poster un avis</div>
							  <paper-ripple fit></paper-ripple>
							</div>
						</a>
					</div>
					
					<!--Tableau de bord Forum-->
					<div class="categories-disc" style="margin-top: 10px">
						<div class="col-shadow">
							<div class="row">
							
									<div class="col-md-12"> <div class="tab-forum"> <a href="my_reviews.php"> Mes avis </a> </div></div>
									
									<div class="col-md-12">
										<div class="tab-avis-notif">
											
										</div>
										
										<div class="tab-avis-notif2">
											
										</div>
									</div>	
									
							</div>
						</div>	
					</div>
				
				</div>
		
		
		
				<div class="col-md-8"> 
						<script type="text/javascript" src="js/jquery.form.js"></script> 
						<script src="js/bootstrap-tagsinput.min.js"></script> 
						<script src="js/bootstrap-filestyle.min.js"></script> 
						<script>
										$(document).ready(function()
										{
											$('#SubmitForm').on('submit', function(e)
											{
												e.preventDefault();
												$('#submitButton').attr('disabled', ''); // disable upload button
												//show uploading message
												$("#output").html('<div class="alert alert-info" role="alert">Sauvegarde en cours...</div>');
												$('html, body').animate({scrollTop: '0px'}, 300);
												$(this).ajaxSubmit
												({
													target: '#output',
													success:  afterSuccess //call function after success
												});
											});
										});
		 
										function afterSuccess()
										{	
											 
											$('#submitButton').removeAttr('disabled'); //enable submit button
										   
										}

										$(function()
										{

											$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Choisir une photo"});

										});
						</script>

	<?php 

	$TimeNow			= time();
	$RandNumber   		= rand(0, 9999);
	$UniqNumber			= $UserId.$RandNumber.$TimeNow;	

	?>

    <div class="col-shadow">
	
      <div class="biz-title-2">
        <h1>Donner votre avis sur un produit</h1>
      </div>
	  
      <div class="col-desc">
              <div id="output"></div>
			  
              <form id="SubmitForm" class="forms" action="submit_product.php?id=<?php echo $UniqNumber;?>" enctype="multipart/form-data" method="post">
			  
                <div class="form-group">
                  <label for="inputProdname">Nom du Produit</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputProdname" id="inputProdname" placeholder="Nom de produit">
                  </div>
                </div>
				
					
                <div class="form-group">
                  <label for="inputImage">Ajouter une image</label>
                  <input type="file" name="inputImage" id="inputImage" class="filestyle" data-iconName="glyphicon-picture" data-buttonText="Choisir une image">
                </div>
		
                <div class="form-group">
                  <label for="inputCategory">Categorie</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCategory" name="inputCategory">
                      <option value="">Choisir une catégorie</option>
                      
					  
					  <?php
								
								if($SelDes = $mysqli->query("Select cat_description from categories where parent_id = '0' Group by cat_description Order by cat_description ASC"))
								{
											while($descRow = mysqli_fetch_array($SelDes))
											{
													$description = $descRow["cat_description"];
													
													
													echo '<optgroup label="'.utf8_encode($description).'">';
													
													if($SelectCategories = $mysqli->query("SELECT cat_id, category  FROM categories WHERE cat_description='$description' Order by category ASC"))
													{
															while($categoryRow = mysqli_fetch_array($SelectCategories))
															{
																?>
																<option value="<?php echo $categoryRow['cat_id'];?>"><?php echo utf8_encode($categoryRow['category']);?></option>
																<?php
															}	
															
													}
													else
													{
														printf("Il semble y avoir eu un problème");
													}
													
													echo '</optgroup>';
											}
								}
								else
								{
									printf("Il semble y avoir eu un problème");
								}
								
						?>
					  
					  
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputSubcategory">Sous-catégorie (optionnelle)</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputSubcategory" name="inputSubcategory">
                      <option value="">Choisir une sous-catégorie</option>
                    </select>
                  </div>
                </div>
                
                
                
                <div class="form-group">
                  <label for="inputTags">Mots clés</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" data-role="tagsinput" name="inputTags" id="inputTags">
                  </div>
                </div>
                
              
 
  
	<div class="col-shadow" id="col-review-box">
      <div class="biz-title-2">
        <h1>Dîtes-nous ce que vous pensez de ce produit</h1>
      </div>
      <div class="col-desc">
        <?php if(isset($_SESSION['username'])){

		$TimeNow			= time();
		$RandNumber   		= rand(0, 9999);
		$UniqNumber			= $UserId.$RandNumber.$TimeNow;		
			
		?>
<script>
$(function() 
{
		$('#rate-biz-2').raty({
		score: 0,
		click: function(score, evt) 
				{
					$('#score').val(score);	
					$('#rate-msg').html("Votre note est de " + score);
					$('#rate-msg').fadeOut(10000);
				}
			});
			
	
});
</script>
        
          <div class="form-group">
            <label for="star-rate">Note</label>
            <div id="rate-biz-2" class="star-rate"></div>
            <span id="rate-msg"></span> 
			<input type="hidden" class="form-control" name="score" id="score">
		  </div>
          <div class="form-group">
            <label for="inputReview">Votre Avis</label>
            <textarea class="form-control" id="inputReview" name="inputReview" placeholder="Votre avis est important si vous avez utilsé ce produit. Dîtes à la communité ce que vous en pensez."></textarea>
          </div>
          <input type="hidden" class="form-control" name="inputUniq" id="inputUniq" value="<?php echo $UniqNumber;?>">
          <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Postez votre avis</button>
        </form>
        <?php }else{?>
        <div class="col-note">Veuillez vous <a href="login">connecter</a> ou vous <a href="register">enregister</a> pour écrire un avis</div>
        <?php }?>
      </div>
      <!--col-desc--> 
    </div>
  
    </div>
    <!--col-shadow-->
      <script>
$(document).ready(function(){

    $('#inputCategory').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_subcategory.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) {
                console.log(response);
                $("#inputSubcategory").html(response);
            },
        });
    }); 

});



</script>
      
      
<script type="text/javascript">

$(document).ready(function()
{
    $('#imageform').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output-gallery").html('<div class="alert alert-info" role="alert">En cours d\'importation.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output-gallery',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

</script>     

   
    </div><!--col-md-8-->
    
    
    <div class="col-md-4">
      <?php include("side_bar.php");?>
    </div>
    <!--col-md-4--> 
    
  </div>
  <!--container-->
  
  <script>

<?php } include("footer.php");?>