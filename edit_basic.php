<?php include("header.php");
if(!isset($_SESSION['email'])){?>
<script type="text/javascript">
function leave() {
window.location = "login";
}
setTimeout("leave()", 2);
</script>
<?php }else{?>
  <div class="container container-main">
    <div class="col-md-8"> 
      <script type="text/javascript" src="js/jquery.form.js"></script> 
      <script src="js/bootstrap-tagsinput.min.js"></script> 
      <script src="js/bootstrap-filestyle.min.js"></script> 
      <script>
$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

$(function(){

$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Choisir une photo"});

});

$(document).ready(function(){

    $('#inputCategory').on("change",function () {
        var categoryId = $(this).find('option:selected').val();
        $.ajax({
            url: "update_subcategory.php",
            type: "POST",
            data: "categoryId="+categoryId,
            success: function (response) {
                console.log(response);
                $("#inputSubcategory").html(response);
            },
        });
    }); 

});
</script>

<?php 

$id = $mysqli->escape_string($_GET['id']);


if($Biz = $mysqli->query("SELECT * FROM business WHERE biz_id='$id'")){
	
	$BizRow = mysqli_fetch_array($Biz);
	
	$City = stripslashes($BizRow['city']);
	
	$CatId = stripslashes($BizRow['cid']);
	
	$SubCat = stripslashes($BizRow['sid']);
	
	$Biz->close();
	
}else{
    
	 printf("There Seems to be an issue");
}


if($SelectedCat = $mysqli->query("SELECT cat_id, category FROM categories WHERE cat_id='$CatId'")){

    $SelectedRow = mysqli_fetch_array($SelectedCat);	

	$SelectedCat->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

if($SelectedSubCat = $mysqli->query("SELECT cat_id, category FROM categories WHERE cat_id='$SubCat'")){

    $SelectedSubRow = mysqli_fetch_array($SelectedSubCat);
	
	$GetSubId = $SelectedSubRow['cat_id'];
	
	$GetSubName = $SelectedSubRow['category'];	

	$SelectedSubCat->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}


?>

      <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Informations sur le service</h1>
      </div>
      <div class="col-desc">
              <div id="output"></div>
              <form id="SubmitForm" class="forms" action="update_business.php?id=<?php echo $id;?>" enctype="multipart/form-data" method="post">
                <div class="form-group">
                  <label for="inputBizname">Nom du service </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputBizname" id="inputBizname" placeholder="Nom de votre service" value="<?php echo stripslashes($BizRow['business_name']);?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputDescription">Description </label>
                  <textarea class="form-control" id="inputDescription" name="inputDescription" rows="3" placeholder="Dîtes en plus sur votre business"><?php echo stripslashes($BizRow['description']);?></textarea>
                </div>
                <div class="form-group">
                  <label for="inputLineOne">Adresse 1 </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputLineOne" id="inputLineOne" placeholder="Adresse 1" value="<?php echo stripslashes($BizRow['address_1']);?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputLineTwo">Adresse 2</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputLineTwo" id="inputLineTwo" placeholder="Adresse 2" value="<?php echo stripslashes($BizRow['address_2']);?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputCity">Ville </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCity" name="inputCity">
                      
                      <option value="<?php echo $City;?>"><?php echo $City;?></option>	
                      <option value="">Changer de ville</option>
                      <?php
if($SelectCity = $mysqli->query("SELECT city_id, city FROM city WHERE city!='$City'")){

    while($CityRow = mysqli_fetch_array($SelectCity)){
				
?>
                      <option value="<?php echo $CityRow['city'];?>"><?php echo $CityRow['city'];?></option>
                      <?php

}

	$SelectCity->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPhone">Numéro de téléphone </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputPhone" id="inputPhone" placeholder="Numéro de téléphone" value="<?php echo stripslashes($BizRow['phone']);?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputWeb">Adresse Web </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputWeb" id="inputWeb" placeholder="URL Site Web" value="<?php echo stripslashes($BizRow['website']);?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputEmail">Adresse Email </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email de contact" value="<?php echo stripslashes($BizRow['email']);?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <!--<label for="inputMenu">Menu Web Address</label>-->
                 <!-- <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>-->
                    <input type="hidden" class="form-control" name="inputMenu" id="inputMenu" placeholder="Web Address to Your Menu" value="<?php echo stripslashes($BizRow['menu']);?>">
                 <!-- </div> -->
                </div>
                
                <div class="form-group">
                  <label for="inputImage">Image </label>
                  <input type="file" name="inputImage" id="inputImage" class="filestyle" data-iconName="glyphicon-picture" data-buttonText="Choisir une image">
                </div>
                <div class="form-group">
                  <label for="inputCategory">Categorie </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputCategory" name="inputCategory">
                      <option value="<?php echo $SelectedRow['cat_id'];?>"><?php echo $SelectedRow['category'];?></option>
                      <option value="">Changer de Categorie</option>
                      <?php
if($SelectCategories = $mysqli->query("SELECT cat_id, category FROM categories WHERE parent_id=0 AND cat_id!='$CatId'")){

    while($categoryRow = mysqli_fetch_array($SelectCategories)){
				
?>
                      <option value="<?php echo $categoryRow['cat_id'];?>"><?php echo $categoryRow['category'];?></option>
                      <?php

}

	$SelectCategories->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputSubcategory">Sous-categorie </label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <select class="form-control" id="inputSubcategory" name="inputSubcategory">
                <?php if (!empty($GetSubId)){?>    
                    
                    <option value="<?php echo $GetSubId;?>"><?php echo $GetSubName;?></option>
<?php  } ?>
				<option value="">Changer de sous categorie</option>
                
<?php      
        
if($SelectSub = $mysqli->query("SELECT cat_id, category FROM categories WHERE parent_id='$CatId' AND cat_id!='$SubCat '")){

    while($SubRow = mysqli_fetch_array($SelectSub)){
				
?>
                    <option value="<?php echo $SubRow['cat_id'];?>"><?php echo $SubRow['category'];?></option>
<?php }

	$SelectSub->close();
	
}else{
    
	 printf("Il semble y avoir eu un problème");
}

?>                   </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputFacebook">Adresse Facebook</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputFacebook" id="inputFacebook" placeholder="Adresse de la page Facebook" value="<?php echo stripslashes($BizRow['facebook']);?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTwitter">Adresse Twitter</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputTwitter" id="inputTwitter" placeholder="Adresse de la page Twitter" value="<?php echo stripslashes($BizRow['twitter']);?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPinterest">Adresse Pinterest</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" name="inputPinterest" id="inputPinterest" placeholder="Adresse de la page Pinterest" value="<?php echo stripslashes($BizRow['pinterest']);?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputTags">Mots-clés (séparé par une virgule)</label>
                  <div class="input-group"> <span class="input-group-addon"><span class="fa fa-info"></span></span>
                    <input type="text" class="form-control" data-role="tagsinput" name="inputTags" id="inputTags" value="<?php echo stripslashes($BizRow['tags']);?>">
                  </div>
                </div>
                <button type="submit" id="submitButton" class="btn btn-danger btn-lg pull-right">Sauvegarder</button>
              </form>
  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
    
</div><!--col-md-8-->
    
    
    <div class="col-md-4">
      <?php include("side_bar.php");?>
    </div>
    <!--col-md-4--> 
    
  </div>
  <!--container-->
  
<?php } include("footer.php");?>