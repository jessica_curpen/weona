<?php 
	include("header_login.php");

?>
<div id="fb-root"></div>
<script>
  // This is called with the results from FB.getLoginStatus().
  function statusChangeCallback(response) 
  {
    console.log('statusChangeCallback');
    console.log(response);
    
    if (response.status === 'connected') 
	{
      
      testAPI();
    } 
	else if (response.status === 'not_authorized') 
	{
      
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } 
	else 
	{
      
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

 
  function checkLoginState() 
  {
    FB.getLoginStatus(function(response) 
	{
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() 
  {
		FB.init
		({
			appId      : '1823126117920821',
			cookie     : true,  
			xfbml      : true,  
			version    : 'v2.7' 
	  });



	  FB.getLoginStatus(function(response) 
	  {
		statusChangeCallback(response);
	  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) 
  {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/fr_FR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


  function loginFBUser() 
  {
		  FB.login(function(response) 
		  {
			console.log('statusChangeCallback');
			console.log(response);
			
			if (response.status === 'connected') 
			{
			  
			  callAPI();
			} 
			else if (response.status === 'not_authorized') 
			{
			  
			  document.getElementById('status').innerHTML = 'Please log ' +
				'into this app.';
			} 
			else 
			{
			  
			  document.getElementById('status').innerHTML = 'Please log ' +
				'into Facebook.';
			}
		 }, {
				scope: 'public_profile, email',
				return_scopes: true
			});
 
  }
  
  
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) 
	{
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
		
		document.getElementById('FBusername').innerHTML = response.name ;
    });
  }
  
  function callAPI() 
  {
    console.log('Attempting call to API');
	
    FB.api('/me', 'GET',{fields: 'id,name,email,first_name,last_name,picture.type(large){url}'},function(response) 
	{
      console.log('Calling funtion to log user :  ' + response.name);
      //console.log(response.picture.data.url);
	  
	  $.ajax({
									type: "POST",
									url: "check_facebook_user.php",
									data:{
											id : response.id,
											username : response.name,
											firstname : response.first_name,
											lastname : response.last_name,
											email : response.email,
											picture : response.picture.data.url
											//gender : response.gender
										 }, 
									cache: false,
									success: function(html)
									{
										//	alert(html);
										if(html == 'ok')
										{
											window.location = "index.php";
										}
									}
							});
	  
    });
  }
</script>


<div class="container container-main">

<div class="col-md-8">

<script type="text/javascript" src="js/jquery.form.js"></script>

<script>
$(document).ready(function()
{
    $('#LoginForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours de connexion.. Veuillez patienter..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}
</script>





      <div class="col-shadow">
      <div class="biz-title-2">
        <h1>Connexion</h1>
      </div>
      <div class="col-desc">
                      <div id="output"></div>
                          <form id="LoginForm" class="forms" action="submit_login.php" method="post">
<div class="form-group">
            <label for="inputUsername">Email</label>
                <div class="input-group">
                   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
<input type="text" class="form-control" name="inputUsername" id="inputUsername" placeholder="Email">
</div>
</div>

<div class="form-group">
            <label for="inputPassword">Mot de Passe</label>
                <div class="input-group">
                   <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
<input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Password">
</div>
</div>
                              <div class="form-group">
                              <div class="txt-centered">Avez-vous oublié vos <a href="recover">identifiants</a> ?</div>
                              </div>
                               <button type="submit" id="submitButton" class="btn btn-danger btn-lg btn-block">Connexion</button>
							   </form>
							   <button class="fbBtn" onclick="loginFBUser()"><strong>Connexion</strong> avec <strong>facebook</strong><strong><div id="FBusername"></div></strong></button>
							   
								<a href="register.php"><p> Vous voulez créer un compte ? Inscrivez vous !</p></a>


  </div>
      <!--col-desc--> 
    </div>
    <!--col-shadow-->
      
<?php if(!empty($Ad2)){?>
<div class="col-shadow col-ads">
<?php echo $Ad2;?>
</div><!--col-shadow-->
<?php } ?>      

</div><!--col-md-8-->

<div class="col-md-4">
<?php include("side_bar.php");?>
</div><!--col-md-4-->


</div><!--container-->

<?php include("footer.php");?>