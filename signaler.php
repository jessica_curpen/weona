<?php include("header.php");

?>

<script>
$(document).ready(function()
{
    $('#SubmitForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">En cours d\'envoi..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
}

</script>
<div class="container container-main">

		<div class="col-md-8">
			
				<form action="submit_remarque.php" id="SubmitForm" method="post" >
				
						<div class="col-shadow">
						
								<div class="biz-title-2">
								
										<h1> Faîtes remonter un problème ou des remarques </h1>
										
								</div>
								 <div id="output"></div>
								<div class="col-desc">
										
										<div class="form-group">
											<label for="objet">Objet</label>
											
											<input id="objet" name="objet" class="form-control" type="text">
										</div>
										
										
										<div class="form-group">
											<label for="description">Description</label>
											
											<textarea id="description" name="description" class="form-control"  rows="6"> 
											</textarea>
										</div>
										
										<button type="submit" class="btn btn-lg btn-danger pull-right" id="submitButton">Signaler</button>
									
								</div>
								
						  <!--col-desc-->
						  
						</div>
				
				</form>
				
		</div>

</div>